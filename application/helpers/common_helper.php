<?php

//Check Authentication
function CheckAuthentication($token, $user_id) {
    $ci = & get_instance();
    $ci->db->select('*');
    $ci->db->from('users');
    $ci->db->where('id', $user_id);
    $ci->db->where('auth_token', $token);
    $query = $ci->db->get();
    $result = $query->result();
    return $result;
}

//Generate Random Number
function GenerateRandomNumber($length) {
    $token = "";
    $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $codeAlphabet .= "abcdefghijklmnopqrstuvwxyz";
    $codeAlphabet .= "0123456789";
    $max = strlen($codeAlphabet); // edited

    for ($i = 0; $i < $length; $i++) {
        $token .= $codeAlphabet[rand(0, $max - 1)];
    }

    return $token;
}

//Get Details From Table
function GetDetails($tablename, $where = NULL) {
    $ci = & get_instance();
    $ci->db->select('*');
    $ci->db->from($tablename);
    if ($where)
        $ci->db->where($where);
    $query = $ci->db->get();
    $result = $query->result();
    return $result;
}

//Get Details From Table
function getBrandById($id) {
    $ci = & get_instance();
    $ci->db->select('brand');
    $ci->db->from('brands');
    if ($id)
        $ci->db->where('id', $id);
    $query = $ci->db->get();
    $result = $query->row();
    return $result->brand;
}

//Get Details From Table
function getModelById($id) {
    $ci = & get_instance();
    $ci->db->select('model');
    $ci->db->from('models');
    if ($id)
        $ci->db->where('id', $id);
    $query = $ci->db->get();
    $result = $query->row();
    return $result->model;
}

function CreateMessageForNotification($lang, $key) {
    $ci = & get_instance();
    $ci->lang->load('message', $lang);
    $value = $ci->lang->line($key);
    return $value;
}

//check Admin Login
function CheckAdminLogin() {
    $ci = & get_instance();
    if (!$ci->session->userdata('admin_data'))
        redirect(base_url() . 'Admin');
}
