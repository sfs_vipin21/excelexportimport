<?php

class Apimodel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function GetUserProfile($where = NULL, $order_by_key = 'users.id', $order_by = 'DESC') {
        $this->db->select('users.*, user_profile.first_name, user_profile.last_name, user_profile.profile_image, user_profile.dob, user_profile.address, user_profile.apartment, user_profile.street,user_profile.city,user_profile.state, user_profile.country,user_profile.zipcode, user_profile.address_latitude, user_profile.address_longitude, user_profile.profession, user_profile.about, user_profile.language, user_other_details.fav_junk_food,user_other_details.fav_destination,user_other_details.fav_vegan_cuisine,user_other_details.allergies,user_other_details.other_allergies');
        $this->db->from('users');
        $this->db->join('user_profile', 'users.id = user_profile.user_id');
        $this->db->join('user_other_details', 'user_profile.user_id = user_other_details.user_id');
        if ($where)
            $this->db->where($where);
        $this->db->order_by($order_by_key, $order_by);
        //echo $this->db->last_query();
        $query = $this->db->get();
        return $query->result();
    }

    public function GetAllergies($where = NULL, $lang) {
        $this->db->select('*');
        $this->db->from('allergies');
        if ($where)
            $this->db->where($where);
        $this->db->where('status', 1);
        $query = $this->db->get();
        $result = $query->result();
        if (!empty($result)) {
            foreach ($result as $row) {
                if ($lang == 'en') {
                    $AllergiesData[] = array(
                        'id' => $row->id,
                        'allergy_name' => $row->name
                    );
                } else {
                    $AllergiesData[] = array(
                        'id' => $row->id,
                        'allergy_name' => $row->name
                    );
                }
            }
        } else {
            $AllergiesData = array();
        }
        return $AllergiesData;
    }

    public function GetVeganCuisine($where = NULL, $lang) {
        $this->db->select('*');
        $this->db->from('vegan_cuisine');
        if ($where)
            $this->db->where($where);
        $this->db->where('status', 1);
        $query = $this->db->get();
        $result = $query->result();
        if (!empty($result)) {
            foreach ($result as $row) {
                if ($lang == 'en') {
                    $CuisineData[] = array(
                        'id' => $row->id,
                        'cuisine_name' => $row->name
                    );
                } else {
                    $CuisineData[] = array(
                        'id' => $row->id,
                        'cuisine_name' => $row->name
                    );
                }
            }
        } else {
            $CuisineData = array();
        }
        return $CuisineData;
    }

    public function GetLanguages($where = NULL, $lang) {
        $this->db->select('*');
        $this->db->from('language');
        if ($where)
            $this->db->where($where);
        $this->db->where('status', 1);
        $query = $this->db->get();
        $result = $query->result();
        if (!empty($result)) {
            foreach ($result as $row) {
                if ($lang == 'en') {
                    $LanguageData[] = array(
                        'id' => $row->id,
                        'lang_name' => $row->lang_name
                    );
                } else {
                    $LanguageData[] = array(
                        'id' => $row->id,
                        'lang_name' => $row->lang_name
                    );
                }
            }
        } else {
            $LanguageData = array();
        }
        return $LanguageData;
    }

    public function GetDrinks($where = NULL, $lang) {
        $this->db->select('*');
        $this->db->from('drinks');
        if ($where)
            $this->db->where($where);
        $this->db->where('status', 1);
        $query = $this->db->get();
        $result = $query->result();
        if (!empty($result)) {
            foreach ($result as $row) {
                if ($lang == 'en') {
                    $DrinkData[] = array(
                        'id' => $row->id,
                        'drink_name' => $row->drink_name
                    );
                } else {
                    $DrinkData[] = array(
                        'id' => $row->id,
                        'drink_name' => $row->drink_name
                    );
                }
            }
        } else {
            $DrinkData = array();
        }
        return $DrinkData;
    }

    public function GetDesserts($where = NULL, $lang) {
        $this->db->select('*');
        $this->db->from('dessert');
        if ($where)
            $this->db->where($where);
        $this->db->where('status', 1);
        $query = $this->db->get();
        $result = $query->result();
        if (!empty($result)) {
            foreach ($result as $row) {
                if ($lang == 'en') {
                    $DessertData[] = array(
                        'id' => $row->id,
                        'dessert_name' => $row->dessert_name
                    );
                } else {
                    $DessertData[] = array(
                        'id' => $row->id,
                        'dessert_name' => $row->dessert_name
                    );
                }
            }
        } else {
            $DessertData = array();
        }
        return $DessertData;
    }

    public function GetEvents($where = NULL, $order_by_key = 'event.id', $order_by = 'DESC') {
        $this->db->select('event.*,event_other_details.apartment,event_other_details.name,event_other_details.street,event_other_details.city,event_other_details.state,event_other_details.country,event_other_details.zipcode,event_other_details.about_me_cuisine,event_other_details.vegan_cuisine,event_other_details.drinks,event_other_details.other_drinks,event_other_details.common_allergies,event_other_details.other_common_allergies,event_other_details.recurring_event,event_other_details.week_day_id,event_other_details.min_guest,event_other_details.max_guest,event_other_details.no_of_courses,event_other_details.dessert,event_other_details.latitude,event_other_details.longitude');
        $this->db->from('event');
        $this->db->join('event_other_details', 'event.id = event_other_details.event_id');
        if ($where)
            $this->db->where($where);
        $this->db->order_by($order_by_key, $order_by);
        //echo $this->db->last_query();
        $query = $this->db->get();
        return $query->result();
    }

    public function GetEventCoverImage($event_id) {
        $this->db->select('*');
        $this->db->from('event_cover_image');
        $this->db->where('event_id', $event_id);
        $query = $this->db->get();
        $result = $query->result();
        if (!empty($result)) {
            foreach ($result as $row) {
                $ResultArr[] = array(
                    'id' => $row->id,
                    'cover_pic' => ($row->cover_pic) ? base_url() . EVENT_PIC_URL . $row->cover_pic : "",
                );
            }
        } else {
            $ResultArr = array();
        }
        return $ResultArr;
    }

}
