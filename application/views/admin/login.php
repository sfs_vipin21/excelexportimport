<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><?php echo $title; ?></title>
        <!-- The styles -->
        <link id="bs-css" href="<?php echo INCLUDE_ADMIN_ASSETS; ?>css/bootstrap-cerulean.min.css" rel="stylesheet">
        <link href="<?php echo INCLUDE_ADMIN_ASSETS; ?>css/charisma-app.css" rel="stylesheet">
        <link href='<?php echo INCLUDE_ADMIN_ASSETS; ?>css/animate.min.css' rel='stylesheet'>
        <script src="<?php echo INCLUDE_ADMIN_ASSETS; ?>js/jquery.min.js"></script>
        <link href="<?php echo INCLUDE_ADMIN_ASSETS; ?>css/style.css" rel="stylesheet">
        <link rel="shortcut icon" href="img/favicon.ico">
    </head>
    <body>
        <div class="ch-container">
            <div class="row">
                <div class="row">
                    <div class="col-md-12 center login-header">
                        <h2>Welcome to Cloud Car Hire</h2>
                    </div>
                    <!--/span-->
                </div><!--/row-->
                <div class="row">
                    <div class="well col-md-5 center login-box">
                        <?php
                        if ($this->session->flashdata('error_msg')) {
                            ?>
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <?php echo $this->session->flashdata('error_msg'); ?>
                            </div>
                        <?php } ?>
                        <form class="form-horizontal" action="<?php echo base_url() . 'admin'; ?>" method="post">
                            <fieldset>
                                <div class="input-group input-group-lg">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user red"></i></span>
                                    <input type="email" name="email" id="email" class="form-control" placeholder="Email" required="">
                                </div>
                                <div class="clearfix"></div><br>

                                <div class="input-group input-group-lg">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
                                    <input type="password" name="pwd" id="pwd" class="form-control" placeholder="Password" required="">
                                </div>
                                <div class="clearfix"></div>
                                <p class="center col-md-5">
                                    <button type="submit" name="login_sub" class="btn btn-primary">Login</button>
                                </p>
                            </fieldset>
                        </form>
                    </div>
                    <!--/span-->
                </div><!--/row-->
            </div><!--/fluid-row-->

        </div><!--/.fluid-container-->

        <!-- external javascript -->

        <script src="<?php echo INCLUDE_ADMIN_ASSETS; ?>js/bootstrap.min.js"></script>
        <!-- application script for Charisma demo -->
        <script src="<?php echo INCLUDE_ADMIN_ASSETS; ?>js/charisma.js"></script>


    </body>
</html>
