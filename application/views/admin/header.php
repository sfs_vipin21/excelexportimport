<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $title; ?></title>
        <!-- The styles -->
        <link id="bs-css" href="<?php echo INCLUDE_ADMIN_ASSETS; ?>css/bootstrap-cerulean.min.css" rel="stylesheet">
        <link href="<?php echo INCLUDE_ADMIN_ASSETS; ?>css/font_awesome.css" rel="stylesheet">
        <link href="<?php echo INCLUDE_ADMIN_ASSETS; ?>css/charisma-app.css" rel="stylesheet">
        <link href="<?php echo INCLUDE_ADMIN_ASSETS; ?>css/style.css" rel="stylesheet">
        <link href='<?php echo INCLUDE_ADMIN_ASSETS; ?>css/fullcalendar.css' rel='stylesheet'>
        <link href='<?php echo INCLUDE_ADMIN_ASSETS; ?>css/fullcalendar.print.css' rel='stylesheet' media='print'>
        <link href='<?php echo INCLUDE_ADMIN_ASSETS; ?>css/responsive-tables.css' rel='stylesheet'>
        <link href='<?php echo INCLUDE_ADMIN_ASSETS; ?>css/animate.min.css' rel='stylesheet'>
        <script src="<?php echo INCLUDE_ADMIN_ASSETS; ?>js/jquery.min.js"></script>
        <link rel="shortcut icon" href="<?php echo INCLUDE_ADMIN_ASSETS ?>image/favicon.ico">
        <style>
            #DataTables_Table_0_filter{float: right !important;}
        </style>
    </head>
    <body>
        <!-- topbar starts -->
        <div class="navbar navbar-default" role="navigation">
            <div class="navbar-inner">
                <button type="button" class="navbar-toggle pull-left animated flip">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_url() . 'dashboard'; ?>"> 
                    <img alt="Charisma Logo" src="<?php echo INCLUDE_ADMIN_ASSETS ?>image/logo.png" class="hidden-xs"/>
                </a>

                <!-- user dropdown starts -->
                <div class="btn-group pull-right">
                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> <?php echo $AdminDetails[0]->name; ?></span>
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        <li><a href="#">Profile</a></li>
                        <li class="divider"></li>
                        <li><a href="<?php echo base_url() . 'admin'; ?>">Logout</a></li>
                    </ul>
                </div>
                <!-- user dropdown ends -->
            </div>
        </div>
        <!-- topbar ends -->
        <div class="ch-container">
            <div class="row">