<div id="content" class="col-lg-10 col-sm-10">
    <div>
        <ul class="breadcrumb">
            <li>
                <a href="<?php echo base_url() . 'dashboard'; ?>">Dashboard</a>
            </li>
            <li>
                <a href="<?php echo base_url() . 'brands'; ?>">Brand Management</a>
            </li>
        </ul>
    </div>
    <div class=" row">
        <div class="box col-md-12">
            <div class="box-inner">
                <div class="box-header well" data-original-title="">
                    <h2><i class="fa fa-list"></i> Brands</h2>
                    <a class="pull-right" href="<?php echo base_url() . 'addbrand'; ?>">
                        <button type="button" class="btn btn-sm btn-info heading_btn">Add Brand</button>
                    </a>
                </div>
                <div class="box-content">
                    <?php
                    if ($this->session->flashdata('error_msg')) {
                        ?>
                        <div class="alert alert-danger alert-dismissible alert_msg">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <?php echo $this->session->flashdata('error_msg'); ?>
                        </div>

                    <?php } if ($this->session->flashdata('success_msg')) { ?>

                        <div class="alert alert-success alert-dismissible alert_msg">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <?php echo $this->session->flashdata('success_msg'); ?>
                        </div>

                    <?php } ?>
                    <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Brand</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($brandsData) {
                                foreach ($brandsData as $key => $row) {
                                    ?>
                                    <tr>
                                        <td><?php echo $key + 1; ?></td>
                                        <td><?php echo $row->brand; ?></td>
                                        <td class="center">
                                            <a title="Edit Brand" class="btn btn-info" href="<?php echo base_url(); ?>">
                                                <i class="glyphicon glyphicon-edit icon-white"></i>
                                                Edit
                                            </a>
                                            <a title="Delete Brand" onClick="return confirm('Are you sure you want to delete this item?')" class="btn btn-danger" href="<?php echo base_url() . 'deletebrand/' . $row->id; ?>">
                                                <i class="glyphicon glyphicon-trash icon-white"></i>
                                                Delete
                                            </a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div><!--/#content.col-md-0-->
</div><!--/fluid-row-->