<div id="content" class="col-lg-10 col-sm-10">
    <!-- content starts -->
    <div>
        <ul class="breadcrumb">
            <li>
                <a class="text-light" href="<?php echo base_url() . 'brands'; ?>"><i class="fa fa-tags"></i> Brands</a>
            </li>
            <li>
                <a class="text-light">Add Brand</a>
            </li>
        </ul>
    </div>
    <div class=" row">
        <div class="box col-md-12">
            <div class="box-inner">
                <div class="box-header well" data-original-title="">
                    <h2 class="text-light"><i class="fa fa-tags"></i> Add Brand</h2>
                </div>
                <div class="box-content">
                    <?php
                    if ($this->session->flashdata('error_msg')) {
                        ?>
                        <div class="alert alert-danger alert-dismissible alert_msg">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <?php echo $this->session->flashdata('error_msg'); ?>
                        </div>

                    <?php } if ($this->session->flashdata('success_msg')) { ?>

                        <div class="alert alert-success alert-dismissible alert_msg">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <?php echo $this->session->flashdata('success_msg'); ?>
                        </div>

                    <?php } ?>
                    <form style="overflow: hidden;" role="form" action="<?php echo base_url() . 'addbrand'; ?>" method="post">
                        <div class="form-group">
                            <label><h5>Name</h5></label>
                            <input type="text" class="form-control" name="brand" id="brand" placeholder="Brand" value="<?php echo set_value('brand'); ?>" required="">
                            <span class="field_error text-danger"><?php echo form_error('brand'); ?></span>
                        </div>
                        <div class="form-group">
                            <input type="submit" name="submit_btn" id="submit_btn" class="btn btn-info pull-right" value="Add">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- content ends -->
</div><!--/#content.col-md-0-->