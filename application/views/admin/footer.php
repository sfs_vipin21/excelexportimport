<footer class="row" style="bottom: 0 !important;width: 70% !important;">
    <p class="col-md-9 col-sm-9 col-xs-12 copyright">&copy; <a>Cloud Car Hire</a> <?php echo date('Y'); ?></p>
</footer>
</div><!--/.fluid-container-->

<script src="<?php echo INCLUDE_ADMIN_ASSETS; ?>js/bootstrap.min.js"></script>
<!-- calender plugin -->
<script src='<?php echo INCLUDE_ADMIN_ASSETS; ?>js/moment.min.js'></script>
<script src='<?php echo INCLUDE_ADMIN_ASSETS; ?>js/fullcalendar.min.js'></script>
<!-- data table plugin -->
<script src='<?php echo INCLUDE_ADMIN_ASSETS; ?>js/jquery.dataTables.js'></script>
<!-- library for making tables responsive -->
<script src="<?php echo INCLUDE_ADMIN_ASSETS; ?>js/responsive-tables.js"></script>
<!-- application script for Charisma demo -->
<script src="<?php echo INCLUDE_ADMIN_ASSETS; ?>js/charisma.js"></script>


</body>
</html>
