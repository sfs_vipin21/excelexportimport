<div id="content" class="col-lg-10 col-sm-10">
    <div>
        <ul class="breadcrumb">
            <li>
                <a href="<?php echo base_url().'Dashboard'; ?>">Dashboard</a>
            </li>
        </ul>
    </div>
    <div class=" row">
        <div class="col-md-3 col-sm-3 col-xs-6">
            <a data-toggle="tooltip" title="6 new members." class="well top-block" href="#">
                <i class="glyphicon glyphicon-user blue"></i>
                <div>Total Users</div>
                <div>507</div>
                <span class="notification">6</span>
            </a>
        </div>
    </div>
</div><!--/#content.col-md-0-->
</div><!--/fluid-row-->