<div class="col-sm-2 col-lg-2">
    <div class="sidebar-nav">
        <div class="nav-canvas">
            <div class="nav-sm nav nav-stacked">

            </div>
            <ul class="nav nav-pills nav-stacked main-menu">
                <li class="nav-header">Main</li>
                <li <?php if ($this->uri->segment(1) == 'dashboard') { ?> class="active" <?php } ?>>
                    <a href="<?php echo base_url() . 'dashboard'; ?>">
                        <i class="glyphicon glyphicon-home"></i>
                        <span> Dashboard</span>
                    </a>
                </li>
                <!-- User Management -->
                <li <?php if ($this->uri->segment(1) == 'users') { ?> class="active" <?php } ?>>
                    <a href="<?php echo base_url() . 'users'; ?>">
                        <i class="fa fa-users"></i>
                        <span> User Management</span>
                    </a>
                </li>
                <!-- Brand Management -->
                <li <?php if ($this->uri->segment(1) == 'brands' || $this->uri->segment(1) == 'addbrand') { ?> class="active" <?php } ?>>
                    <a href="<?php echo base_url() . 'brands'; ?>">
                        <i class="fa fa-list"></i>
                        <span> Brand Management</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>