<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Cloud Car Hire</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

        <style>
            .logo{width:50%;margin:0 auto 50px;}
            .logo img{width:100%;}
            .submit{background:#b7d166;border:none;}
        </style>
    </head>
    <body>

        <div class="container">
            <div class="col-md-6 offset-md-3 my-4">
                <div class="logo">
                    <img src="<?php echo base_url(); ?>Assets/logo.png">
                </div>
                <?php
                $show = 1;
                if ($this->session->flashdata('success_msg')) {
                    $show = 0;
                    ?>
                    <div class="alert alert-success">
                        <?php echo $this->session->flashdata('success_msg'); ?>
                    </div>
                    <?php
                }
                if ($this->session->flashdata('error_msg')) {
                    $show = 1;
                    ?>
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <?php echo $this->session->flashdata('error_msg'); ?>
                    </div>
                <?php } ?>
                <form action="<?php echo base_url().'Api/UpdatePassword/' . $email; ?>" method="post" <?php if ($show == 0) { ?> style="display:none;" <?php } ?>>
                    <div class="form-group">
                        <label for="pwd">New Password:</label>
                        <input type="password" class="form-control" id="new_password" name="new_password" value="" required="">
                    </div>
                    <div class="form-group">
                        <label for="pwd">Confirm Password:</label>
                        <input type="password" class="form-control" id="confirm_password" name="confirm_password" value="" required="">
                    </div>
                    <input type="hidden" name="id" value="<?php echo $id; ?>">
                    <button type="submit" class="btn btn-success submit" name="submit_btn" value="Submit">Submit</button>
                </form>
            </div>
        </div>

    </body>
</html>