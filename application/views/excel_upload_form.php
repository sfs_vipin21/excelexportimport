<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Import Export Excel</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

</head>
<body>
    <div class="container">
        <div class="row justify-content-center mt-5">
            <div class="col-sm-6">

                <!-- Display Error or success message -->
                <?php
                    if($this->session->flashdata('message'))
                    {
                        echo $this->session->flashdata('message');
                    }
                ?>

                <div class="card">
                    <div class="card-header">
                        Export Data To Rental Agencies
                    </div>
                    <div class="card-body">
                        <form action="<?php echo base_url('index.php/RentalAgenciesController/import_excel_data_to_db')?>" method="POST" enctype="multipart/form-data">
                            <div class="form-group">
                                <!-- <input type="file" name="file_upload" class="form-control"> -->
                                <input type="file" name="upload_file" class="form-control">
                            </div>

                            <input type="submit" name="upload_file" value="Upload" class="btn btn-primary my-3">
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- <div class="row justify-content-center mt-5">
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-header">
                        Download Excel Data From Rental Agencies
                    </div>
                    <div class="card-body">
                        <a href="<?php //echo base_url('index.php/RentalAgenciesController/spreadsheet_download')?>" class="btn btn-success">Download Excel Data</a>
                    </div>
                </div>
            </div>
        </div> -->
    </div>
</body>
</html>