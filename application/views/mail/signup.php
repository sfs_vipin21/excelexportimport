<!DOCTYPE html>
<head>
    <title>Signup - Cloud Car Hire</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style type="text/css">
        @font-face {
            font-family: 'Circular Std';
            src: url('<?php echo base_url(); ?>Assets/font/CircularStd-Medium.woff2') format('woff2'),
            url('<?php echo base_url(); ?>Assets/font/CircularStd-Medium.woff') format('woff');
            font-weight: 500;
            font-style: normal;
        }
    </style>
</head>
<body style="font-family:'Circular Std'; padding:0; margin:0;">

    <div id="mailsub" class="notification" align="center" style="width:100%; max-width:680px; min-width: 300px; margin:0 auto; margin:0 auto; box-shadow: 0 0 10px rgba(0,0,0,0.2); background-color:#ffffff;">
        <table width="100%" style="border-collapse: collapse;">
            <tr style="text-align: center; margin-bottom: 25px;">
                <td style="background-image: url(<?php echo base_url(); ?>Assets/banner.png); width:100%;  height: 250px; background-size: cover; background-repeat: no-repeat; ">
                   <img src="<?php echo base_url(); ?>Assets/logo.png">
                </td>
            </tr>
             <tr>
                <td style="padding: 15px 25px;">
                   <h2 style="font-family: 'Circular Std'; font-weight: lighter; margin-top: 20px; padding:0; font-size:30px; color: #484848; text-align:center;"><?php echo $hello . ' ' . $first_name; ?></h2>
                </td>
            </tr>
             <tr>
                <td style="margin: 15px 0 35px; display: block; padding: 0 25px; text-align: center;">
                  <p style="padding:0;  margin:0 0 25px  0;   font-size: 20px; line-height: 20px;  color: #868686; font-weight: lighter; border-bottom: 2px solid #054991; display: inline-block;     padding-bottom: 10px;">Welcome</p>
                  <p style="padding:0;  margin:0 0 15px  0;   font-size: 16px; line-height: 20px;  color: #868686; font-weight: lighter;">Your new Cloud Car Hire account has been created. Welcome to the Cloud Car Hire. Please login in to your account using your email address and your password.</p>
                  <p style="padding:0;  margin:0 0 40px  0;   font-size: 16px; line-height: 20px;  color: #868686; font-weight: lighter;">Click below button to verify your account.</p>
                  <a href="<?php echo $link; ?>" style="text-decoration: none;"><button style="background-color:#f68d1f; color:#fff; border:none; outline:none; padding:14px 50px; font-size:16px; cursor: pointer; text-transform: uppercase; border-radius: 2px; font-family: 'Circular Std';">Verify</button></a>
                </td>
            </tr>
             <tr>
                <td style="padding: 15px 25px;">
                   <h5 style="margin-top: 20px; padding:0; font-size:18px; line-height: 22px; font-weight: 400; font-family: 'Circular Std';     font-weight: 500; color: #868686;"><?php echo $thanks; ?>, <br> <?php $dinning_app_team; ?></h5>
                </td>
            </tr>
             <tr>
                <td style="background-color: #054991; text-align:center; padding: 0 25px;">
                   <p style="color:#fff; padding: 0; margin:15px 0; font-size:12px; font-family: 'Circular Std';">COPYRIGHT © 2018 CLOUD CAR HIRE</p>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>