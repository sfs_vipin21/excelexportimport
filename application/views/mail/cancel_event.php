<!DOCTYPE html>
<head>
    <title>Cancel Event</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>

    <?php
    $first_name = (isset($host->first_name) && !empty($host->first_name)) ? $host->first_name : '';
    $last_name = (isset($host->last_name) && !empty($host->last_name)) ? $host->last_name : '';
    $title = (isset($event->title) && !empty($event->title)) ? $event->title : '';
    $start_time = (isset($event->start_time) && !empty($event->start_time)) ? date("h:i A", strtotime($event->start_time . ':00')) : '';
    $end_time = (isset($event->end_time) && !empty($event->end_time)) ? date("h:i A", strtotime($event->end_time . ':00')) : '';
    $meal_type = (isset($event->meal_type) && !empty($event->meal_type)) ? date("h:i A", strtotime($event->meal_type . ':00')) : '';
    $date = (isset($event_date) && !empty($event_date)) ? $event_date : '';
    ?>
    <div id="mailsub" class="notification" align="center">
        <table style="min-width: 320px;">
            <tr>
                <td align="center" bgcolor="#eff3f8">
                    <table border="0" width="100%" style="max-width: 680px; min-width: 300px;">
                        <!--header -->
                        <tr>
                            <td align="center" bgcolor="#ffffff">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td align="center" style="border-bottom:1px solid #eee; padding: 5%;background:white">
                                            <font face="font-size: 13px;">
                                            <img src="<?php echo base_url(); ?>Assets/logo.png" width="150" alt="" border="0" />
                                            </font>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        <tr>
                            <td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding: 5px 20px;">
                                    <tr>
                                        <td>
                                            <p style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 22px; color: #333; line-height: 24px;">Hello Admin</p>
                                            <p class="font-family: Arial, Helvetica, sans-serif; font-size: 17px; color: #57697e; line-height: 24px;"><?php echo $first_name . ' ' . $last_name; ?> has cancelled the event '<?php echo $title; ?>' on date <?php echo $event_date; ?>. Below information is about event eaters and joined eaters to be refunded for given amount.</p>  
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding: 5px 20px;">
                                    <tr>
                                        <td style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 17px; color: #333; line-height: 24px;">Event</td>
                                        <td style="font-family: Arial, Helvetica, sans-serif; font-size: 17px; color: #333; line-height: 24px;"><?php echo $title; ?></td>
                                    </tr>
                                    <tr>
                                        <td style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 17px; color: #333; line-height: 24px;">Date</td>
                                        <td style="font-family: Arial, Helvetica, sans-serif; font-size: 17px; color: #333; line-height: 24px;"><?php echo $event_date; ?></td>
                                    </tr>
                                    <tr>
                                        <td style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 17px; color: #333; line-height: 24px;">Time</td>
                                        <td style="font-family: Arial, Helvetica, sans-serif; font-size: 17px; color: #333; line-height: 24px;"><?php echo $start_time . ' - ' . $end_time; ?></td>
                                    </tr>
                                    <tr>
                                        <td style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 17px; color: #333; line-height: 24px;">Meal type</td>
                                        <td style="font-family: Arial, Helvetica, sans-serif; font-size: 17px; color: #333; line-height: 24px;"><?php echo $meal_type; ?></td>
                                    </tr>
                                    <tr>
                                        <td style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 17px; color: #333; line-height: 24px;">Host</td>
                                        <td style="font-family: Arial, Helvetica, sans-serif; font-size: 17px; color: #333; line-height: 24px;"><?php echo $first_name . ' ' . $last_name; ?></td>
                                    </tr> 
                                    <tr>
                                        <td colspan="2" style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 20px; color: #333; padding: 13px 0px 0px;">Joined Eaters List</td>
                                    </tr>
                                </table>
                                <hr style="color: #ddd; width: 95%; align: center;"><br>
                                <table width="94%" align="center" border="1" cellspacing="0" cellpadding="10">
                                    <tr style="background: #eff3f8;">
                                        <td style="width:20%;font-family: Arial, Helvetica, sans-serif;font-size: 18px; color: #333;font-weight: bold;">Name</td>
                                        <td style="width:20%;font-family: Arial, Helvetica, sans-serif;font-size: 18px; color: #333;font-weight: bold;">Email</td>
                                        <td style="width:20%;font-family: Arial, Helvetica, sans-serif;font-size: 18px; color: #333;font-weight: bold;">Amount</td>
                                        <td style="width:20%;font-family: Arial, Helvetica, sans-serif;font-size: 18px; color: #333;font-weight: bold;">Payment Mode</td>
                                    </tr>
                                    <?php if (isset($users) && !empty($users)) { ?>
                                        <?php foreach ($users as $key => $value) { ?>
                                            <?php $payment_mode = (isset($value->payment_mode) && !empty($value->payment_mode) && $value->payment_mode == 'card') ? 'Card' : 'Cryptocurrency'; ?>
                                            <?php $bg = ($key % 2 == 0) ? '#fff' : '#eff3f8'; ?>
                                            <tr style="background: <?php echo $bg; ?>;">
                                                <td style="width:20%;font-family: Arial, Helvetica, sans-serif;font-size: 16px; color: #333;"><?php echo $value->first_name . ' ' . $value->last_name; ?></td>
                                                <td style="width:20%;font-family: Arial, Helvetica, sans-serif;font-size: 16px; color: #333;"><?php echo $value->email; ?></td>
                                                <td style="width:20%;font-family: Arial, Helvetica, sans-serif;font-size: 16px; color: #333;"><?php echo $value->total_amount; ?></td>
                                                <td style="width:20%;font-family: Arial, Helvetica, sans-serif;font-size: 16px; color: #333;"><?php echo $payment_mode; ?></td>
                                            </tr>
                                        <?php } ?>
                                    <?php } else { ?>
                                            <tr>
                                                <td colspan="4">No eaters have joined this event.</td>
                                            </tr>
                                    <?php } ?>
                                </table>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding: 5px 20px;">
                                    <tr>
                                        <td style="font-family: Arial, Helvetica, sans-serif;font-size: 16px; color: #333; font-weight: bold;"><br>Regards!<br>Veats App</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>