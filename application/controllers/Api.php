<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Twilio\Rest\Client;

require 'vendor/autoload.php';

use Aws\S3\S3Client;

class Api extends CI_Controller {

    public $language;

    public function __construct() {
        parent::__construct();
        //ini_set('memory_limit', '-1');
        $this->response = new stdClass();
        //ini_set("display_errors", 1);
        //error_reporting(0);
        //require_once(APPPATH . 'libraries/stripe/init.php');
        //\Stripe\Stripe::setApiKey("sk_test_AzXnnmtP3fY5uEqmlSY07EdJ");
        date_default_timezone_set("UTC");
        $this->load->model('Apimodel');
        $this->language = (isset($_POST['lang']) && !empty($_POST['lang'])) ? $_POST['lang'] : "en";
        if ($this->language == 'en')
            $this->lang->load('message', 'english');
        else
            $this->lang->load('message', 'english');
         
        $this->s3 = new S3Client(array(
            'version' => 'latest',
            'region' => 'us-west-1',
            'credentials' => array(
                'key' => 'AKIAQTSAYYP6CGQNG4JN',
                'secret' => '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',
            )
        ));
    }

    public function index() {
        echo $this->lang->line('welcome_message');
    }

    public function send_sms_request() {
        $number = ($this->input->post('number', TRUE)) ? $this->input->post('number', TRUE) : '';
        if (!empty($number)) {
            $api_key = 'a7108f02';
            $api_secret = 'DB9lp6Ghgspzwq2T';
            $brand = 'Cloud Car Hire';

            $url = "https://api.nexmo.com/verify/json?";

            $query = http_build_query([
                'number' => $number,
                'api_key' => $api_key,
                'api_secret' => $api_secret,
                'brand' => $brand,
                'next_event_wait' => 60
            ]);

            $response = file_get_contents($url . $query);
            $result = json_decode($response);
            $this->response->success = 200;
            $this->response->message = 'Success.';
            $this->response->data = $result;
            die(json_encode($this->response));
        } else {
            $this->response->success = 203;
            $this->response->message = 'Please send mobile number.';
            die(json_encode($this->response));
        }
    }

    public function verify_sms_request() {
        $request_id = ($this->input->post('request_id', TRUE)) ? $this->input->post('request_id', TRUE) : '';
        $code = ($this->input->post('code', TRUE)) ? $this->input->post('code', TRUE) : '';
        if (!empty($request_id) && !empty($code)) {
            $api_key = 'a7108f02';
            $api_secret = 'DB9lp6Ghgspzwq2T';

            $url = "https://api.nexmo.com/verify/check/json?";

            $query = http_build_query([
                'api_key' => $api_key,
                'api_secret' => $api_secret,
                'request_id' => $request_id,
                'code' => $code
            ]);

            $response = file_get_contents($url . $query);
            $result = json_decode($response);
            $this->response->success = 200;
            $this->response->message = 'Success.';
            $this->response->data = $result;
            die(json_encode($this->response));
        } else {
            $this->response->success = 203;
            $this->response->message = 'Please send all required fields.';
            die(json_encode($this->response));
        }
    }

    public function SendMail($email, $subject, $content) {
        /* require '/var/www/html/backend/PHPMailer/PHPMailerAutoload.php';

          // Instantiate a new PHPMailer
          $mail = new PHPMailer;

          // Tell PHPMailer to use SMTP
          $mail->isSMTP();
          $mail->setFrom('souvikdutta.qa@gmail.com', 'Cloud Car Hire');
          $mail->addAddress($email);
          $mail->Username = 'AKIAILPK57HXM74CALSQ';
          $mail->Password = 'AvhnPxfSuPFEZY/zZAgVhMI6g0EDwZOP88o6U2DWZ26I';

          $mail->Host = 'email-smtp.eu-west-1.amazonaws.com';
          //$mail->SMTPDebug = 2;

          $mail->Subject = $subject;

          $mail->Body = $content;

          // Tells PHPMailer to use SMTP authentication
          $mail->SMTPAuth = true;

          // Enable TLS encryption over port 587
          $mail->SMTPSecure = 'tls'; //ssl
          $mail->Port = 587; //465
          $mail->isHTML(true);

          //$mail->AltBody = "Email Test\r\nThis email was sent through the Amazon SES SMTP interface using the PHPMailer class.";

          $mail->send(); */

        $config['protocol'] = 'sendmail';
        $config['mailtype'] = 'html';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'utf-8';
        $config['wordwrap'] = TRUE;

        $this->email->initialize($config);

        $this->email->from('info@realestate.com', $this->lang->line('realestate_app'));
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($content);
        $this->email->send();
        //echo $this->email->print_debugger();
    }

    public function SignUp() {
        $first_name = ($this->input->post('first_name', TRUE)) ? $this->input->post('first_name', TRUE) : '';
        $last_name = ($this->input->post('last_name', TRUE)) ? $this->input->post('last_name', TRUE) : '';
        $email = ($this->input->post('email', TRUE)) ? $this->input->post('email', TRUE) : '';
        $password = ($this->input->post('password', TRUE)) ? $this->input->post('password', TRUE) : '';
        $phone_number = ($this->input->post('phone_number', TRUE)) ? $this->input->post('phone_number', TRUE) : '';
        $company_name = ($this->input->post('company_name', TRUE)) ? $this->input->post('company_name', TRUE) : '';
        $licence_number = ($this->input->post('licence_number', TRUE)) ? $this->input->post('licence_number', TRUE) : '';
        $license_exp_date = ($this->input->post('license_exp_date', TRUE)) ? $this->input->post('license_exp_date', TRUE) : '';
        $license_front_image = (!empty($_FILES['license_front_image'])) ? $_FILES['license_front_image']['name'] : '';
        $license_back_image = (!empty($_FILES['license_back_image'])) ? $_FILES['license_back_image']['name'] : '';
        $device_type = ($this->input->post('device_type', TRUE)) ? $this->input->post('device_type', TRUE) : '';
        $device_token = ($this->input->post('device_token', TRUE)) ? $this->input->post('device_token', TRUE) : '';
        $role = ($this->input->post('role', TRUE)) ? $this->input->post('role', TRUE) : 'tenant'; // agent , tenant
        $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $res = "";
        for ($i = 0; $i < 7; $i++) {
            $res .= $chars[mt_rand(0, strlen($chars) - 1)];
        }
        $created = date("Y-m-d H:i:s");

        if (!empty($first_name) && !empty($email) && !empty($password)) {
            if ($role == 'agent' && (empty($company_name) || empty($licence_number) || empty($license_exp_date))) {
                $this->response->success = 201;
                $this->response->message = $this->lang->line('required_field_error');
                die(json_encode($this->response));
            }

            $check_email_already_exist = $this->Custom->query("SELECT * FROM users WHERE email = '" . $email . "'");
            if (!empty($check_email_already_exist)) {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('email_exist');
                $this->response->data = $check_email_already_exist[0];
                die(json_encode($this->response));
            }

            if (!empty($license_front_image)) {
                $name_ext = explode(".", $license_front_image);
                $ext = end($name_ext);
                $new_name = time() . '_front_image.' . $ext;
                $config['file_name'] = $new_name;
                $config['upload_path'] = $_SERVER['DOCUMENT_ROOT'] . "/Assets/uploads/license_images/";
                $config['allowed_types'] = '*';
                $config['overwrite'] = TRUE;
                $config['remove_spaces'] = TRUE;

                if (!is_dir($config['upload_path']))
                    die("THE UPLOAD DIRECTORY DOES NOT EXIST");

                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if (!$this->upload->do_upload('license_front_image')) {
                    $_POST['license_front_image'] = '';
                } else {
                    $_POST['license_front_image'] = $new_name;
                }
            }

            if (!empty($license_back_image)) {
                $name_ext2 = explode(".", $license_back_image);
                $ext2 = end($name_ext2);
                $new_name2 = time() . '_back_image.' . $ext2;
                $config['file_name'] = $new_name2;
                $config['upload_path'] = $_SERVER['DOCUMENT_ROOT'] . "/Assets/uploads/license_images/";
                $config['allowed_types'] = '*';
                $config['overwrite'] = TRUE;
                $config['remove_spaces'] = TRUE;

                if (!is_dir($config['upload_path']))
                    die("THE UPLOAD DIRECTORY DOES NOT EXIST");

                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if (!$this->upload->do_upload('license_front_image')) {
                    $_POST['license_front_image'] = '';
                } else {
                    $_POST['license_front_image'] = $new_name2;
                }
            }

            $auth_token = GenerateRandomNumber(11);
            $insert_arr = array(
                'first_name' => $first_name,
                'last_name' => $first_name,
                'email' => $email,
                'password' => md5($password),
                'role' => $role,
                'phone_number' => $phone_number,
                'company_name' => $company_name,
                'profile_pic' => '',
                'fb_id' => '',
                'g_id' => '',
                'is_email_verified' => 0,
                'licence_number' => $licence_number,
                'license_exp_date' => $license_exp_date,
                'license_front_image' => $license_front_image,
                'license_back_image' => $license_back_image,
                'device_type' => $device_type,
                'device_token' => $device_token,
                'auth_token' => $auth_token,
                'status' => 1,
                'created' => $created
            );
            $insert_id = $this->Custom->insert_data('users', $insert_arr);
            if ($insert_id) {
                $user_data = $this->Custom->get_where('users', array('id' => $insert_id));

                //send mail
                $mail_data['link'] = base_url('index.php/Api/ConfirmAccount/' . urlencode($email));
                $mail_data['hello'] = $this->lang->line('hello');
                $mail_data['thanks'] = $this->lang->line('thanks');
                $mail_data['realestate_app_team'] = $this->lang->line('realestate_app_team');
                $mail_data['account_register_content'] = $this->lang->line('account_register_content');
                $mail_data['first_name_text'] = $this->lang->line('first_name_text');
                $mail_data['email_text'] = $this->lang->line('email_text');
                $mail_data['last_name_text'] = $this->lang->line('last_name_text');
                $mail_data['password_text'] = $this->lang->line('password_text');
                $mail_data['verify_text'] = $this->lang->line('verify_text');
                $mail_data['verify_link_content'] = $this->lang->line('verify_link_content');
                $mail_data['first_name'] = $first_name;
                $mail_data['password'] = $password;
                $mail_data['email'] = $email;

                $content = $this->load->view('mail/signup', $mail_data, TRUE);
                $subject = $this->lang->line('account_register_subject') . ' - ' . $this->lang->line('realestate_app');
                //$this->SendMail($email, $subject, $content);

                $ret_data = (object) array('id' => $insert_id, 'role' => $role, 'auth_token' => $auth_token);

                $this->response->success = 200;
                $this->response->message = $this->lang->line('user_registered');
                $this->response->data = $user_data[0];
                die(json_encode($this->response));
            } else {
                $this->response->success = 202;
                $this->response->message = $this->lang->line('something_went_wrong');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function checkEmail() {
        $email = ($this->input->post('email', TRUE)) ? $this->input->post('email', TRUE) : '';
        if (!empty($email)) {
            $email_record = $this->Custom->get_data('users', 'email', $email);
            if (isset($email_record) && !empty($email_record)) {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('email_exist');
                die(json_encode($this->response));
            } else {
                $this->response->success = 200;
                $this->response->message = "Email available.";
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function ConfirmAccount() {
        $email = $this->uri->segment(3);
        $email = (isset($email) && !empty($email)) ? urldecode($email) : '';
        if (!empty($email)) {
            $check_user = $this->Custom->get_where('users', array('email' => $email));
            if (isset($check_user) && !empty($check_user)) {
                $user_id = $check_user[0]->id;
                $status = $check_user[0]->status;
                if ($status == 1) {
                    $mail_data['thanks'] = $this->lang->line('thanks');
                    $mail_data['name'] = $check_user[0]->name;
                    $mail_data['realestate_app_team'] = $this->lang->line('realestate_app_team');
                    $mail_data['message'] = $this->lang->line('account_already_confirmed');
                    $this->load->view('mail/confirmation_mail', $mail_data);
                } else {
                    $result = $this->Custom->update('users', array('status' => 1, 'is_email_verified' => 1), 'id', $user_id);

                    $mail_data['thanks'] = $this->lang->line('thanks');
                    $mail_data['name'] = $check_user[0]->name;
                    $mail_data['realestate_app_team'] = $this->lang->line('realestate_app_team');
                    $mail_data['message'] = $this->lang->line('email_verified');
                    $this->load->view('mail/confirmation_mail', $mail_data);
                }
            } else {
                $mail_data['thanks'] = $this->lang->line('thanks');
                $mail_data['name'] = '';
                $mail_data['realestate_app_team'] = $this->lang->line('realestate_app_team');
                $mail_data['message'] = $this->lang->line('account_not_confirmed');
                $this->load->view('mail/confirmation_mail', $mail_data);
            }
        } else {
            $mail_data['thanks'] = $this->lang->line('thanks');
            $mail_data['realestate_app_team'] = $this->lang->line('realestate_app_team');
            $mail_data['message'] = $this->lang->line('account_not_confirmed');
            $this->load->view('mail/confirmation_mail', $mail_data);
        }
    }

    public function ForgotPassword() {
        $email = ($this->input->post('email', TRUE)) ? $this->input->post('email', TRUE) : '';
        if (!empty($email)) {
            $user_record = $this->Custom->get_where('users', array('email' => $email));
            if (isset($user_record) && !empty($user_record)) {
                //$data['id'] = $user_record[0]->id;
                //$this->load->view('ForgotPassword', $data);
                //$password = GenerateRandomNumber(7);
                //$new_password = md5($password);
                //if ($this->Custom->update('users', array('password' => $new_password, 'password_status' => 1), 'id', $user_record[0]->id)) {
                $mail_data['hello'] = $this->lang->line('hello');
                $mail_data['thanks'] = $this->lang->line('thanks');
                $mail_data['realestate_app_team'] = $this->lang->line('realestate_app_team');
                $mail_data['forgot_password_content'] = $this->lang->line('forgot_password_content');
                $mail_data['password_link'] = base_url('Api/UpdatePassword/' . urlencode($email));
                $content = $this->load->view('mail/forgot_mail', $mail_data, TRUE);
                $subject = $this->lang->line('forgot_password_subject') . ' - ' . $this->lang->line('realestate_app');
                //$this->SendMail($email, $subject, $content);

                $this->response->success = 200;
                $this->response->message = $this->lang->line('password_updated');
                die(json_encode($this->response));
                /* } else {
                  $this->response->success = 202;
                  $this->response->message = $this->lang->line('something_went_wrong');
                  die(json_encode($this->response));
                  } */
//            } else {
//                $this->response->success = 203;
//                $this->response->message = $this->lang->line('email_not_registered');
//                die(json_encode($this->response));
//            }
            } else {
                $this->response->success = 202;
                $this->response->message = "Your email account is not registered with us";
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function UpdatePassword() {
        $email = $this->uri->segment(3);
        if (isset($_POST['submit_btn']) && !empty($_POST['submit_btn'])) {
            if ($_POST['new_password'] != $_POST['confirm_password']) {
                $this->session->set_flashdata('error_msg', "New password & confirm password doesn't match.");
                $data['id'] = $_POST['id'];
                redirect(base_url() . 'Api/UpdatePassword/' . $email);
            }
            if ($this->Custom->update('users', array('password' => md5($_POST['new_password'])), 'id', $_POST['id'])) {
                $data['id'] = $_POST['id'];
                $this->session->set_flashdata('success_msg', "Password successfully changed.");
                redirect(base_url() . 'Api/UpdatePassword/' . $email);
            } else {
                $data['id'] = $_POST['id'];
                $this->session->set_flashdata('error_msg', "Something went wrong.");
                redirect(base_url() . 'Api/UpdatePassword/' . $email);
            }
        } else {
            $email = (isset($email) && !empty($email)) ? urldecode($email) : '';
            $user_record = $this->Custom->get_where('users', array('email' => $email));
            $data['id'] = $user_record[0]->id;
            $this->load->view('ForgotPassword', $data);
        }
    }

    public function checkSocialAccount() {
        $login_type = ($this->input->post('login_type', TRUE)) ? $this->input->post('login_type', TRUE) : '';
        $fb_id = ($this->input->post('fb_id', TRUE)) ? $this->input->post('fb_id', TRUE) : '';
        $g_id = ($this->input->post('g_id', TRUE)) ? $this->input->post('g_id', TRUE) : '';
        $device_token = ($this->input->post('device_token', TRUE)) ? $this->input->post('device_token', TRUE) : '';
        $device_type = ($this->input->post('device_type', TRUE)) ? $this->input->post('device_type', TRUE) : '';
        $profile_pic = ($this->input->post('profile_pic', TRUE)) ? $this->input->post('profile_pic', TRUE) : '';
        if (isset($login_type) && !empty($login_type)) {
            switch ($login_type):
                case 'facebook' :
                    if (!empty($fb_id)) {
                        $user_data = $this->Custom->get_where('users', array('fb_id' => $fb_id));
                        if ($user_data) {
                            $user_id = $user_data[0]->id;
                            $auth_token = GenerateRandomNumber(11);
                            $this->Custom->update_where('users', array('auth_token' => $auth_token), array('id' => $user_id));

                            if (!empty($device_token))
                                $this->Custom->update_where('users', array('device_token' => $device_token), array('id' => $user_id));

                            if (!empty($device_type))
                                $this->Custom->update_where('users', array('device_type' => $device_type), array('id' => $user_id));

                            if (!empty($profile_pic))
                                $this->Custom->update_where('users', array('profile_pic' => $profile_pic), array('id' => $user_id));

                            $user_updated_data = $this->Custom->get_where('users', array('fb_id' => $fb_id));
                            if (isset($user_updated_data[0]->profile_pic) && !empty($user_updated_data[0]->profile_pic)) {
                                if (preg_match("/http/i", $user_updated_data[0]->profile_pic)) {
                                    $user_updated_data[0]->profile_pic = $user_updated_data[0]->profile_pic;
                                } else {
                                    $user_updated_data[0]->profile_pic = base_url('Assets/uploads/license_images/' . $user_updated_data[0]->profile_pic);
                                }
                            } else {
                                $user_updated_data[0]->profile_pic = '';
                            }
                            //$user_updated_data[0]->profile_pic = ($user_updated_data[0]->profile_pic) ? base_url('Assets/uploads/license_images/' . $user_updated_data[0]->profile_pic)  : "";
                            $user_updated_data[0]->license_front_image = ($user_updated_data[0]->license_front_image) ? base_url('Assets/uploads/license_images/' . $user_updated_data[0]->license_front_image) : "";
                            $user_updated_data[0]->license_back_image = ($user_updated_data[0]->license_back_image) ? base_url('Assets/uploads/license_images/' . $user_updated_data[0]->license_back_image) : "";

                            $this->response->success = 200;
                            $this->response->message = $this->lang->line('logged_in');
                            $this->response->data = $user_updated_data[0];
                            die(json_encode($this->response));
                        } else {
                            $this->response->success = 202;
                            $this->response->message = $this->lang->line('something_went_wrong');
                            die(json_encode($this->response));
                        }
                    } else {
                        $this->response->success = 201;
                        $this->response->message = $this->lang->line('required_field_error');
                        die(json_encode($this->response));
                    }
                    break;

                case 'google' :
                    if (!empty($g_id)) {
                        $user_data = $this->Custom->get_where('users', array('g_id' => $g_id));
                        if ($user_data) {
                            $user_id = $user_data[0]->id;
                            $auth_token = GenerateRandomNumber(11);
                            $this->Custom->update_where('users', array('auth_token' => $auth_token), array('id' => $user_id));

                            if (!empty($device_token))
                                $this->Custom->update_where('users', array('device_token' => $device_token), array('id' => $user_id));

                            if (!empty($device_type))
                                $this->Custom->update_where('users', array('device_type' => $device_type), array('id' => $user_id));

                            if (!empty($profile_pic)) {
                                $this->Custom->update_where('users', array('profile_pic' => $profile_pic), array('id' => $user_id));
                            }
                            $user_updated_data = $this->Custom->get_where('users', array('id' => $user_id));
                            $user_updated_data[0]->profile_pic = ($user_updated_data[0]->profile_pic) ? base_url('Assets/uploads/license_images/' . $user_updated_data[0]->profile_pic) : "";
                            $user_updated_data[0]->license_front_image = ($user_updated_data[0]->license_front_image) ? base_url('Assets/uploads/license_images/' . $user_updated_data[0]->license_front_image) : "";
                            $user_updated_data[0]->license_back_image = ($user_updated_data[0]->license_back_image) ? base_url('Assets/uploads/license_images/' . $user_updated_data[0]->license_back_image) : "";

                            $this->response->success = 200;
                            $this->response->message = $this->lang->line('logged_in');
                            $this->response->data = $user_updated_data[0];
                            die(json_encode($this->response));
                        } else {
                            $this->response->success = 202;
                            $this->response->message = $this->lang->line('something_went_wrong');
                            die(json_encode($this->response));
                        }
                    } else {
                        $this->response->success = 201;
                        $this->response->message = $this->lang->line('required_field_error');
                        die(json_encode($this->response));
                    }
                    break;
            endswitch;
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function Login() {
        $login_type = ($this->input->post('login_type', TRUE)) ? $this->input->post('login_type', TRUE) : 'manual';
        $fb_id = ($this->input->post('fb_id', TRUE)) ? $this->input->post('fb_id', TRUE) : '';
        $g_id = ($this->input->post('g_id', TRUE)) ? $this->input->post('g_id', TRUE) : '';
        $first_name = ($this->input->post('first_name', TRUE)) ? $this->input->post('first_name', TRUE) : '';
        $last_name = ($this->input->post('last_name', TRUE)) ? $this->input->post('last_name', TRUE) : '';
        $email = ($this->input->post('email', TRUE)) ? $this->input->post('email', TRUE) : '';
        $password = ($this->input->post('password', TRUE)) ? $this->input->post('password', TRUE) : '';
        $device_token = ($this->input->post('device_token', TRUE)) ? $this->input->post('device_token', TRUE) : '';
        $device_type = ($this->input->post('device_type', TRUE)) ? $this->input->post('device_type', TRUE) : '';
        $profile_pic = ($this->input->post('profile_pic', TRUE)) ? $this->input->post('profile_pic', TRUE) : '';
        $phone_number = ($this->input->post('phone_number', TRUE)) ? $this->input->post('phone_number', TRUE) : '';
        $role = ($this->input->post('role', TRUE)) ? $this->input->post('role', TRUE) : 'tenant';
        $created_at = date("Y-m-d H:i:s");
        if (isset($login_type) && !empty($login_type)) {
            switch ($login_type):
                case 'manual':
                    if (!empty($email) && !empty($password)) {
                        $user_data_email = $this->Custom->get_where('users', array('email' => $email));
                        if (empty($user_data_email)) {
                            $this->response->success = 203;
                            $this->response->message = $this->lang->line('email_not_registered');
                            die(json_encode($this->response));
                        }
                        $user_data = $this->Custom->get_where('users', array('email' => $email, 'password' => md5($password)));
                        if (empty($user_data)) {
                            $this->response->success = 203;
                            $this->response->message = $this->lang->line('invalid_email_password');
                            die(json_encode($this->response));
                        }
                        if ($user_data[0]->status != 1) {
                            $this->response->success = 204;
                            $this->response->message = $this->lang->line('account_not_activate');
                            die(json_encode($this->response));
                        }
                        $user_id = $user_data[0]->id;
                        $auth_token = GenerateRandomNumber(11);
                        $this->Custom->update_where('users', array('auth_token' => $auth_token), array('id' => $user_id));
                        if (!empty($device_token))
                            $this->Custom->update_where('users', array('device_token' => $device_token), array('id' => $user_id));

                        if (!empty($device_type))
                            $this->Custom->update_where('users', array('device_type' => $device_type), array('id' => $user_id));
                        $user_details = $this->Custom->get_where('users', array('id' => $user_id));
                        if (isset($user_details[0]->profile_pic) && !empty($user_details[0]->profile_pic)) {
                            if (preg_match("/http/i", $user_details[0]->profile_pic)) {
                                $user_details[0]->profile_pic = $user_details[0]->profile_pic;
                            } else {
                                $user_details[0]->profile_pic = base_url('Assets/uploads/license_images/' . $user_details[0]->profile_pic);
                            }
                        } else {
                            $user_details[0]->profile_pic = '';
                        }
                        //$user_details[0]->profile_pic = ($user_details[0]->profile_pic) ? base_url('Assets/uploads/license_images/' . $user_details[0]->profile_pic)  : "";
                        $user_details[0]->license_front_image = ($user_details[0]->license_front_image) ? base_url('Assets/uploads/license_images/' . $user_details[0]->license_front_image) : "";
                        $user_details[0]->license_back_image = ($user_details[0]->license_back_image) ? base_url('Assets/uploads/license_images/' . $user_details[0]->license_back_image) : "";

                        $this->response->success = 200;
                        $this->response->message = $this->lang->line('logged_in');
                        $this->response->data = $user_details[0];
                        die(json_encode($this->response));
                    } else {
                        $this->response->success = 201;
                        $this->response->message = $this->lang->line('required_field_error');
                        die(json_encode($this->response));
                    }
                    break;

                case 'facebook' :
                    if (!empty($fb_id)) {
                        $user_data = $this->Custom->get_where('users', array('fb_id' => $fb_id));
                        if ($user_data) {
                            $user_id = $user_data[0]->id;
                            $auth_token = GenerateRandomNumber(11);
                            $this->Custom->update_where('users', array('auth_token' => $auth_token), array('id' => $user_id));
                            if (!empty($latitude))
                                $this->Custom->update_where('users', array('latitude' => $latitude), array('id' => $user_id));

                            if (!empty($longitude))
                                $this->Custom->update_where('users', array('longitude' => $longitude), array('id' => $user_id));

                            if (!empty($device_token))
                                $this->Custom->update_where('users', array('device_token' => $device_token), array('id' => $user_id));

                            if (!empty($device_type))
                                $this->Custom->update_where('users', array('device_type' => $device_type), array('id' => $user_id));

                            if (!empty($profile_pic)) {
                                $this->Custom->update_where('users', array('profile_pic' => $profile_pic), array('id' => $user_id));
                            }

                            $user_data_updated = $this->Custom->get_where('users', array('fb_id' => $fb_id));
                            if (isset($user_data_updated[0]->profile_pic) && !empty($user_data_updated[0]->profile_pic)) {
                                if (preg_match("/http/i", $user_data_updated[0]->profile_pic)) {
                                    $user_data_updated[0]->profile_pic = $user_data_updated[0]->profile_pic;
                                } else {
                                    $user_data_updated[0]->profile_pic = base_url('Assets/uploads/license_images/' . $user_data_updated[0]->profile_pic);
                                }
                            } else {
                                $user_data_updated[0]->profile_pic = '';
                            }

                            $this->response->success = 200;
                            $this->response->message = $this->lang->line('logged_in');
                            $this->response->data = $user_data_updated[0];
                            die(json_encode($this->response));
                        } else {
                            $auth_token = GenerateRandomNumber(11);
                            if (!empty($email)) {
                                $user_data_email = $this->Custom->get_where('users', array('email' => $email));
                                if (isset($user_data_email) && !empty($user_data_email)) {
                                    $this->Custom->update_where('users', array('fb_id' => $fb_id, 'auth_token' => $auth_token), array('id' => $user_data_email[0]->id));
                                    /* if (!empty($profile_pic)) {
                                      $this->Custom->update_where('users', array('profile_pic' => $profile_pic), array('id' => $user_data_email[0]->id));
                                      $user_data_email[0]->profile_pic = $profile_pic;
                                      } else {
                                      $user_data_email[0]->profile_pic = ($user_data_email[0]->profile_pic) ? $user_data_email[0]->profile_pic : "";
                                      } */
                                    if (isset($user_data_email[0]->profile_pic) && !empty($user_data_email[0]->profile_pic)) {
                                        if (preg_match("/http/i", $user_data_email[0]->profile_pic)) {
                                            $user_data_email[0]->profile_pic = $user_data_email[0]->profile_pic;
                                        } else {
                                            $user_data_email[0]->profile_pic = base_url('Assets/uploads/license_images/' . $user_data_email[0]->profile_pic);
                                        }
                                    } else {
                                        $user_data_email[0]->profile_pic = '';
                                    }
                                    //$user_data_email[0]->profile_pic = ($user_data_email[0]->profile_pic) ? base_url('Assets/uploads/license_images/' . $user_data_email[0]->profile_pic)  : "";
                                    $user_data_email[0]->auth_token = $auth_token;
                                    $user_data_email[0]->license_front_image = ($user_data_email[0]->license_front_image) ? base_url('Assets/uploads/license_images/' . $user_data_email[0]->license_front_image) : "";
                                    $user_data_email[0]->license_back_image = ($user_data_email[0]->license_back_image) ? base_url('Assets/uploads/license_images/' . $user_data_email[0]->license_back_image) : "";
                                    $this->response->success = 200;
                                    $this->response->data = $user_data_email[0];
                                    $this->response->message = $this->lang->line('logged_in');
                                    die(json_encode($this->response));
                                }
                            }

                            $insert_arr = array(
                                'first_name' => $first_name,
                                'last_name' => $last_name,
                                'email' => $email,
                                'password' => '',
                                'profile_pic' => $profile_pic,
                                'fb_id' => $fb_id,
                                'g_id' => '',
                                'phone_number' => $phone_number,
                                'is_email_verified' => (isset($email) && !empty($email)) ? 1 : 0,
                                'device_type' => $device_type,
                                'device_token' => $device_token,
                                'auth_token' => $auth_token,
                                'role' => $role,
                                'status' => 1,
                                'created' => $created_at
                            );
                            $insert_id = $this->Custom->insert_data('users', $insert_arr);
                            if ($insert_id) {
                                $user_data_email = $this->Custom->get_where('users', array('email' => $email));
                                if (isset($user_data_email) && !empty($user_data_email)) {
                                    /* if (!empty($profile_pic)) {
                                      $this->Custom->update_where('users', array('profile_pic' => $profile_pic), array('id' => $insert_id));
                                      $user_data_email[0]->profile_pic = $profile_pic;
                                      } else {
                                      $user_data_email[0]->profile_pic = ($user_data_email[0]->profile_pic) ? $user_data_email[0]->profile_pic : "";
                                      } */
                                    if (isset($user_data_email[0]->profile_pic) && !empty($user_data_email[0]->profile_pic)) {
                                        if (preg_match("/http/i", $user_data_email[0]->profile_pic)) {
                                            $user_data_email[0]->profile_pic = $user_data_email[0]->profile_pic;
                                        } else {
                                            $user_data_email[0]->profile_pic = base_url('Assets/uploads/license_images/' . $user_data_email[0]->profile_pic);
                                        }
                                    } else {
                                        $user_data_email[0]->profile_pic = '';
                                    }
                                    $user_data_email[0]->license_front_image = ($user_data_email[0]->license_front_image) ? base_url('Assets/uploads/license_images/' . $user_data_email[0]->license_front_image) : "";
                                    $user_data_email[0]->license_back_image = ($user_data_email[0]->license_back_image) ? base_url('Assets/uploads/license_images/' . $user_data_email[0]->license_back_image) : "";
                                    $this->response->success = 200;
                                    $this->response->data = $user_data_email[0];
                                    $this->response->message = $this->lang->line('logged_in');
                                    die(json_encode($this->response));
                                } else {
                                    $this->response->success = 202;
                                    $this->response->message = $this->lang->line('something_went_wrong');
                                    die(json_encode($this->response));
                                }
                            } else {
                                $this->response->success = 202;
                                $this->response->message = $this->lang->line('something_went_wrong');
                                die(json_encode($this->response));
                            }
                        }
                    } else {
                        $this->response->success = 201;
                        $this->response->message = $this->lang->line('required_field_error');
                        die(json_encode($this->response));
                    }
                    break;

                case 'google' :
                    if (!empty($g_id)) {
                        $user_data = $this->Custom->get_where('users', array('g_id' => $g_id));
                        if ($user_data) {
                            $auth_token = GenerateRandomNumber(11);
                            $user_id = $user_data[0]->id;
                            $this->Custom->update_where('users', array('auth_token' => $auth_token), array('id' => $user_id));
                            if (!empty($device_token))
                                $this->Custom->update_where('users', array('device_token' => $device_token), array('id' => $user_id));

                            if (!empty($device_type))
                                $this->Custom->update_where('users', array('device_type' => $device_type), array('id' => $user_id));

                            if (!empty($profile_pic)) {
                                $this->Custom->update_where('users', array('profile_pic' => $profile_pic), array('id' => $user_id));
                                $user_data[0]->profile_pic = $profile_pic;
                            } else {
                                $user_data[0]->profile_pic = ($user_data[0]->profile_pic) ? $user_data[0]->profile_pic : "";
                            }

                            $this->response->success = 200;
                            $this->response->message = $this->lang->line('logged_in');
                            $this->response->data = $user_data[0];
                            die(json_encode($this->response));
                        } else {
                            $auth_token = GenerateRandomNumber(11);
                            if (!empty($email)) {
                                $user_data_email = $this->Custom->get_where('users', array('email' => $email));
                                if (isset($user_data_email) && !empty($user_data_email)) {
                                    $this->Custom->update_where('users', array('g_id' => $g_id, 'auth_token' => $auth_token), array('id' => $user_data_email[0]->id));
                                    /* if (!empty($profile_pic)) {
                                      $this->Custom->update_where('users', array('profile_pic' => $profile_pic), array('id' => $user_data_email[0]->id));
                                      $user_data_email[0]->profile_pic = $profile_pic;
                                      } else {
                                      $user_data_email[0]->profile_pic = ($user_data_email[0]->profile_pic) ? $user_data_email[0]->profile_pic : "";
                                      } */
                                    $user_data_email[0]->profile_pic = ($user_data_email[0]->profile_pic) ? base_url('Assets/uploads/license_images/' . $user_data_email[0]->profile_pic) : "";
                                    $user_data_email[0]->license_front_image = ($user_data_email[0]->license_front_image) ? base_url('Assets/uploads/license_images/' . $user_data_email[0]->license_front_image) : "";
                                    $user_data_email[0]->license_back_image = ($user_data_email[0]->license_back_image) ? base_url('Assets/uploads/license_images/' . $user_data_email[0]->license_back_image) : "";
                                    $this->response->success = 200;
                                    $this->response->data = $user_data_email[0];
                                    $this->response->message = $this->lang->line('logged_in');
                                    die(json_encode($this->response));
                                }
                            }

                            $insert_arr = array(
                                'first_name' => $first_name,
                                'last_name' => $last_name,
                                'email' => $email,
                                'password' => '',
                                'profile_pic' => $profile_pic,
                                'fb_id' => '',
                                'g_id' => $g_id,
                                'is_email_verified' => 1,
                                'device_type' => $device_type,
                                'device_token' => $device_token,
                                'auth_token' => $auth_token,
                                'role' => $role,
                                'status' => 1,
                                'created' => $created_at
                            );
                            $insert_id = $this->Custom->insert_data('users', $insert_arr);
                            if ($insert_id) {
                                $user_data_email = $this->Custom->get_where('users', array('email' => $email));
                                if (isset($user_data_email) && !empty($user_data_email)) {
                                    /* if (!empty($profile_pic)) {
                                      $this->Custom->update_where('users', array('profile_pic' => $profile_pic), array('id' => $insert_id));
                                      $user_data_email[0]->profile_pic = $profile_pic;
                                      } else {
                                      $user_data_email[0]->profile_pic = ($user_data_email[0]->profile_pic) ? $user_data_email[0]->profile_pic : "";
                                      } */
                                    $user_data_email[0]->profile_pic = ($user_data_email[0]->profile_pic) ? base_url('Assets/uploads/license_images/' . $user_data_email[0]->profile_pic) : "";
                                    $user_data_email[0]->license_front_image = ($user_data_email[0]->license_front_image) ? base_url('Assets/uploads/license_images/' . $user_data_email[0]->license_front_image) : "";
                                    $user_data_email[0]->license_back_image = ($user_data_email[0]->license_back_image) ? base_url('Assets/uploads/license_images/' . $user_data_email[0]->license_back_image) : "";
                                    $this->response->success = 200;
                                    $this->response->data = $user_data_email[0];
                                    $this->response->message = $this->lang->line('logged_in');
                                    die(json_encode($this->response));
                                } else {
                                    $this->response->success = 202;
                                    $this->response->message = $this->lang->line('something_went_wrong');
                                    die(json_encode($this->response));
                                }
                            } else {
                                $this->response->success = 202;
                                $this->response->message = $this->lang->line('something_went_wrong');
                                die(json_encode($this->response));
                            }
                        }
                    } else {
                        $this->response->success = 201;
                        $this->response->message = $this->lang->line('required_field_error');
                        die(json_encode($this->response));
                    }
                    break;
            endswitch;
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function GetPropertyContactName() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';
        if (!empty($user_id)) {
            $user_record = $this->Custom->get_data('users', 'id', $user_id);
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }
            }
        }
        $properties = $this->Custom->query("SELECT DISTINCT CONTACT_COMPANY from property_contacts WHERE `CONTACT_COMPANY` != '' ");
        if (empty($properties)) {
            $this->response->success = 205;
            $this->response->message = $this->lang->line('no_record');
            die(json_encode($this->response));
        }
        $this->response->success = 200;
        $this->response->message = "Contact Company Name";
        $this->response->properties = $properties;
        die(json_encode($this->response));
    }

    public function GetNeighborhoodName() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';
        if (!empty($user_id)) {
            $user_record = $this->Custom->get_data('users', 'id', $user_id);
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }
            }
        }
        $area = $this->Custom->query("SELECT DISTINCT NEIGHBORHOODS from properties Where is_deleted = 0");
        if (empty($area)) {
            $this->response->success = 205;
            $this->response->message = $this->lang->line('no_record');
            die(json_encode($this->response));
        }
        $this->response->success = 200;
        $this->response->message = "Neighborhoods Name";
        $this->response->properties = $area;
        die(json_encode($this->response));
    }

    public function Logout() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';
        if (isset($user_id) && !empty($user_id)) {
            $user_record = $this->Custom->get_data('users', 'id', $user_id);
            if (isset($user_record) && !empty($user_record)) {
                $update_res = $this->Custom->update("users", array('auth_token' => ''), 'id', $user_id);
                if ($update_res) {
                    $this->response->success = 200;
                    $this->response->message = $this->lang->line('logout');
                    die(json_encode($this->response));
                } else {
                    $this->response->success = 202;
                    $this->response->message = $this->lang->line('something_went_wrong');
                    die(json_encode($this->response));
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function ChangePassword() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';
        $old_password = ($this->input->post('old_password')) ? $this->input->post('old_password') : '';
        $new_password = ($this->input->post('new_password')) ? $this->input->post('new_password') : '';
        if (!empty($user_id) && !empty($old_password) && !empty($new_password)) {
            $user_record = $this->Custom->get_data('users', 'id', $user_id);
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }

                /* if ($user_record[0]->status != 1) {
                  $this->response->success = 204;
                  $this->response->message = $this->lang->line('account_not_activate');
                  die(json_encode($this->response));
                  } */
                if ($user_record[0]->password != md5($old_password)) {
                    $this->response->success = 203;
                    $this->response->message = $this->lang->line('old_password_error');
                    die(json_encode($this->response));
                }
                $update_res = $this->Custom->update("users", array('password' => md5($new_password)), 'id', $user_id);
                if ($update_res) {
                    $user_details = $this->Custom->get_data('users', 'id', $user_id);

                    $this->response->success = 200;
                    $this->response->message = $this->lang->line('password_changed');
                    $this->response->data = $user_details[0];
                    die(json_encode($this->response));
                } else {
                    $this->response->success = 202;
                    $this->response->message = $this->lang->line('something_went_wrong');
                    die(json_encode($this->response));
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function UpdateUserSettings() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';
        //die(json_encode($_POST));
        if (!empty($user_id)) {
            $user_record = $this->Custom->get_data('users', 'id', $user_id);
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }

                /* if ($user_record[0]->status != 1) {
                  $this->response->success = 204;
                  $this->response->message = $this->lang->line('account_not_activate');
                  die(json_encode($this->response));
                  } */


                unset($_POST['user_id']);
                $update_res = $this->Custom->update("user_settings", $_POST, 'user_id', $user_id);
                if ($update_res) {
                    $user_details = $this->Custom->get_data('user_settings', 'user_id', $user_id);

                    $this->response->success = 200;
                    $this->response->message = $this->lang->line('number_verified');
                    $this->response->data = $user_details[0];
                    die(json_encode($this->response));
                } else {
                    $this->response->success = 202;
                    $this->response->message = $this->lang->line('something_went_wrong');
                    die(json_encode($this->response));
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function GetProfile() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';

        if (!empty($user_id)) {
            $user_record = $this->Custom->get_data('users', 'id', $user_id);
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }

                $user_record[0]->profile_pic = ($user_record[0]->profile_pic) ? base_url('Assets/uploads/license_images/' . $user_record[0]->profile_pic) : "";
                $user_record[0]->license_front_image = ($user_record[0]->license_front_image) ? base_url('Assets/uploads/license_images/' . $user_record[0]->license_front_image) : "";
                $user_record[0]->license_back_image = ($user_record[0]->license_back_image) ? base_url('Assets/uploads/license_images/' . $user_record[0]->license_back_image) : "";

                /* if ($user_record[0]->status != 1) {
                  $this->response->success = 204;
                  $this->response->message = $this->lang->line('account_not_activate');
                  die(json_encode($this->response));
                  } */
                $this->response->success = 200;
                $this->response->message = $this->lang->line('profile_details');
                $this->response->data = $user_record[0];
                die(json_encode($this->response));
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function GetUserSettings() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';

        if (!empty($user_id)) {
            $user_record = $this->Custom->get_data('users', 'id', $user_id);
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }

                $user_setting = $this->Custom->get_data('user_settings', 'user_id', $user_id);
                if (isset($user_setting) && !empty($user_setting)) {
                    $this->response->success = 200;
                    $this->response->message = "User settings";
                    $this->response->data = $user_setting[0];
                    die(json_encode($this->response));
                } else {
                    $this->response->success = 203;
                    $this->response->message = "User settings not found.";
                    die(json_encode($this->response));
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function GetProperties() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';
        $sort = ($this->input->post('sort')) ? $this->input->post('sort') : 'DATE_UPDATE';
        $order = ($this->input->post('order')) ? $this->input->post('order') : 'desc';
        $PROPERTY_TYPE = ($this->input->post('PROPERTY_TYPE')) ? $this->input->post('PROPERTY_TYPE') : '';
        $keyword = ($this->input->post('keyword')) ? $this->input->post('keyword') : '';
        $priceMin = ($this->input->post('priceMin')) ? $this->input->post('priceMin') : '';
        $priceMax = ($this->input->post('priceMax')) ? $this->input->post('priceMax') : '';
        $bedsMin = $this->input->post('bedsMin');
        $bedsMax = $this->input->post('bedsMax');
        $bathMin = ($this->input->post('bathMin')) ? $this->input->post('bathMin') : '';
        $bathMax = ($this->input->post('bathMax')) ? $this->input->post('bathMax') : '';
        $sfMin = ($this->input->post('sfMin')) ? $this->input->post('sfMin') : '';
        $sfMax = ($this->input->post('sfMax')) ? $this->input->post('sfMax') : '';
        $amenities = ($this->input->post('amenities')) ? $_POST['amenities'] : '';              //array
        $type = ($this->input->post('type')) ? $this->input->post('type') : ''; //map
        $LATITUDE = ($this->input->post('LATITUDE')) ? $this->input->post('LATITUDE') : '';
        $LONGITUDE = ($this->input->post('LONGITUDE')) ? $this->input->post('LONGITUDE') : '';
        $ADDRESS = ($this->input->post('ADDRESS')) ? $this->input->post('ADDRESS') : '';
        $DATE_AVAILABLE = ($this->input->post('DATE_AVAILABLE')) ? $this->input->post('DATE_AVAILABLE') : '';
        $CONTACT_COMPANY = ($this->input->post('CONTACT_COMPANY')) ? $this->input->post('CONTACT_COMPANY') : '';   //"Lakeview","Bicycle"  =>double quoted string
        $ACTUAL_PHOTOS = ($this->input->post('ACTUAL_PHOTOS')) ? $this->input->post('ACTUAL_PHOTOS') : '';
        $area = ($this->input->post('area')) ? $this->input->post('area') : '';                  //"Lakeview","Bicycle"  =>double quoted string
        $page = ($this->input->post('page')) ? $this->input->post('page') : 1;
        $per_page = 10;
        $offset = ($page == 1) ? 0 : ($page - 1) * $per_page;
  //echo"<pre>";
        if (!empty($user_id)) {
            $user_record = $this->Custom->get_data('users', 'id', $user_id);
 //print_r($user_record);die;
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }
            }
        }

        $where = "";
        if (!empty($PROPERTY_TYPE)) {
            $where .= " p.PROPERTY_TYPE = '$PROPERTY_TYPE' AND";
        }
        if (!empty($keyword)) {
            $where .= " (p.LISTING_TITLE LIKE '%$keyword%' OR p.NAME LIKE '%$keyword%' OR p.ADDRESS LIKE '%$keyword%') AND";
        }
        if (!empty($priceMin) && !empty($priceMax)) {
            $where .= " (p.PRICE BETWEEN $priceMin AND $priceMax) AND";
        }
        if ((isset($bedsMin) && !empty($bedsMin)) || (isset($bedsMax) && !empty($bedsMax))) {
            $where .= " (p.BEDROOMS BETWEEN $bedsMin AND $bedsMax) AND";
        } elseif (isset($bedsMin) && isset($bedsMax) && ($bedsMin == 0) && ($bedsMax == 0)) {
            $where .= " (p.BEDROOMS = 0) AND";
        }
        //print_r($where);die;
        if (!empty($bathMin) && !empty($bathMax)) {
            $where .= " (p.BATHROOMS BETWEEN $bathMin AND $bathMax) AND";
        }
        if (!empty($sfMin) && !empty($sfMax)) {
            $where .= " (p.SQUARE_FOOTAGE BETWEEN $sfMin AND $sfMax) AND";
        }
        $amenitiesVal = "";
        if (!empty($amenities)) {
            $amenitiesVal = implode("','", $amenities);
            //$where .= " pa.amenity IN ('$amenities') AND";
        }
        if (!empty($ADDRESS)) {
            $where .= " (p.ADDRESS LIKE '%$ADDRESS%') AND";
        }
        if (!empty($DATE_AVAILABLE)) {
            if ($DATE_AVAILABLE == 'VACANT')
                $where .= " (p.ACCESS_NOTE = '$DATE_AVAILABLE') AND";
            else
                $where .= " (p.DATE_AVAILABLE = '$DATE_AVAILABLE' AND p.DATE_AVAILABLE != '0000-00-00') AND";
        }
        if (!empty($CONTACT_COMPANY)) {
            // $where .= " (pc.CONTACT_COMPANY = '$CONTACT_COMPANY') AND";
            $where .= " pc.CONTACT_COMPANY IN($CONTACT_COMPANY) AND";
        }

        if (!empty($area)) {
            // $where .= " (p.NEIGHBORHOODS LIKE '%$area%') AND";
//            $area_decode = json_decode($area);
            //$areaVal = implode(',',$area);
//            $where .= " p.NEIGHBORHOODS IN('$areaVal') AND";
            $where .= " p.NEIGHBORHOODS IN($area) AND";
        }
        if (!empty($ACTUAL_PHOTOS) && ($ACTUAL_PHOTOS == 1)) {
            $where .= " (pl.name = 'Actual Photos' ) AND";
        }

        if (!empty($where)) {
            $where = substr($where, 0, (strrpos($where, " AND")));
        }
        $properties = "";
        $new_per_page = $per_page + 1;
        if (empty($type)) {
            if (empty($where) && empty($amenitiesVal)) {
                $properties = $this->Custom->query("SELECT DISTINCT p.propertyID, p.id, p.PRICE, p.BEDROOMS, p.BATHROOMS, p.ADDRESS, p.FEE, p.AGENT_PHONE, p.AGENT_MOBILE, p.AGENT_NAME, p.AGENT_EMAIL, p.LISTING_TITLE, p.LATITUDE, p.LONGITUDE, p.ACCESS_NOTE, pc.CONTACT_COMPANY FROM properties as p LEFT JOIN property_contacts as pc ON p.propertyID = pc.propertyID LEFT JOIN property_label as pl ON p.propertyID = pl.propertyID where p.is_deleted = 0 LIMIT $offset,$new_per_page");
              
            } else {
                $propertyIDArr = array();
                $propertyIDval = "";
                if ($amenitiesVal) {
                    $GetAminity = $this->Custom->query("select DISTINCT propertyID from property_amenities where amenity IN ('$amenitiesVal')");
                    if ($GetAminity) {
                        foreach ($GetAminity as $row) {
                            $propertyIDArr[] = $row->propertyID;
                        }
                        $propertyIDval = implode("','", $propertyIDArr);
                    }
                }

                if ($propertyIDArr)
                    $properties = $this->Custom->query("SELECT DISTINCT p.propertyID, p.id, p.PRICE, p.BEDROOMS, p.BATHROOMS, p.ADDRESS, p.FEE, p.AGENT_PHONE,p.DATE_UPDATE, p.AGENT_MOBILE, p.AGENT_NAME, p.AGENT_EMAIL, p.LISTING_TITLE, p.LATITUDE, p.LONGITUDE, p.ACCESS_NOTE, pc.CONTACT_COMPANY FROM properties as p LEFT JOIN property_contacts as pc ON p.propertyID = pc.propertyID LEFT JOIN property_label as pl ON p.propertyID = pl.propertyID WHERE $where AND p.propertyID IN ('$propertyIDval') AND p.is_deleted = 0 ORDER BY $sort $order LIMIT $offset,$new_per_page");
                else if (!empty($where) && empty($amenitiesVal))
                    $properties = $this->Custom->query("SELECT DISTINCT p.propertyID, p.id, p.PRICE,p.NEIGHBORHOODS, p.ACCESS_NOTE, p.BEDROOMS, p.BATHROOMS, p.ADDRESS, p.FEE, p.AGENT_PHONE,p.DATE_UPDATE, p.AGENT_MOBILE, p.AGENT_NAME, p.AGENT_EMAIL, p.LISTING_TITLE, p.LATITUDE, p.LONGITUDE, p.ACCESS_NOTE, pc.CONTACT_COMPANY FROM properties as p LEFT JOIN property_contacts as pc ON p.propertyID = pc.propertyID LEFT JOIN property_label as pl ON p.propertyID = pl.propertyID WHERE $where AND  p.is_deleted = 0 ORDER BY $sort $order LIMIT $offset,$new_per_page");
                else
                    $properties = "";
            }
        } else {
            if (!empty($LATITUDE) && !empty($LONGITUDE) && $type == 'map') {
                //$properties = $this->Custom->query("SELECT DISTINCT p.propertyID, p.id, p.PRICE, p.BEDROOMS, p.BATHROOMS, p.ADDRESS, p.FEE, p.AGENT_PHONE, p.AGENT_MOBILE, p.AGENT_NAME, p.AGENT_EMAIL, p.LISTING_TITLE, p.LATITUDE, p.LONGITUDE, ( 3959 * acos ( cos ( radians($LATITUDE) ) * cos( radians( LATITUDE ) ) * cos( radians( LONGITUDE ) - radians($LONGITUDE) ) + sin ( radians($LATITUDE) ) * sin( radians( LATITUDE ) ) ) ) AS `distance` FROM properties as p LEFT JOIN property_amenities as pa ON p.propertyID = pa.propertyID having distance <= 5 LIMIT $offset,$new_per_page");
                $properties = $this->Custom->query("SELECT DISTINCT p.propertyID, p.id, p.PRICE, p.BEDROOMS, p.BATHROOMS, p.ADDRESS, p.FEE, p.AGENT_PHONE, p.AGENT_MOBILE, p.AGENT_NAME, p.AGENT_EMAIL, p.ACCESS_NOTE, p.LISTING_TITLE, p.LATITUDE, p.LONGITUDE, ( 3959 * acos ( cos ( radians($LATITUDE) ) * cos( radians( LATITUDE ) ) * cos( radians( LONGITUDE ) - radians($LONGITUDE) ) + sin ( radians($LATITUDE) ) * sin( radians( LATITUDE ) ) ) ) AS `distance` FROM properties as p having distance <= 5 where p.is_deleted = 0");
            } else {
                //$properties = $this->Custom->query("SELECT DISTINCT p.propertyID, p.id, p.PRICE, p.BEDROOMS, p.BATHROOMS, p.ADDRESS, p.FEE, p.AGENT_PHONE, p.AGENT_MOBILE, p.AGENT_NAME, p.AGENT_EMAIL, p.LISTING_TITLE, p.LATITUDE, p.LONGITUDE FROM properties as p LEFT JOIN property_amenities as pa ON p.propertyID = pa.propertyID LEFT JOIN property_label as pl ON p.propertyID = pl.propertyID LIMIT $offset,$new_per_page");
                $properties = $this->Custom->query("SELECT DISTINCT p.propertyID, p.id, p.PRICE, p.BEDROOMS, p.BATHROOMS, p.ADDRESS, p.FEE, p.AGENT_PHONE, p.AGENT_MOBILE, p.AGENT_NAME, p.AGENT_EMAIL, p.LISTING_TITLE, p.LATITUDE, p.LONGITUDE, p.ACCESS_NOTE FROM properties as p LEFT JOIN property_label as pl ON p.propertyID = pl.propertyID WHERE  p.is_deleted = 0");
            }
        }
        $Totalcount = 0;
        //print_r($properties);die;
        $propertiesArr = array();
        if (isset($properties) && !empty($properties)) {
            $Totalcount = count($properties);
            $FilterRec = $per_page - 1;
            foreach ($properties as $key => $value) {
            	//print_r($value);die;
                if ($key <= $FilterRec) {
                    //$sortArr[] = $value->$sort;
                    $sortArr[] = $value->PRICE;
                    if (!empty($user_id)) {
                        $check_favourite_property = $this->Custom->get_where('liked_properties', array('propertyID' => $value->propertyID, 'user_id' => $user_id));
                        if (isset($check_favourite_property) && !empty($check_favourite_property)) {
                            $value->isLiked = 1;
                        } else {
                            $value->isLiked = 0;
                        }
                    } else {
                        $value->isLiked = 0;
                    }
                    $images = $this->Custom->get_data('property_images', 'propertyID', $value->propertyID);
                    $property_amenities = $this->Custom->get_data('property_amenities', 'propertyID', $value->propertyID);
                    $amenities = array();
                    $value->distance = (isset($value->distance) && !empty($value->distance)) ? $value->distance : '';
                    $value->CONTACT_COMPANY = (isset($value->CONTACT_COMPANY) && !empty($value->CONTACT_COMPANY)) ? $value->CONTACT_COMPANY : '';
                    $properties[$key]->PHOTO_URL = (isset($images[0]->PHOTO_URL) && !empty($images[0]->PHOTO_URL)) ? $images[0]->PHOTO_URL : '';
                    if (isset($property_amenities) && !empty($property_amenities)) {
                        foreach ($property_amenities as $k => $val) {
                            $amenities[] = $val->amenity;
                        }
                        $properties[$key]->AMENITIES = implode(",", $amenities);
                    } else {
                        $properties[$key]->AMENITIES = '';
                    }
                    $propertiesArr[] = $properties[$key];
                }
            }
            if ($order == 'asc'){
                array_multisort($sortArr, SORT_ASC, $propertiesArr);
            }
            else{
                array_multisort($sortArr, SORT_DESC, $propertiesArr);
            }
            $this->response->success = 200;
            $this->response->NextPage = ($Totalcount > $per_page) ? 1 : 0;
            $this->response->message = "Properties list";
            $this->response->properties = $propertiesArr;
            die(json_encode($this->response));
        } else {
            $this->response->success = 203;
            $this->response->message = "No properties found.";
            die(json_encode($this->response));
        }
        /* } else {
          $this->response->success = 203;
          $this->response->message = $this->lang->line('invalid_user_id');
          die(json_encode($this->response));
          }
          } else {
          $this->response->success = 201;
          $this->response->message = $this->lang->line('required_field_error');
          die(json_encode($this->response));
          } */
    }

    public function GetPropertyDetails() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';
        $property_id = ($this->input->post('property_id')) ? $this->input->post('property_id') : '';

         $data = $this->Custom->get_where('property_video', array('propertyID' => $property_id,'user_id' => $user_id));

        if (!empty($property_id)) {
            if (!empty($user_id)) {
                $user_record = $this->Custom->get_data('users', 'id', $user_id);
                if (isset($user_record) && !empty($user_record)) {
                    //check authentication
                    $headers = apache_request_headers();
                    $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                    if (empty($auth_record)) {
                        $this->response->success = 404;
                        $this->response->message = $this->lang->line('authentication_failed');
                        die(json_encode($this->response));
                    }
                }
                 $VideosCount = $this->Custom->get_where('property_video', array('propertyID' => $property_id));
            }else{

            	 $VideosCount = $this->Custom->get_where('property_video', array('propertyID' => $property_id));

            }

            $properties = $this->Custom->get_data('properties', 'propertyID', $property_id);
            if (isset($properties) && !empty($properties)) {
                foreach ($properties as $key => $value) {
                    $value->BUILDING_AGENTS_NOTE = preg_replace('/[\r\n]/', '', $value->BUILDING_AGENTS_NOTE);
                    if (!empty($user_id)) {
                        $check_favourite_property = $this->Custom->get_where('liked_properties', array('propertyID' => $value->propertyID, 'user_id' => $user_id));
                        if (isset($check_favourite_property) && !empty($check_favourite_property)) {
                            $properties[$key]->isLiked = 1;
                        } else {
                            $properties[$key]->isLiked = 0;
                        }
                    } else {
                        $properties[$key]->isLiked = 0;
                    }
                    $images = $this->Custom->get_data('property_images', 'propertyID', $value->propertyID);
                    $property_amenities = $this->Custom->get_data('property_amenities', 'propertyID', $value->propertyID);
                    $amenities = array();
                    if (isset($images) && !empty($images)) {
                        $properties[$key]->images = $images;
                    } else {
                        $properties[$key]->images = array();
                    }
                    if (isset($property_amenities) && !empty($property_amenities)) {
                        foreach ($property_amenities as $k => $val) {
                            $amenities[] = $val->amenity;
                        }
                        $properties[$key]->AMENITIES = implode(",", $amenities);
                    } else {
                        $properties[$key]->AMENITIES = '';
                    }
                }
               
                $this->response->success = 200;
                $this->response->message = "Property details";
                $this->response->properties = $properties[0];
                $this->response->countvideo= (!empty($VideosCount)) ? count(  $VideosCount ) : 0;
                die(json_encode($this->response));
            } else {
                $this->response->success = 203;
                $this->response->message = "No property found.";
                die(json_encode($this->response));
            }
            /* } else {
              $this->response->success = 203;
              $this->response->message = $this->lang->line('invalid_user_id');
              die(json_encode($this->response));
              } */
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function GetFavouriteProperties() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';

        if (!empty($user_id)) {
            $user_record = $this->Custom->get_data('users', 'id', $user_id);
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }
                $fav_properties = $this->Custom->get_data('liked_properties', 'user_id', $user_id);
                $properties = array();
                if (isset($fav_properties) && !empty($fav_properties)) {
                    foreach ($fav_properties as $key => $value) {
                        $property = "";
                        $property = $this->Custom->query("SELECT DISTINCT p.propertyID, p.id, p.PRICE, p.BEDROOMS, p.BATHROOMS, p.ADDRESS, p.FEE, p.AGENT_PHONE, p.AGENT_MOBILE, p.AGENT_NAME, p.AGENT_EMAIL, p.LISTING_TITLE, p.LATITUDE, p.LONGITUDE FROM properties as p LEFT JOIN property_amenities as pa ON p.propertyID = pa.propertyID where p.propertyID = " . $value->propertyID . " AND p.is_deleted = 0");
                        if (isset($property) && !empty($property)) {
                            $properties[] = $property[0];
                        }
                    }
                    if (isset($properties) && !empty($properties)) {
                        foreach ($properties as $key => $value) {
                            $value->isLiked = 1;
                            $images = $this->Custom->get_data('property_images', 'propertyID', $value->propertyID);
                            $property_amenities = $this->Custom->get_data('property_amenities', 'propertyID', $value->propertyID);
                            $amenities = array();
                            $value->distance = (isset($value->distance) && !empty($value->distance)) ? $value->distance : '';
                            $properties[$key]->PHOTO_URL = (isset($images[0]->PHOTO_URL) && !empty($images[0]->PHOTO_URL)) ? $images[0]->PHOTO_URL : '';
                            if (isset($property_amenities) && !empty($property_amenities)) {
                                foreach ($property_amenities as $k => $val) {
                                    $amenities[] = $val->amenity;
                                }
                                $properties[$key]->AMENITIES = implode(",", $amenities);
                            } else {
                                $properties[$key]->AMENITIES = '';
                            }
                        }

                        $this->response->success = 200;
                        $this->response->message = "Properties list";
                        $this->response->properties = $properties;
                        die(json_encode($this->response));
                    } else {
                        $this->response->success = 203;
                        $this->response->message = "No properties found.";
                        die(json_encode($this->response));
                    }
                } else {
                    $this->response->success = 203;
                    $this->response->message = "No properties found.";
                    die(json_encode($this->response));
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function scheduleAppointment() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';
        $property_id = ($this->input->post('property_id')) ? $this->input->post('property_id') : '';
        $datetime = ($this->input->post('appointment_datetime')) ? $this->input->post('appointment_datetime') : '';
        $description = ($this->input->post('description')) ? $this->input->post('description') : '';

        if (!empty($user_id) && !empty($datetime) && !empty($property_id)) {
            $user_record = $this->Custom->get_data('users', 'id', $user_id);
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }

                $created = date("Y-m-d H:i:s");
                $dataArr = array(
                    'user_id' => $user_id,
                    'property_id' => $property_id,
                    'appointment_datetime' => $datetime,
                    'description' => $description,
                    'agent_id' => 0,
                    'status' => 0,
                    'created' => $created
                );
                $insert_id = $this->Custom->insert_data('appointments', $dataArr);
                if ($insert_id) {
                    $name = $user_record[0]->first_name . ' ' . $user_record[0]->last_name;
                    $message = $name . ' has requested for an appointment at ' . $datetime;
                    $this->createNotification($user_id, $property_id, 'ScheduleAppointment', $insert_id, $message);
                    $this->response->success = 200;
                    $this->response->message = "Appointment scheduled";
                    die(json_encode($this->response));
                } else {
                    $this->response->success = 203;
                    $this->response->message = $this->lang->line('something_went_wrong');
                    die(json_encode($this->response));
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function GetAppointmentRequests() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';

        if (!empty($user_id)) {
            $user_record = $this->Custom->get_data('users', 'id', $user_id);
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }
                $date = date("Y-m-d H:i:s");
                $appointments = $this->Custom->query("SELECT u.first_name, u.last_name, u.profile_pic, a.* FROM appointments as a LEFT JOIN users as u ON a.user_id = u.id where a.appointment_datetime > '$date' AND a.is_agent_assigned = 0 and a.user_id != 0 ORDER BY appointment_datetime ASC");
                //$brands = $this->Custom->get_where('brands', array('status' => 1, 'delete_status' => 0));
                if (isset($appointments) && !empty($appointments)) {
                    $appointmentsArr = array();
                    foreach ($appointments as $key => $value) {
                        $is_blocked = $this->Custom->get_where('rejected_appointment_requests', array('appointment_id' => $value->id, 'user_id' => $user_id));
                        if (empty($is_blocked)) {
                            $appointmentsArr[] = $value;
                        }
                        $is_blocked = '';
                    }
                    if (!empty($appointmentsArr)) {
                        $this->response->success = 200;
                        $this->response->message = "Appointments list";
                        $this->response->data = $appointmentsArr;
                        die(json_encode($this->response));
                    } else {
                        $this->response->success = 203;
                        $this->response->message = "No appointment found.";
                        die(json_encode($this->response));
                    }
                } else {
                    $this->response->success = 203;
                    $this->response->message = "No appointment found.";
                    die(json_encode($this->response));
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function acceptAppointmentRequest() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';
        $appointment_id = ($this->input->post('appointment_id')) ? $this->input->post('appointment_id') : '';

        if (isset($user_id) && !empty($user_id) && isset($appointment_id) && !empty($appointment_id)) {
            $user_record = $this->Custom->get_where('users', array('id' => $user_id));
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }

                $checkAppointmentStatus = $this->Custom->get_where("appointments", array('id' => $appointment_id, 'is_agent_assigned' => 1));
                if (isset($checkAppointmentStatus) && !empty($checkAppointmentStatus)) {
                    $this->response->success = 203;
                    $this->response->message = "Appointment accepted by other agent.";
                    die(json_encode($this->response));
                }

                $checkAppointment = $this->Custom->get_where("appointments", array('id' => $appointment_id, 'status' => 0));
                if (isset($checkAppointment) && !empty($checkAppointment)) {
                    if ($this->Custom->update('appointments', array('status' => 1, 'is_agent_assigned' => 1, 'agent_id' => $user_id), 'id', $appointment_id)) {
                        $name = $user_record[0]->first_name . ' ' . $user_record[0]->last_name;
                        $message = $name . ' has accepted your appointment.';
                        $this->createNotification($checkAppointment[0]->user_id, $checkAppointment[0]->property_id, 'AcceptAppointment', $appointment_id, $message);

                        $this->response->success = 200;
                        $this->response->message = "Appointment accepted.";
                        die(json_encode($this->response));
                    } else {
                        $this->response->success = 203;
                        $this->response->message = "Appointment not accepted.";
                        die(json_encode($this->response));
                    }
                } else {
                    $this->response->success = 203;
                    $this->response->message = "Appointment not found.";
                    die(json_encode($this->response));
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function rejectAppointmentRequest() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';
        $appointment_id = ($this->input->post('appointment_id')) ? $this->input->post('appointment_id') : '';

        if (isset($user_id) && !empty($user_id) && isset($appointment_id) && !empty($appointment_id)) {
            $user_record = $this->Custom->get_where('users', array('id' => $user_id));
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }

                $created = date("Y-m-d H:i:s");
                $checkAppointment = $this->Custom->get_where("appointments", array('id' => $appointment_id, 'status' => 0));
                if (isset($checkAppointment) && !empty($checkAppointment)) {
                    $insert_id = $this->Custom->insert_data('rejected_appointment_requests', array('status' => 1, 'appointment_id' => $appointment_id, 'user_id' => $user_id, 'created' => $created));
                    if ($insert_id) {
                        $this->response->success = 200;
                        $this->response->message = "Appointment rejected.";
                        die(json_encode($this->response));
                    } else {
                        $this->response->success = 203;
                        $this->response->message = "Appointment not rejected.";
                        die(json_encode($this->response));
                    }
                } else {
                    $this->response->success = 203;
                    $this->response->message = "Appointment not found.";
                    die(json_encode($this->response));
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function MyAppointments() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';

        if (!empty($user_id)) {
            $user_record = $this->Custom->get_data('users', 'id', $user_id);
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }
                $date = date("Y-m-d H:i:s");
                $appointments = $this->Custom->query("SELECT u.first_name, u.last_name, u.profile_pic, u.email, u.phone_number, a.* FROM appointments as a LEFT JOIN users as u ON a.agent_id = u.id where a.user_id = $user_id AND a.is_agent_assigned = 1 ORDER BY appointment_datetime ASC");
                //$brands = $this->Custom->get_where('brands', array('status' => 1, 'delete_status' => 0));
                if (isset($appointments) && !empty($appointments)) {
                    $this->response->success = 200;
                    $this->response->message = "Appointments list";
                    $this->response->data = $appointments;
                    die(json_encode($this->response));
                } else {
                    $this->response->success = 203;
                    $this->response->message = "No appointment found.";
                    die(json_encode($this->response));
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function agentAppointments() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';

        if (!empty($user_id)) {
            $user_record = $this->Custom->get_data('users', 'id', $user_id);
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }
                $date = date("Y-m-d H:i:s");
                $appointments = $this->Custom->query("SELECT u.first_name, u.last_name, u.profile_pic, u.email, u.phone_number, a.* FROM appointments as a LEFT JOIN users as u ON a.user_id = u.id where a.agent_id = $user_id AND (a.is_agent_assigned = 1 OR a.user_id = 0) ORDER BY appointment_datetime ASC");
                foreach ($appointments as $row) {
                    if ($row->property_id) {
                        $property_det = $this->Custom->get_where('properties', array('propertyID' => $row->property_id,'is_deleted' => 0));
                        if ($property_det)
                            $row->property_address = $property_det[0]->ADDRESS;
                    }else {
                        $row->property_address = "";
                    }
                }
                //$brands = $this->Custom->get_where('brands', array('status' => 1, 'delete_status' => 0));
                if (isset($appointments) && !empty($appointments)) {
                    $this->response->success = 200;
                    $this->response->message = "Appointments list";
                    $this->response->data = $appointments;
                    die(json_encode($this->response));
                } else {
                    $this->response->success = 203;
                    $this->response->message = "No appointment found.";
                    die(json_encode($this->response));
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function getAmenitiesList() {
        $amenities = $this->Custom->query("SELECT DISTINCT amenity FROM `property_amenities` WHERE amenity != '' ORDER BY amenity ASC");
        if (isset($amenities) && !empty($amenities)) {
            $amenitiesVal = array();
            $amenitiesArr = array('Light', 'City View', 'Lake View', 'Brownstone', 'Original Details', 'Pet Friendly');
            foreach ($amenities as $key => $value) {
                if (!in_array($value->amenity, $amenitiesArr))
                    $amenitiesVal[] = $value;
            }
            array_push($amenitiesVal, array("amenity" => "Pet Friendly"));
            $this->response->success = 200;
            $this->response->message = "Amenities list";
            $this->response->data = $amenitiesVal;
            die(json_encode($this->response));
        } else {
            $this->response->success = 203;
            $this->response->message = "No amenity found.";
            die(json_encode($this->response));
        }
    }

    public function getPropertyTypes() {
        $propertyTypes = $this->Custom->query("SELECT DISTINCT PROPERTY_TYPE FROM `properties` WHERE PROPERTY_TYPE != '' AND is_deleted = 0 ORDER BY PROPERTY_TYPE ASC");
        if (isset($propertyTypes) && !empty($propertyTypes)) {
            $this->response->success = 200;
            $this->response->message = "Property types list";
            $this->response->data = $propertyTypes;
            die(json_encode($this->response));
        } else {
            $this->response->success = 203;
            $this->response->message = "No property type found.";
            die(json_encode($this->response));
        }
    }

    public function likeDislikeProperty() {
        $user_id = $this->input->post('user_id', TRUE);
        $propertyID = $this->input->post('propertyID', TRUE);

        if (isset($user_id) && !empty($user_id) && isset($propertyID) && !empty($propertyID)) {
            $user_record = $this->Custom->get_data('users', 'id', $user_id);
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }
                $_POST['created'] = date("Y-m-d H:i:s");
                $_POST['status'] = 1;

                $check_favourite_property = $this->Custom->get_where('liked_properties', array('propertyID' => $propertyID, 'user_id' => $user_id));
                if (isset($check_favourite_property) && !empty($check_favourite_property)) {
                    $this->Custom->delete_where('liked_properties', array('user_id' => $user_id, 'propertyID' => $propertyID));
                    $this->response->success = 200;
                    $this->response->message = "Property disliked.";
                    $this->response->error_type = "";
                    die(json_encode($this->response));
                } else {
                    $insert_id = $this->Custom->insert_data('liked_properties', $_POST);
                    if (isset($insert_id) && !empty($insert_id)) {
                        $this->response->success = 200;
                        $this->response->message = "Property liked.";
                        die(json_encode($this->response));
                    } else {
                        $this->response->success = 204;
                        $this->response->message = "Failed to perform like operation.";
                        $this->response->error_type = "failed";
                        die(json_encode($this->response));
                    }
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function userNotifications() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';

        if (!empty($user_id)) {
            $user_record = $this->Custom->get_data('users', 'id', $user_id);
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }
                $date = date("Y-m-d H:i:s");
                $notifications = $this->Custom->query("SELECT * FROM notifications where user_id = $user_id AND (type = 'AcceptAppointment' OR type = 'ScheduleAgentAppointment') ORDER BY created DESC");
                //$notifications = $this->Custom->get_where('notifications', array('user_id' => $user_id, 'type' => ''));
                if (isset($notifications) && !empty($notifications)) {
                    foreach ($notifications as $key => $value) {
                        $appointment_id = $value->appointment_id;
                        $appointment = $this->Custom->get_where('appointments', array('id' => $appointment_id));
                        if (isset($appointment) && !empty($appointment)) {
                            $agent_id = $appointment[0]->agent_id;
                            $user_data = $this->Custom->query("SELECT id, first_name, last_name, profile_pic FROM users WHERE id = $agent_id");
                            if (isset($user_data[0]->profile_pic) && !empty($user_data[0]->profile_pic)) {
                                if (preg_match("/http/i", $user_data[0]->profile_pic)) {
                                    $user_data[0]->profile_pic = $user_data[0]->profile_pic;
                                } else {
                                    $user_data[0]->profile_pic = ($user_data[0]->profile_pic) ? base_url('Assets/uploads/license_images/' . $user_data[0]->profile_pic) : "";
                                }
                            } else {
                                $user_data[0]->profile_pic = '';
                            }
                            $value->user_data = (isset($user_data) && !empty($user_data)) ? $user_data[0] : new stdClass();
                        } else {
                            $value->user_data = new stdClass();
                        }
                    }
                    $this->response->success = 200;
                    $this->response->message = "Notifications list";
                    $this->response->data = $notifications;
                    die(json_encode($this->response));
                } else {
                    $this->response->success = 203;
                    $this->response->message = "No notifications found.";
                    die(json_encode($this->response));
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function agentNotifications() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';

        if (!empty($user_id)) {
            $user_record = $this->Custom->get_data('users', 'id', $user_id);
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }
                $date = date("Y-m-d H:i:s");
                $notifications = $this->Custom->query("SELECT * FROM notifications where type = 'ScheduleAppointment' ORDER BY created DESC");
                //$notifications = $this->Custom->query("SELECT * FROM notifications where type = 'ScheduleAppointment' ORDER BY created DESC");
                if (isset($notifications) && !empty($notifications)) {
                    foreach ($notifications as $key => $value) {
                        $agent_id = $value->user_id;
                        $user_data = $this->Custom->query("SELECT id, first_name, last_name, profile_pic FROM users WHERE id = $agent_id");
                        if (isset($user_data[0]->profile_pic) && !empty($user_data[0]->profile_pic)) {
                            if (preg_match("/http/i", $user_data[0]->profile_pic)) {
                                $user_data[0]->profile_pic = $user_data[0]->profile_pic;
                            } else {
                                $user_data[0]->profile_pic = ($user_data[0]->profile_pic) ? base_url('Assets/uploads/license_images/' . $user_data[0]->profile_pic) : "";
                            }
                        } else {
                            $user_data[0]->profile_pic = '';
                        }
                        $value->user_data = (isset($user_data) && !empty($user_data)) ? $user_data[0] : new stdClass();
                    }
                    $this->response->success = 200;
                    $this->response->message = "Notifications list";
                    $this->response->data = $notifications;
                    die(json_encode($this->response));
                } else {
                    $this->response->success = 203;
                    $this->response->message = "No notifications found.";
                    die(json_encode($this->response));
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function scheduleTenantAppointment() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';
        //$tenant_id = ($this->input->post('tenant_id')) ? $this->input->post('tenant_id') : '';
        $property_id = ($this->input->post('property_id')) ? $this->input->post('property_id') : '';
        $datetime = ($this->input->post('appointment_datetime')) ? $this->input->post('appointment_datetime') : '';
        $description = ($this->input->post('description')) ? $this->input->post('description') : '';

        if (!empty($user_id) && !empty($datetime) && !empty($property_id)) {
            $user_record = $this->Custom->get_data('users', 'id', $user_id);
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }

                $created = date("Y-m-d H:i:s");
                $dataArr = array(
                    'property_id' => $property_id,
                    'appointment_datetime' => $datetime,
                    'description' => $description,
                    'agent_id' => $user_id,
                    'status' => 1,
                    'created' => $created
                );
                $insert_id = $this->Custom->insert_data('appointments', $dataArr);
                if ($insert_id) {
//                    $name = $user_record[0]->first_name . ' ' . $user_record[0]->last_name;
//                    $message = $name . ' has scheduled an appointment at ' . $datetime;
//                    $this->createNotification($tenant_id, $property_id, 'ScheduleAgentAppointment', $insert_id, $message);
                    $this->response->success = 200;
                    $this->response->message = "Appointment scheduled";
                    die(json_encode($this->response));
                } else {
                    $this->response->success = 203;
                    $this->response->message = $this->lang->line('something_went_wrong');
                    die(json_encode($this->response));
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function scheduleTenantAppointmentWITHTENANT() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';
        $tenant_id = ($this->input->post('tenant_id')) ? $this->input->post('tenant_id') : '';
        $property_id = ($this->input->post('property_id')) ? $this->input->post('property_id') : '';
        $datetime = ($this->input->post('appointment_datetime')) ? $this->input->post('appointment_datetime') : '';
        $description = ($this->input->post('description')) ? $this->input->post('description') : '';

        if (!empty($user_id) && !empty($datetime) && !empty($property_id) && !empty($tenant_id)) {
            $user_record = $this->Custom->get_data('users', 'id', $user_id);
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }

                $created = date("Y-m-d H:i:s");
                $dataArr = array(
                    'user_id' => $tenant_id,
                    'property_id' => $property_id,
                    'appointment_datetime' => $datetime,
                    'description' => $description,
                    'agent_id' => $user_id,
                    'status' => 1,
                    'created' => $created
                );
                $insert_id = $this->Custom->insert_data('appointments', $dataArr);
                if ($insert_id) {
                    $name = $user_record[0]->first_name . ' ' . $user_record[0]->last_name;
                    $message = $name . ' has scheduled an appointment at ' . $datetime;
                    $this->createNotification($tenant_id, $property_id, 'ScheduleAgentAppointment', $insert_id, $message);
                    $this->response->success = 200;
                    $this->response->message = "Appointment scheduled";
                    die(json_encode($this->response));
                } else {
                    $this->response->success = 203;
                    $this->response->message = $this->lang->line('something_went_wrong');
                    die(json_encode($this->response));
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function createNotification($user_id, $property_id, $type, $appointment_id, $message) {
        $created = date("Y-m-d H:i:s");
        $dataArr = array(
            'user_id' => $user_id,
            'property_id' => $property_id,
            'type' => $type,
            'appointment_id' => $appointment_id,
            'message' => $message,
            'is_seen' => 0,
            'status' => 1,
            'created' => $created
        );
        $this->Custom->insert_data('notifications', $dataArr);
    }

    public function GetAllBrands() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';

        if (!empty($user_id)) {
            $user_record = $this->Custom->get_data('users', 'id', $user_id);
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }

                $brands = $this->Custom->get_where('brands', array('status' => 1, 'delete_status' => 0));
                if (isset($brands) && !empty($brands)) {
                    $this->response->success = 200;
                    $this->response->message = "Brands list";
                    $this->response->data = $brands;
                    die(json_encode($this->response));
                } else {
                    $this->response->success = 203;
                    $this->response->message = "No brands found.";
                    die(json_encode($this->response));
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function GetAllModelsByBrand() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';
        $brand_id = ($this->input->post('brand_id')) ? $this->input->post('brand_id') : '';

        if (!empty($user_id) && !empty($brand_id)) {
            $user_record = $this->Custom->get_data('users', 'id', $user_id);
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }

                $models = $this->Custom->get_where('models', array('brand_id' => $brand_id, 'status' => 1, 'delete_status' => 0));
                if (isset($models) && !empty($models)) {
                    $this->response->success = 200;
                    $this->response->message = "Models list";
                    $this->response->data = $models;
                    die(json_encode($this->response));
                } else {
                    $this->response->success = 203;
                    $this->response->message = "No models found.";
                    die(json_encode($this->response));
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function UpdateProfile() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';
        $first_name = ($this->input->post('name')) ? $this->input->post('name') : '';
        $address = ($this->input->post('address')) ? $this->input->post('address') : '';
        $device_type = ($this->input->post('device_type')) ? $this->input->post('device_type') : '';
        $device_token = ($this->input->post('device_token')) ? $this->input->post('device_token') : '';
        $role = ($this->input->post('role')) ? $this->input->post('role') : '';

        if (!empty($user_id)) {
            $user_record = $this->Custom->get_data('users', 'id', $user_id);
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }

                /* if ($user_record[0]->status != 1) {
                  $this->response->success = 204;
                  $this->response->message = $this->lang->line('account_not_activate');
                  die(json_encode($this->response));
                  } */

                unset($_POST['user_id']);
                $update_res = $this->Custom->update("users", $_POST, 'id', $user_id);

                if ($update_res) {
                    $user_details = $this->Custom->get_data('users', 'id', $user_id);
                    $user_details[0]->profile_pic = ($user_details[0]->profile_pic) ? $user_details[0]->profile_pic : "";

                    $this->response->success = 200;
                    $this->response->message = $this->lang->line('profile_updated');
                    $this->response->data = $user_details[0];
                    die(json_encode($this->response));
                } else {
                    $this->response->success = 202;
                    $this->response->message = $this->lang->line('something_went_wrong');
                    die(json_encode($this->response));
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function UpdateProfileImage() {
        include '/var/www/html/backend/aws_sdk/index.php';
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';

        if (!empty($user_id)) {
            $user_record = $this->Custom->get_data('users', 'id', $user_id);
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }

                /* if ($user_record[0]->status != 1) {
                  $this->response->success = 204;
                  $this->response->message = $this->lang->line('account_not_activate');
                  die(json_encode($this->response));
                  } */

                $profile_pic = (isset($user_record[0]->profile_pic) && !empty($user_record[0]->profile_pic)) ? $user_record[0]->profile_pic : '';
                //upload profile image
                if (empty($_FILES['profile_pic']['name'])) {
                    $this->response->success = 201;
                    $this->response->message = $this->lang->line('required_field_error');
                    die(json_encode($this->response));
                }

                if (!empty($logo)) {
                    $new_name = time() . '_' . $_FILES["profile_pic"]['name'];
                    $bucket = 'cloudcarhire';

                    $result = $s3->putObject(
                            array(
                                'Bucket' => $bucket,
                                'Key' => $new_name,
                                'SourceFile' => $_FILES['profile_pic']['tmp_name'],
                                'ContentType' => 'text/plain',
                                'ACL' => 'public-read'
                            )
                    );
                    if (isset($result['ObjectURL']) && !empty($result['ObjectURL'])) {
                        $url = $result['ObjectURL'];
                        $profile_pic = $new_name;
                    } else {
                        $this->response->success = 203;
                        $this->response->message = $this->lang->line('image_not_upload');
                        die(json_encode($this->response));
                    }
                } else {
                    $this->response->success = 203;
                    $this->response->message = $this->lang->line('image_not_upload');
                    die(json_encode($this->response));
                }
                $updateProfileArr = array(
                    'profile_pic' => $profile_pic
                );
                $user_record[0]->profile_pic = $profile_pic;
                $update_res = $this->Custom->update("users", $updateProfileArr, 'id', $user_id);

                if ($update_res) {
                    $this->response->success = 200;
                    $this->response->message = $this->lang->line('profile_updated');
                    $this->response->data = $user_record[0];
                    die(json_encode($this->response));
                } else {
                    $this->response->success = 202;
                    $this->response->message = $this->lang->line('something_went_wrong');
                    die(json_encode($this->response));
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function CreateStripeCustomer() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';
        $token = ($this->input->post('token')) ? $this->input->post('token') : '';

        if (!empty($user_id) && isset($token) && !empty($token)) {
            $user_record = $this->Custom->get_data('users', 'id', $user_id);
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }
                try {
                    $customer = \Stripe\Customer::create(array(
                                "email" => $auth_record[0]->email,
                                "source" => $token
                    ));
                    if ($customer) {
                        if (isset($customer->id) && !empty($customer->id)) {
                            $updateArr = array(
                                'stripe_cus_id' => $customer->id
                            );
                            $update_res = $this->Custom->update('users', $updateArr, 'id', $user_id);
                            if ($update_res) {
                                $this->response->success = 200;
                                $this->response->message = $this->lang->line('stripe_customer_created');
                                $this->response->data = $customer->id;
                                die(json_encode($this->response));
                            } else {
                                $this->response->success = 202;
                                $this->response->message = $this->lang->line('something_went_wrong');
                                die(json_encode($this->response));
                            }
                        } else {
                            $this->response->success = 202;
                            $this->response->message = $this->lang->line('something_went_wrong');
                            die(json_encode($this->response));
                        }
                    } else {
                        $this->response->success = 202;
                        $this->response->message = $this->lang->line('something_went_wrong');
                        die(json_encode($this->response));
                    }
                } catch (\Stripe\Error\Card $e) {
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $error = $err['message'];
                    $this->response->success = 205;
                    $this->response->message = $error;
                    die(json_encode($this->response));
                } catch (\Stripe\Error\RateLimit $e) {
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $error = $err['message'];
                    $this->response->success = 205;
                    $this->response->message = $error;
                    die(json_encode($this->response));
                } catch (\Stripe\Error\InvalidRequest $e) {
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $error = $err['message'];
                    $this->response->success = 205;
                    $this->response->message = $error;
                    die(json_encode($this->response));
                } catch (\Stripe\Error\Authentication $e) {
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $error = $err['message'];
                    $this->response->success = 205;
                    $this->response->message = $error;
                    die(json_encode($this->response));
                } catch (\Stripe\Error\ApiConnection $e) {
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $error = $err['message'];
                    $this->response->success = 205;
                    $this->response->message = $error;
                    die(json_encode($this->response));
                } catch (\Stripe\Error\Base $e) {
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $error = $err['message'];
                    $this->response->success = 205;
                    $this->response->message = $error;
                    die(json_encode($this->response));
                } catch (Exception $e) {
                    $body = $e->getJsonBody();
                    $err = $body['error'];
                    $error = $err['message'];
                    $this->response->success = 205;
                    $this->response->message = $error;
                    die(json_encode($this->response));
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function uploadCar() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';
        $title = ($this->input->post('title')) ? $this->input->post('title') : '';
        $registration_number = ($this->input->post('registration_number')) ? $this->input->post('registration_number') : '';
        $brand = ($this->input->post('brand')) ? $this->input->post('brand') : '';
        $model = ($this->input->post('model')) ? $this->input->post('model') : '';
        $year = ($this->input->post('year')) ? $this->input->post('year') : '';
        $vehicle_type = ($this->input->post('vehicle_type')) ? $this->input->post('vehicle_type') : '';
        $transmission = ($this->input->post('transmission')) ? $this->input->post('transmission') : '';
        $no_of_doors = ($this->input->post('no_of_doors')) ? $this->input->post('no_of_doors') : '';
        $no_of_seats = ($this->input->post('no_of_seats')) ? $this->input->post('no_of_seats') : '';
        $fuel = ($this->input->post('fuel')) ? $this->input->post('fuel') : '';
        $car_value = ($this->input->post('car_value')) ? $this->input->post('car_value') : '';
        $description = ($this->input->post('description')) ? $this->input->post('description') : '';
        $price_per_day = ($this->input->post('price_per_day')) ? $this->input->post('price_per_day') : '';

        $primary_address = ($this->input->post('primary_address')) ? $this->input->post('primary_address') : '';
        $primary_lat = ($this->input->post('primary_lat')) ? $this->input->post('primary_lat') : '';
        $primary_lng = ($this->input->post('primary_lng')) ? $this->input->post('primary_lng') : '';
        $secondary_address = ($this->input->post('secondary_address')) ? $this->input->post('secondary_address') : '';
        $secondary_lat = ($this->input->post('secondary_lat')) ? $this->input->post('secondary_lat') : '';
        $secondary_lng = ($this->input->post('secondary_lng')) ? $this->input->post('secondary_lng') : '';

        $created_at = date('Y-m-d H:i:s');
        $primary_image = ($this->input->post('primary_image')) ? $this->input->post('primary_image') : '';
        $images = ($this->input->post('images')) ? urldecode($_POST['images']) : '';
        //$icon = ($this->input->post('icon')) ? $this->input->post('icon') : '';
        $accessories = ($this->input->post('accessories')) ? urldecode($_POST['accessories']) : '';
        //$type = ($this->input->post('type')) ? $this->input->post('type') : '';
        $amount = ($this->input->post('amount')) ? urldecode($_POST['amount']) : '';

        if (!empty($user_id) && !empty($registration_number) && !empty($brand) && !empty($model) && !empty($year) && !empty($vehicle_type) && !empty($transmission) && !empty($no_of_doors) && !empty($no_of_seats) && !empty($fuel) && !empty($car_value) && !empty($primary_address) && !empty($primary_lat) && !empty($primary_lng) && !empty($images)) {
            $user_record = $this->Custom->get_data('users', 'id', $user_id);
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }

                if ($user_record[0]->role != 'owner') {
                    $this->response->success = 203;
                    $this->response->message = "User role is not an owner";
                    die(json_encode($this->response));
                }

                $insertArr = array(
                    'user_id' => $user_id,
                    'title' => $title,
                    'description' => $description,
                    'registration_number' => $registration_number,
                    'brand' => $brand,
                    'model' => $model,
                    'price_per_day' => $price_per_day,
                    'year' => $year,
                    'vehicle_type' => $vehicle_type,
                    'transmission' => $transmission,
                    'no_of_doors' => $no_of_doors,
                    'no_of_seats' => $no_of_seats,
                    'fuel' => $fuel,
                    'car_value' => $car_value,
                    'primary_image' => $primary_image,
                    'created' => $created_at,
                    'updated' => $created_at,
                    'status' => 0
                );
                $insert_id = $this->Custom->insert_data('cars', $insertArr);
                if ($insert_id) {
                    //profile data
                    $EventOtherArr = array(
                        'user_id' => $user_id,
                        'car_id' => $insert_id,
                        'primary_address' => $primary_address,
                        'primary_lat' => $primary_lat,
                        'primary_lng' => $primary_lng,
                        'secondary_address' => $secondary_address,
                        'secondary_lat' => $secondary_lat,
                        'secondary_lng' => $secondary_lng,
                        'created' => $created_at,
                        'status' => 1
                    );
                    $this->Custom->insert_data('car_location', $EventOtherArr);
                    //upload cover image
                    if (!empty($images)) {
                        $images = json_decode($images);
                        foreach ($images as $key => $image) {
                            $BannerArr = array(
                                'user_id' => $user_id,
                                'car_id' => $insert_id,
                                'image' => $image,
                                'created' => $created_at,
                                'status' => 1
                            );
                            $this->Custom->insert_data('car_images', $BannerArr);
                        }
                    }
                    if (!empty($accessories)) {
                        $accessories = json_decode($accessories);
                        $amount = json_decode($amount);
                        foreach ($accessories as $key => $accessory) {
                            $accessoriesArr = array(
                                'car_id' => $insert_id,
                                'accessory' => $accessory,
                                'amount' => (isset($amount[$key]) && !empty($amount[$key])) ? $amount[$key] : 0,
                                'created' => $created_at,
                                'status' => 1
                            );
                            $this->Custom->insert_data('car_accessories', $accessoriesArr);
                        }
                    }

                    $this->response->success = 200;
                    $this->response->message = "Your car has been uploaded successfully.";
                    die(json_encode($this->response));
                } else {
                    $this->response->success = 202;
                    $this->response->message = $this->lang->line('something_went_wrong');
                    die(json_encode($this->response));
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function uploadLicense() {
        $user_id = ($this->input->post('user_id', TRUE)) ? $this->input->post('user_id', TRUE) : '';
        $front_pic = ($this->input->post('front_pic', TRUE)) ? $this->input->post('front_pic', TRUE) : '';
        $back_pic = ($this->input->post('back_pic', TRUE)) ? $this->input->post('back_pic', TRUE) : '';
        $license_number = ($this->input->post('license_number', TRUE)) ? $this->input->post('license_number', TRUE) : '';
        $expiry_date = ($this->input->post('expiry_date', TRUE)) ? $this->input->post('expiry_date', TRUE) : '';
        $_POST['in_english'] = ($this->input->post('in_english', TRUE)) ? $this->input->post('in_english', TRUE) : 0;
        $_POST['created'] = date("Y-m-d H:i:s");

        if (!empty($user_id) && !empty($front_pic) && !empty($back_pic) && !empty($license_number) && !empty($expiry_date)) {
            $user_record = $this->Custom->get_data('users', 'id', $user_id);
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }
                $check_license = $this->Custom->get_data('user_licence', 'user_id', $user_id);
                if (isset($check_license) && !empty($check_license)) {
                    $this->response->success = 202;
                    $this->response->message = "You have already uploaded license.";
                    die(json_encode($this->response));
                }
                $insert_id = $this->Custom->insert_data('user_licence', $_POST);
                if ($insert_id) {
                    $user_data = $this->Custom->get_where('user_licence', array('id' => $insert_id));

                    $this->response->success = 200;
                    $this->response->message = "License uploaded.";
                    $this->response->data = $user_data[0];
                    die(json_encode($this->response));
                } else {
                    $this->response->success = 202;
                    $this->response->message = $this->lang->line('something_went_wrong');
                    die(json_encode($this->response));
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function checkLicense() {
        $user_id = ($this->input->post('user_id', TRUE)) ? $this->input->post('user_id', TRUE) : '';
        if (!empty($user_id)) {
            $user_record = $this->Custom->get_data('users', 'id', $user_id);
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }
                $check_license = $this->Custom->get_data('user_licence', 'user_id', $user_id);
                if (isset($check_license) && !empty($check_license)) {
                    $this->response->success = 200;
                    $this->response->message = "License";
                    $this->response->data = $check_license[0];
                    die(json_encode($this->response));
                } else {
                    $this->response->success = 202;
                    $this->response->message = "License not found.";
                    die(json_encode($this->response));
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function GetRentalRates() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';

        if (!empty($user_id)) {
            $user_record = $this->Custom->get_data('users', 'id', $user_id);
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }

                $rental_rates = $this->Custom->get_where('rental_rates', array('status' => 1));
                if (isset($rental_rates) && !empty($rental_rates)) {
                    $this->response->success = 200;
                    $this->response->message = "Rental rates";
                    $this->response->data = $rental_rates[0];
                    die(json_encode($this->response));
                } else {
                    $this->response->success = 203;
                    $this->response->message = "No rental rates found.";
                    die(json_encode($this->response));
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function addCarBrand() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';
        $brand = ($this->input->post('brand')) ? $this->input->post('brand') : '';

        if (!empty($user_id) && !empty($brand)) {
            $user_record = $this->Custom->get_data('users', 'id', $user_id);
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }

                $check_brand = $this->Custom->get_where('brands', array('brand' => $brand, 'status' => 1, 'delete_status' => 0));
                if (isset($check_brand) && !empty($check_brand)) {
                    $this->response->success = 203;
                    $this->response->message = "Brand already exist in list";
                    die(json_encode($this->response));
                } else {
                    $created = date("Y-m-d H:i:s");
                    $dataArr = array(
                        'brand' => $brand,
                        'status' => 1,
                        'delete_status' => 0,
                        'created' => $created
                    );
                    $insert_id = $this->Custom->insert_data('brands', $dataArr);
                    if ($insert_id) {
                        $get_brand = $this->Custom->get_where('brands', array('id' => $insert_id));
                        $this->response->success = 200;
                        $this->response->message = "Brand added";
                        $this->response->data = $get_brand[0];
                        die(json_encode($this->response));
                    } else {
                        $this->response->success = 203;
                        $this->response->message = $this->lang->line('something_went_wrong');
                        die(json_encode($this->response));
                    }
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function addCarFeature() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';
        $accessory = ($this->input->post('accessory')) ? $this->input->post('accessory') : '';
        $icon = ($this->input->post('icon')) ? $this->input->post('icon') : '';
        $type = ($this->input->post('type')) ? $this->input->post('type') : 1; // Free => 1, Paid => 2

        if (!empty($user_id) && !empty($icon) && !empty($accessory) && !empty($type)) {
            $user_record = $this->Custom->get_data('users', 'id', $user_id);
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }

                $check_accessory = $this->Custom->get_where('base_accessories', array('title' => $accessory, 'status' => 1, 'delete_status' => 0));
                if (isset($check_accessory) && !empty($check_accessory)) {
                    $this->response->success = 203;
                    $this->response->message = "Accessory already exist in list";
                    die(json_encode($this->response));
                } else {
                    $created = date("Y-m-d H:i:s");
                    $dataArr = array(
                        'title' => $accessory,
                        'icon' => $icon,
                        'type' => $type,
                        'status' => 1,
                        'created' => $created,
                        'added_by_admin' => $user_id
                    );
                    $insert_id = $this->Custom->insert_data('base_accessories', $dataArr);
                    if ($insert_id) {
                        $get_feature = $this->Custom->get_where('base_accessories', array('id' => $insert_id));
                        $this->response->success = 200;
                        $this->response->message = "Feature added";
                        $this->response->data = $get_feature[0];
                        die(json_encode($this->response));
                    } else {
                        $this->response->success = 203;
                        $this->response->message = $this->lang->line('something_went_wrong');
                        die(json_encode($this->response));
                    }
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function deleteCarFeature() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';
        $accessory_id = ($this->input->post('accessory_id')) ? $this->input->post('accessory_id') : '';

        if (!empty($user_id) && !empty($accessory_id)) {
            $user_record = $this->Custom->get_data('users', 'id', $user_id);
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }

                if ($this->Custom->delete_where('base_accessories', array('id' => $accessory_id, 'added_by_admin' => $user_id))) {
                    $this->response->success = 200;
                    $this->response->message = "Accessory deleted successfully";
                    die(json_encode($this->response));
                } else {
                    $this->response->success = 203;
                    $this->response->message = $this->lang->line('something_went_wrong');
                    die(json_encode($this->response));
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function addCarModel() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';
        $brand = ($this->input->post('brand_id')) ? $this->input->post('brand_id') : '';
        $model = ($this->input->post('model')) ? $this->input->post('model') : '';

        if (!empty($user_id) && !empty($brand) && !empty($model)) {
            $user_record = $this->Custom->get_data('users', 'id', $user_id);
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }

                $check_model = $this->Custom->get_where('models', array('model' => $model, 'status' => 1, 'delete_status' => 0, 'brand_id' => $brand));
                if (isset($check_model) && !empty($check_model)) {
                    $this->response->success = 203;
                    $this->response->message = "Model already exist in list";
                    die(json_encode($this->response));
                } else {
                    $created = date("Y-m-d H:i:s");
                    $dataArr = array(
                        'brand_id' => $brand,
                        'model' => $model,
                        'status' => 1,
                        'created' => $created
                    );
                    $insert_id = $this->Custom->insert_data('models', $dataArr);
                    if ($insert_id) {
                        $get_model = $this->Custom->get_where('models', array('id' => $insert_id));
                        $this->response->success = 200;
                        $this->response->message = "Model added";
                        $this->response->data = $get_model[0];
                        die(json_encode($this->response));
                    } else {
                        $this->response->success = 203;
                        $this->response->message = $this->lang->line('something_went_wrong');
                        die(json_encode($this->response));
                    }
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function setRentalPrice() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';
        $price_per_day = ($this->input->post('price_per_day')) ? $this->input->post('price_per_day') : '';
        $car_id = ($this->input->post('car_id')) ? $this->input->post('car_id') : '';
        if (!empty($user_id) && !empty($price_per_day) && !empty($car_id)) {
            $user_record = $this->Custom->get_data('users', 'id', $user_id);
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }

                $updateArr = array(
                    'price_per_day' => $price_per_day
                );
                $update_res = $this->Custom->update("cars", $updateArr, 'id', $car_id);
                if ($update_res) {
                    $this->response->success = 200;
                    $this->response->message = "Rental price updated.";
                    die(json_encode($this->response));
                } else {
                    $this->response->success = 202;
                    $this->response->message = $this->lang->line('something_went_wrong');
                    die(json_encode($this->response));
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function setAvailability() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';
        $car_id = ($this->input->post('car_id')) ? $this->input->post('car_id') : '';
        $start_date = ($this->input->post('start_date')) ? json_decode($this->input->post('start_date')) : '';
        $end_date = ($this->input->post('end_date')) ? json_decode($this->input->post('end_date')) : '';
        $created = date("Y-m-d H:i:s");
        $status = 1;
        //$days = ($this->input->post('days')) ? $this->input->post('days') : '';
        //$reccuring = ($this->input->post('reccuring')) ? $this->input->post('reccuring') : 0;
        //$price_per_day = ($this->input->post('price_per_day')) ? $this->input->post('price_per_day') : '';
        //$start_time = ($this->input->post('start_time')) ? $this->input->post('start_time') : '';
        //$end_time = ($this->input->post('end_time')) ? $this->input->post('end_time') : '';
        if (!empty($user_id) && !empty($car_id) && !empty($start_date) && !empty($end_date)) {
            $user_record = $this->Custom->get_data('users', 'id', $user_id);
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }

                foreach ($start_date as $key => $value) {
                    $endDate = $end_date[$key];
                }
                $insert_id = $this->Custom->insert_data("car_availability", $_POST);
                if ($insert_id) {
                    $this->response->success = 200;
                    $this->response->message = "Car availability is set.";
                    die(json_encode($this->response));
                } else {
                    $this->response->success = 202;
                    $this->response->message = $this->lang->line('something_went_wrong');
                    die(json_encode($this->response));
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function getMyCars() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';

        if (!empty($user_id)) {
            $user_record = $this->Custom->get_data('users', 'id', $user_id);
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }

                $cars = $this->Custom->query("SELECT c.id, c.title, c.primary_image, c.model, c.brand, c.status, ca.primary_address FROM cars as c JOIN car_location as ca ON c.id = ca.car_id");
                if (isset($cars) && !empty($cars)) {
                    foreach ($cars as $key => $value) {
                        $value->brand = getBrandById($value->brand);
                        $value->model = getModelById($value->model);
                    }
                    $this->response->success = 200;
                    $this->response->message = "My cars";
                    $this->response->data = $cars;
                    die(json_encode($this->response));
                } else {
                    $this->response->success = 203;
                    $this->response->message = "No cars found.";
                    die(json_encode($this->response));
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function GetCatValue() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';

        if (!empty($user_id)) {
            $user_record = $this->Custom->get_data('users', 'id', $user_id);
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }

                $car_value = $this->Custom->get_where('car_value', array('status' => 1, 'delete_status' => 0));
                if (isset($car_value) && !empty($car_value)) {
                    $this->response->success = 200;
                    $this->response->message = "Car Value";
                    $this->response->data = $car_value;
                    die(json_encode($this->response));
                } else {
                    $this->response->success = 203;
                    $this->response->message = "No car value found.";
                    die(json_encode($this->response));
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function carDetails() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';
        $car_id = ($this->input->post('car_id')) ? $this->input->post('car_id') : '';

        if (!empty($user_id) && !empty($car_id)) {
            $user_record = $this->Custom->get_data('users', 'id', $user_id);
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }

                $car = $this->Custom->query("SELECT c.*, ca.primary_address, ca.secondary_lng, ca.primary_lat, ca.primary_lng, ca.secondary_address, ca.secondary_lat FROM cars as c JOIN car_location as ca ON c.id = ca.car_id where c.id = $car_id");
                if (isset($car) && !empty($car)) {
                    $images = $this->Custom->get_data('car_images', 'car_id', $car_id);
                    $car[0]->images = (isset($images) && !empty($images)) ? $images : array();
                    $accessories = $this->Custom->get_data('car_accessories', 'car_id', $car_id);
                    $car[0]->accessories = (isset($accessories) && !empty($accessories)) ? $accessories : array();
                    $this->response->success = 200;
                    $this->response->message = "Car details";
                    $this->response->data = $car[0];
                    die(json_encode($this->response));
                } else {
                    $this->response->success = 203;
                    $this->response->message = "No car found.";
                    die(json_encode($this->response));
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function editCar() {
        $car_id = ($this->input->post('car_id')) ? $this->input->post('car_id') : '';
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';
        $title = ($this->input->post('title')) ? $this->input->post('title') : '';
        $registration_number = ($this->input->post('registration_number')) ? $this->input->post('registration_number') : '';
        $brand = ($this->input->post('brand')) ? $this->input->post('brand') : '';
        $model = ($this->input->post('model')) ? $this->input->post('model') : '';
        $year = ($this->input->post('year')) ? $this->input->post('year') : '';
        $vehicle_type = ($this->input->post('vehicle_type')) ? $this->input->post('vehicle_type') : '';
        $transmission = ($this->input->post('transmission')) ? $this->input->post('transmission') : '';
        $no_of_doors = ($this->input->post('no_of_doors')) ? $this->input->post('no_of_doors') : '';
        $no_of_seats = ($this->input->post('no_of_seats')) ? $this->input->post('no_of_seats') : '';
        $fuel = ($this->input->post('fuel')) ? $this->input->post('fuel') : '';
        $car_value = ($this->input->post('car_value')) ? $this->input->post('car_value') : '';
        $description = ($this->input->post('description')) ? $this->input->post('description') : '';
        $primary_image = ($this->input->post('primary_image')) ? $this->input->post('primary_image') : '';

        $primary_address = ($this->input->post('primary_address')) ? $this->input->post('primary_address') : '';
        $primary_lat = ($this->input->post('primary_lat')) ? $this->input->post('primary_lat') : '';
        $primary_lng = ($this->input->post('primary_lng')) ? $this->input->post('primary_lng') : '';
        $secondary_address = ($this->input->post('secondary_address')) ? $this->input->post('secondary_address') : '';
        $secondary_lat = ($this->input->post('secondary_lat')) ? $this->input->post('secondary_lat') : '';
        $secondary_lng = ($this->input->post('secondary_lng')) ? $this->input->post('secondary_lng') : '';

        $images = ($this->input->post('images')) ? $this->input->post('images') : '';
        $accessories = ($this->input->post('accessories')) ? $this->input->post('accessories') : '';
        $amount = ($this->input->post('amount')) ? $this->input->post('amount') : '';
        $created_at = date('Y-m-d H:i:s');

        if (!empty($car_id) && !empty($user_id) && !empty($title) && !empty($registration_number) && !empty($brand) && !empty($model) && !empty($year) && !empty($vehicle_type) && !empty($transmission) && !empty($no_of_doors) && !empty($no_of_seats) && !empty($fuel) && !empty($car_value) && !empty($primary_address) && !empty($primary_lat) && !empty($primary_lng) && !empty($images)) {
            $user_record = $this->Custom->get_data('users', 'id', $user_id);
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }

                if ($user_record[0]->role == 'owner') {
                    $this->response->success = 203;
                    $this->response->message = "User role is not an owner";
                    die(json_encode($this->response));
                }

                $updateArr = array(
                    'title' => $title,
                    'description' => $description,
                    'registration_number' => $registration_number,
                    'brand' => $brand,
                    'model' => $model,
                    'year' => $year,
                    'vehicle_type' => $vehicle_type,
                    'transmission' => $transmission,
                    'no_of_doors' => $no_of_doors,
                    'no_of_seats' => $no_of_seats,
                    'fuel' => $fuel,
                    'car_value' => $car_value,
                    'primary_image' => $primary_image,
                    'updated' => $created_at
                );
                $update_res = $this->Custom->update('cars', $updateArr, 'id', $car_id);
                if ($update_res) {
                    $EventOtherArr = array(
                        'primary_address' => $primary_address,
                        'primary_lat' => $primary_lat,
                        'primary_lng' => $primary_lng,
                        'secondary_address' => $secondary_address,
                        'secondary_lat' => $secondary_lat,
                        'secondary_lng' => $secondary_lng
                    );
                    $this->Custom->update('car_location', $EventOtherArr, 'car_id', $car_id);

                    //upload cover image
                    $this->Custom->delete_where('car_images', array('car_id' => $car_id));
                    if (!empty($images)) {
                        $images = json_decode($images);
                        foreach ($images as $key => $image) {
                            $BannerArr = array(
                                'user_id' => $user_id,
                                'car_id' => $car_id,
                                'image' => $image,
                                'created' => $created_at,
                                'status' => 1
                            );
                            $this->Custom->insert_data('car_images', $BannerArr);
                        }
                    }

                    $this->Custom->delete_where('car_accessories', array('car_id' => $car_id));
                    if (!empty($accessories)) {
                        $accessories = json_decode($accessories);
                        $amount = json_decode($amount);
                        foreach ($accessories as $key => $accessory) {
                            $accessoriesArr = array(
                                'car_id' => $car_id,
                                'accessory' => $accessory,
                                'amount' => (isset($amount[$key]) && !empty($amount[$key])) ? $amount[$key] : 0,
                                'created' => $created_at,
                                'status' => 1
                            );
                            $this->Custom->insert_data('car_accessories', $accessoriesArr);
                        }
                    }
                    $this->response->success = 200;
                    $this->response->message = "Your car details has been updated successfully.";
                    die(json_encode($this->response));
                } else {
                    $this->response->success = 202;
                    $this->response->message = $this->lang->line('something_went_wrong');
                    die(json_encode($this->response));
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function CreateEvent() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';
        $title = ($this->input->post('title')) ? $this->input->post('title') : '';
        $description = ($this->input->post('description')) ? $this->input->post('description') : '';
        $event_date = ($this->input->post('event_date')) ? $this->input->post('event_date') : '';
        $start_time = ($this->input->post('start_time')) ? $this->input->post('start_time') : '';
        $end_time = ($this->input->post('end_time')) ? $this->input->post('end_time') : '';
        $event_type = ($this->input->post('event_type')) ? $this->input->post('event_type') : '';
        $price = ($this->input->post('price')) ? $this->input->post('price') : '';
        $price_include_tax = ($this->input->post('price_include_tax')) ? $this->input->post('price_include_tax') : '';
        $apartment = ($this->input->post('apartment')) ? $this->input->post('apartment') : '';
        $name = ($this->input->post('name')) ? $this->input->post('name') : '';
        $street = ($this->input->post('street')) ? $this->input->post('street') : '';
        $city = ($this->input->post('city')) ? $this->input->post('city') : '';
        $state = ($this->input->post('state')) ? $this->input->post('state') : '';
        $country = ($this->input->post('country')) ? $this->input->post('country') : '';
        $zipcode = ($this->input->post('zipcode')) ? $this->input->post('zipcode') : '';
        $about_me_cuisine = ($this->input->post('about_me_cuisine')) ? $this->input->post('about_me_cuisine') : '';
        $vegan_cuisine = ($this->input->post('vegan_cuisine')) ? $this->input->post('vegan_cuisine') : '';
        $drinks = ($this->input->post('drinks')) ? $this->input->post('drinks') : '';
        $other_drinks = ($this->input->post('other_drinks')) ? $this->input->post('other_drinks') : '';
        $dessert = ($this->input->post('dessert')) ? $this->input->post('dessert') : '';
        $common_allergies = ($this->input->post('common_allergies')) ? $this->input->post('common_allergies') : '';
        $other_common_allergies = ($this->input->post('other_common_allergies')) ? $this->input->post('other_common_allergies') : '';
        $recurring_event = ($this->input->post('recurring_event')) ? $this->input->post('recurring_event') : '';
        $week_day_id = ($this->input->post('week_day_id')) ? $this->input->post('week_day_id') : '';
        $min_guest = ($this->input->post('min_guest')) ? $this->input->post('min_guest') : '';
        $max_guest = ($this->input->post('max_guest')) ? $this->input->post('max_guest') : '';
        $no_of_courses = ($this->input->post('no_of_courses')) ? $this->input->post('no_of_courses') : '';
        $created_at = date('Y-m-d H:i:s');
        $images = (isset($_FILES['image']) && !empty($_FILES['image'])) ? $_FILES['image'] : '';
        $latitude = ($this->input->post('latitude', TRUE)) ? $this->input->post('latitude', TRUE) : '';
        $longitude = ($this->input->post('longitude', TRUE)) ? $this->input->post('longitude', TRUE) : '';
        $meal_type = ($this->input->post('meal_type', TRUE)) ? $this->input->post('meal_type', TRUE) : '';
        $images_arr = "";

        if (!empty($user_id) && !empty($title) && !empty($event_date) && !empty($start_time) && !empty($end_time) && !empty($event_type) && !empty($price)) {
            $user_record = $this->Custom->get_data('users', 'id', $user_id);
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }

                /* if ($user_record[0]->status != 1) {
                  $this->response->success = 204;
                  $this->response->message = $this->lang->line('account_not_activate');
                  die(json_encode($this->response));
                  } */
                if ($user_record[0]->role == 2) {
                    $this->response->success = 203;
                    $this->response->message = $this->lang->line('user_not_host');
                    die(json_encode($this->response));
                }
                if ($event_date < date('Y-m-d')) {
                    $this->response->success = 203;
                    $this->response->message = $this->lang->line('invalid_event_date');
                    die(json_encode($this->response));
                }

                if ($recurring_event == 1) {
                    if (empty($week_day_id)) {
                        $this->response->success = 201;
                        $this->response->message = $this->lang->line('required_field_error');
                        die(json_encode($this->response));
                    }
                }

                $where = "event.user_id = $user_id && event.event_date = '$event_date' && event.event_type = $event_type && event.start_time = '$start_time' && event.end_time = '$end_time'";
                //$where = "event.user_id = $user_id && event.event_date = '$event_date' && event.event_type = $event_type && event.start_time = '$start_time' && event.end_time = '$end_time' && event_other_details.address = '$address'";
                $Events = $this->Apimodel->GetEvents($where);
                if (!empty($Events)) {
                    $this->response->success = 203;
                    $this->response->message = $this->lang->line('event_already_exist_on_this_address');
                    die(json_encode($this->response));
                }
                $event_next_date = '';
                if ($recurring_event == 1) {
                    switch ($week_day_id):
                        case 1:
                            $event_next_date = date('Y-m-d', strtotime('next monday', strtotime($event_date)));
                            break;
                        case 2:
                            $event_next_date = date('Y-m-d', strtotime('next tuesday', strtotime($event_date)));
                            break;
                        case 3:
                            $event_next_date = date('Y-m-d', strtotime('next wednesday', strtotime($event_date)));
                            break;
                        case 4:
                            $event_next_date = date('Y-m-d', strtotime('next thursday', strtotime($event_date)));
                            break;
                        case 5:
                            $event_next_date = date('Y-m-d', strtotime('next friday', strtotime($event_date)));
                            break;
                        case 6:
                            $event_next_date = date('Y-m-d', strtotime('next saturday', strtotime($event_date)));
                            break;
                        case 7:
                            $event_next_date = date('Y-m-d', strtotime('next sunday', strtotime($event_date)));
                            break;
                    endswitch;
                }
                if ($recurring_event == 2) {
                    $event_next_date = date('Y-m-d', strtotime('+1 day', strtotime($event_date)));
                }
                //$tax_price = $price * 20 / 100;
                //$price_include_tax = $price + $tax_price;
                //$price_include_tax = $price;
                $insertArr = array(
                    'user_id' => $user_id,
                    'title' => $title,
                    'description' => $description,
                    'event_date' => $event_date,
                    'event_next_date' => $event_next_date,
                    'start_time' => $start_time,
                    'end_time' => $end_time,
                    'event_type' => $event_type,
                    'meal_type' => $meal_type,
                    'price' => $price,
                    'price_include_tax' => $price_include_tax,
                    'status' => 1,
                    'created_at' => $created_at
                );
                $insert_id = $this->Custom->insert_data('event', $insertArr);
                if ($insert_id) {
                    //profile data
                    $EventOtherArr = array(
                        'event_id' => $insert_id,
                        'apartment' => $apartment,
                        'name' => $name,
                        'street' => $street,
                        'city' => $city,
                        'state' => $state,
                        'country' => $country,
                        'zipcode' => $zipcode,
                        'about_me_cuisine' => $about_me_cuisine,
                        'vegan_cuisine' => $vegan_cuisine,
                        'drinks' => $drinks,
                        'other_drinks' => $other_drinks,
                        'dessert' => $dessert,
                        'common_allergies' => $common_allergies,
                        'other_common_allergies' => $other_common_allergies,
                        'recurring_event' => $recurring_event,
                        'week_day_id' => $week_day_id,
                        'min_guest' => $min_guest,
                        'max_guest' => $max_guest,
                        'no_of_courses' => $no_of_courses,
                        'latitude' => $latitude,
                        'longitude' => $longitude,
                        'created_at' => $created_at
                    );
                    $this->Custom->insert_data('event_other_details', $EventOtherArr);
                    //upload cover image
                    if (!empty($images)) {
                        $files = $images;
                        unset($_FILES);
                        $config = array(
                            'upload_path' => EVENT_PIC_PATH . '/',
                            'allowed_types' => '*',
                            'overwrite' => TRUE,
                            'encrypt_name' => FALSE,
                            'remove_spaces' => FALSE
                        );

                        $this->load->library('upload', $config);
                        foreach ($files['name'] as $key => $image) {
                            $_FILES['image']['name'] = $files['name'][$key];
                            $_FILES['image']['type'] = $files['type'][$key];
                            $_FILES['image']['tmp_name'] = $files['tmp_name'][$key];
                            $_FILES['image']['error'] = $files['error'][$key];
                            $_FILES['image']['size'] = $files['size'][$key];

                            $ext = end((explode(".", $image)));
                            $new_name = rand() . time() . '.' . $ext;

                            $config['file_name'] = $new_name;

                            $this->upload->initialize($config);

                            $this->upload->do_upload('image');
                            $images_arr[] = $new_name;
                        }
                    }
                    if (!empty($images_arr)) {
                        foreach ($images_arr as $row) {
                            $BannerArr = array(
                                'event_id' => $insert_id,
                                'cover_pic' => $row,
                                'created_at' => $created_at
                            );
                            $this->Custom->insert_data('event_cover_image', $BannerArr);
                        }
                    }
                    $EventLogArr = array(
                        'event_id' => $insert_id,
                        'eventDate' => $event_date,
                        'event_status' => 0
                    );
                    $this->Custom->insert_data('event_log', $EventLogArr);

                    $this->response->success = 200;
                    $this->response->message = $this->lang->line('event_created');
                    die(json_encode($this->response));
                } else {
                    $this->response->success = 202;
                    $this->response->message = $this->lang->line('something_went_wrong');
                    die(json_encode($this->response));
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function GetEvents() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';
        $user_role = ($this->input->post('user_role')) ? $this->input->post('user_role') : '';
        $type = ($this->input->post('type')) ? $this->input->post('type') : '';
        $city = ($this->input->post('city')) ? $this->input->post('city') : '';
        $country = ($this->input->post('country')) ? $this->input->post('country') : '';

        $number_of_guests = ($this->input->post('number_of_guests')) ? $this->input->post('number_of_guests') : '';
        $min_price = ($this->input->post('min_price')) ? $this->input->post('min_price') : '';
        $max_price = ($this->input->post('max_price')) ? $this->input->post('max_price') : '';
        $event_type = ($this->input->post('event_type')) ? $this->input->post('event_type') : '';
        $vegan_cuisine = ($this->input->post('vegan_cuisines')) ? $this->input->post('vegan_cuisines') : '';
        $language = ($this->input->post('languages')) ? $this->input->post('languages') : '';
        $dates = ($this->input->post('dates')) ? $this->input->post('dates') : '';

        if (!empty($user_id) && !empty($type) && !empty($user_role)) {
            $user_record = $this->Custom->get_where('users', array('id' => $user_id, 'role' => $user_role));
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }

                /* if ($user_record[0]->status != 1) {
                  $this->response->success = 204;
                  $this->response->message = $this->lang->line('account_not_activate');
                  die(json_encode($this->response));
                  } */
                switch ($type):
                    case 'my_event':
                        $where = "event.user_id = $user_id and event.event_date >= CURDATE()";
                        $Events = $this->Apimodel->GetEvents($where);
                        if (empty($Events)) {
                            $this->response->success = 205;
                            $this->response->message = $this->lang->line('no_record');
                            die(json_encode($this->response));
                        }
                        $today = date('Y-m-d');
                        $day = date('w', strtotime($today));
                        foreach ($Events as $row) {
                            $row->cover_pic = $this->Apimodel->GetEventCoverImage($row->id);
                            $review_data = $this->Custom->query("SELECT AVG(rating) as avg_rating, count(*) as total_review from ratings_and_reviews where event_id = $row->id");
                            $row->rating = (isset($review_data[0]->avg_rating) && !empty($review_data[0]->avg_rating)) ? $review_data[0]->avg_rating : 0.0;
                            $row->total_review = (isset($review_data) && !empty($review_data)) ? $review_data[0]->total_review : 0;

                            $recurring_event = $row->recurring_event;
                            if ($recurring_event == 1) {
                                $current_event_date = $row->event_date;
                                $next_event_date = $row->event_next_date;
                                if (strtotime($today) < strtotime($next_event_date)) {
                                    $event_date = $current_event_date;
                                } else {
                                    $event_date = $next_event_date;
                                }
                                $row->current_event_date = $event_date;
                                $guest_count = $this->Custom->query("SELECT SUM(no_of_guests) as total_joined FROM event_bookings WHERE event_id = " . $row->id . " AND booking_date = '$event_date'");
                                $row->JoinGuestCount = (isset($guest_count[0]->total_joined) && !empty($guest_count[0]->total_joined)) ? $guest_count[0]->total_joined : 0;
                            } else if ($recurring_event == 2) {
                                $event_date = $row->event_next_date;
                                $guest_count = $this->Custom->query("SELECT SUM(no_of_guests) as total_joined FROM event_bookings WHERE event_id = " . $row->id . " AND booking_date = '$event_date'");
                                $row->current_event_date = $event_date;
                                $row->JoinGuestCount = (isset($guest_count[0]->total_joined) && !empty($guest_count[0]->total_joined)) ? $guest_count[0]->total_joined : 0;
                            } else {
                                $event_date = $row->event_date;
                                $row->current_event_date = $event_date;
                                $guest_count = $this->Custom->query("SELECT SUM(no_of_guests) as total_joined FROM event_bookings WHERE event_id = " . $row->id);
                                $row->JoinGuestCount = (isset($guest_count[0]->total_joined) && !empty($guest_count[0]->total_joined)) ? $guest_count[0]->total_joined : 0;
                            }
                        }
                        $this->response->success = 200;
                        $this->response->message = $this->lang->line('events_list');
                        $this->response->data = $Events;
                        die(json_encode($this->response));
                        break;

                    case 'city_wise':
                        if (empty($city) && empty($country)) {
                            $this->response->success = 201;
                            $this->response->message = $this->lang->line('required_field_error');
                            die(json_encode($this->response));
                        }
                        $today = date('Y-m-d');
                        $day = date('w', strtotime($today));
                        $where = "";

                        /**
                         * Filters started
                         */
                        if (isset($number_of_guests) && !empty($number_of_guests)) {
                            $where .= "(event_other_details.min_guest <= $number_of_guests AND event_other_details.max_guest >= $number_of_guests) AND ";
                        }

                        if (isset($min_price) && !empty($min_price) && isset($max_price) && !empty($max_price)) {
                            $where .= "(event.price_include_tax between $min_price and $max_price) AND ";
                        }

                        if (isset($event_type) && !empty($event_type) && is_numeric($event_type)) {
                            $where .= "(event.event_type = $event_type) AND ";
                        }

                        $where .= "event.event_date >= CURDATE() AND ";
                        /**
                         * Filters completed
                         */
                        if (!empty($city)) {
                            $where .= "event.user_id != $user_id AND event_other_details.city = '$city'";
                        } else {
                            $where .= "event.user_id != $user_id AND event_other_details.country = '$country'";
                        }
                        $Events = $this->Apimodel->GetEvents($where);
                        //print_r($Events); die('>>>>>>>>><<<<<<');
                        if (empty($Events)) {
                            $this->response->success = 205;
                            $this->response->message = $this->lang->line('no_record');
                            die(json_encode($this->response));
                        }
                        $EventArr = array();
                        foreach ($Events as $row) {
                            $user_profile_pic = $this->Custom->query("SELECT profile_pic FROM user_profile WHERE user_id = " . $row->user_id);
                            if (!empty($user_profile_pic[0]->profile_pic)) {
                                if (preg_match("/http/i", $user_profile_pic[0]->profile_pic)) {
                                    $row->profile_pic = $user_profile_pic[0]->profile_pic;
                                } else {
                                    $row->profile_pic = ($user_profile_pic[0]->profile_pic) ? base_url() . USER_PROFILE_URL . $user_profile_pic[0]->profile_pic : "";
                                }
                            } else {
                                $row->profile_pic = "";
                            }
                            $row->cover_pic = $this->Apimodel->GetEventCoverImage($row->id);
                            $review_data = $this->Custom->query("SELECT AVG(rating) as avg_rating, count(*) as total_review from ratings_and_reviews where event_id = $row->id");
                            $row->rating = (isset($review_data[0]->avg_rating) && !empty($review_data[0]->avg_rating)) ? $review_data[0]->avg_rating : 0.0;
                            $row->total_review = (isset($review_data[0]->total_review) && !empty($review_data[0]->total_review)) ? $review_data[0]->total_review : 0;

                            $recurring_event = $row->recurring_event;
                            if ($recurring_event == 1) {
                                $current_event_date = $row->event_date;
                                $next_event_date = $row->event_next_date;
                                if (strtotime($today) < strtotime($next_event_date)) {
                                    $event_date = $current_event_date;
                                } else {
                                    $event_date = $next_event_date;
                                }
                                $row->current_event_date = $event_date;
                                $guest_count = $this->Custom->query("SELECT SUM(no_of_guests) as total_joined FROM event_bookings WHERE event_id = " . $row->id . " AND booking_date = '$event_date'");
                                $row->JoinGuestCount = (isset($guest_count[0]->total_joined) && !empty($guest_count[0]->total_joined)) ? $guest_count[0]->total_joined : 0;
                            } else if ($recurring_event == 2) {
                                $event_date = $row->event_next_date;
                                $guest_count = $this->Custom->query("SELECT SUM(no_of_guests) as total_joined FROM event_bookings WHERE event_id = " . $row->id . " AND booking_date = '$event_date'");
                                $row->current_event_date = $event_date;
                                $row->JoinGuestCount = (isset($guest_count[0]->total_joined) && !empty($guest_count[0]->total_joined)) ? $guest_count[0]->total_joined : 0;
                            } else {
                                $event_date = $row->event_date;
                                $row->current_event_date = $event_date;
                                $guest_count = $this->Custom->query("SELECT SUM(no_of_guests) as total_joined FROM event_bookings WHERE event_id = " . $row->id);
                                $row->JoinGuestCount = (isset($guest_count[0]->total_joined) && !empty($guest_count[0]->total_joined)) ? $guest_count[0]->total_joined : 0;
                            }
                            if (isset($vegan_cuisine) && !empty($vegan_cuisine)) {
                                $all_vegan_cuisine = (isset($row->vegan_cuisine) && !empty($row->vegan_cuisine)) ? explode(",", $row->vegan_cuisine) : '';
                                if (!isset($all_vegan_cuisine) || empty($all_vegan_cuisine)) {
                                    continue;
                                } else {
                                    $vegan_cuisines = explode(",", $vegan_cuisine);
                                    $cuisine_status = 0;
                                    foreach ($vegan_cuisines as $key => $value) {
                                        if (in_array($value, $all_vegan_cuisine)) {
                                            $cuisine_status = 1;
                                        }
                                    }
                                    if ($cuisine_status == 0) {
                                        continue;
                                    }
                                }
                            }
                            $event_user_id = (isset($row->user_id) && !empty($row->user_id)) ? $row->user_id : '';
                            $event_user_record = $this->Custom->get_where('user_profile', array('id' => $event_user_id));
                            if (isset($event_user_record) && !empty($event_user_record)) {
                                if (!empty($language)) {
                                    if (isset($event_user_record[0]->language) && !empty($event_user_record[0]->language)) {
                                        $all_languages = explode(",", $event_user_record[0]->language);
                                        $languages = explode(",", $language);
                                        $language_status = 0;
                                        foreach ($languages as $key => $value) {
                                            if (in_array($value, $all_languages)) {
                                                $language_status = 1;
                                            }
                                        }
                                        if ($language_status == 0) {
                                            continue;
                                        }
                                    } else {
                                        continue;
                                    }
                                }
                            } else {
                                continue;
                            }

                            if (!empty($dates)) {
                                $dates_array = explode(",", $dates);
                                $event_next_date = array();
                                $recurring_event = $row->recurring_event;
                                $week_day_id = $row->week_day_id;
                                $event_date = $row->event_next_date;
                                if (!empty($event_date)) {
                                    $eventThreeMonthDate = date('Y-m-d', strtotime("+2 months", strtotime($event_date)));
                                    if ($recurring_event == 1) {
                                        if (in_array($event_date, $dates_array)) {
                                            $check_event_block = $this->Custom->get_where('cancelled_event', array('event_id' => $row->id, 'eventDate' => $event_date));
                                            if (!isset($check_event_block) || empty($check_event_block))
                                                $event_next_date[] = $event_date;
                                        }
                                        switch ($week_day_id):
                                            case 1:
                                                while (strtotime($eventThreeMonthDate) >= strtotime($event_date)) {
                                                    $event_date = date('Y-m-d', strtotime('next monday', strtotime($event_date)));
                                                    if (in_array($event_date, $dates_array)) {
                                                        $check_event_block = $this->Custom->get_where('cancelled_event', array('event_id' => $row->id, 'eventDate' => $event_date));
                                                        if (!isset($check_event_block) || empty($check_event_block))
                                                            $event_next_date[] = $event_date;
                                                    }
                                                }
                                                break;
                                            case 2:
                                                while (strtotime($eventThreeMonthDate) >= strtotime($event_date)) {
                                                    $event_date = date('Y-m-d', strtotime('next tuesday', strtotime($event_date)));
                                                    if (in_array($event_date, $dates_array)) {
                                                        $check_event_block = $this->Custom->get_where('cancelled_event', array('event_id' => $row->id, 'eventDate' => $event_date));
                                                        if (!isset($check_event_block) || empty($check_event_block))
                                                            $event_next_date[] = $event_date;
                                                    }
                                                }
                                                break;
                                            case 3:
                                                while (strtotime($eventThreeMonthDate) >= strtotime($event_date)) {
                                                    $event_date = date('Y-m-d', strtotime('next wednesday', strtotime($event_date)));
                                                    if (in_array($event_date, $dates_array)) {
                                                        $check_event_block = $this->Custom->get_where('cancelled_event', array('event_id' => $row->id, 'eventDate' => $event_date));
                                                        if (!isset($check_event_block) || empty($check_event_block))
                                                            $event_next_date[] = $event_date;
                                                    }
                                                }
                                                break;
                                            case 4:
                                                while (strtotime($eventThreeMonthDate) >= strtotime($event_date)) {
                                                    $event_date = date('Y-m-d', strtotime('next thursday', strtotime($event_date)));
                                                    if (in_array($event_date, $dates_array)) {
                                                        $check_event_block = $this->Custom->get_where('cancelled_event', array('event_id' => $row->id, 'eventDate' => $event_date));
                                                        if (!isset($check_event_block) || empty($check_event_block))
                                                            $event_next_date[] = $event_date;
                                                    }
                                                }
                                                break;
                                            case 5:
                                                while (strtotime($eventThreeMonthDate) >= strtotime($event_date)) {
                                                    $event_date = date('Y-m-d', strtotime('next friday', strtotime($event_date)));
                                                    if (in_array($event_date, $dates_array)) {
                                                        $check_event_block = $this->Custom->get_where('cancelled_event', array('event_id' => $row->id, 'eventDate' => $event_date));
                                                        if (!isset($check_event_block) || empty($check_event_block))
                                                            $event_next_date[] = $event_date;
                                                    }
                                                }
                                                break;
                                            case 6:
                                                while (strtotime($eventThreeMonthDate) >= strtotime($event_date)) {
                                                    $event_date = date('Y-m-d', strtotime('next saturday', strtotime($event_date)));
                                                    if (in_array($event_date, $dates_array)) {
                                                        $check_event_block = $this->Custom->get_where('cancelled_event', array('event_id' => $row->id, 'eventDate' => $event_date));
                                                        if (!isset($check_event_block) || empty($check_event_block))
                                                            $event_next_date[] = $event_date;
                                                    }
                                                }
                                                break;
                                            case 7:
                                                while (strtotime($eventThreeMonthDate) >= strtotime($event_date)) {
                                                    $event_date = date('Y-m-d', strtotime('next sunday', strtotime($event_date)));
                                                    if (in_array($event_date, $dates_array)) {
                                                        $check_event_block = $this->Custom->get_where('cancelled_event', array('event_id' => $row->id, 'eventDate' => $event_date));
                                                        if (!isset($check_event_block) || empty($check_event_block))
                                                            $event_next_date[] = $event_date;
                                                    }
                                                }
                                                break;
                                        endswitch;
                                    }
                                    if ($recurring_event == 2) {
                                        if (in_array($event_date, $dates_array)) {
                                            $check_event_block = $this->Custom->get_where('cancelled_event', array('event_id' => $row->id, 'eventDate' => $event_date));
                                            if (!isset($check_event_block) || empty($check_event_block))
                                                $event_next_date[] = $event_date;
                                        }
                                        while (strtotime($eventThreeMonthDate) >= strtotime($event_date)) {
                                            $event_date = date('Y-m-d', strtotime('+1 day', $event_date));
                                            if (in_array($event_date, $dates_array)) {
                                                $check_event_block = $this->Custom->get_where('cancelled_event', array('event_id' => $row->id, 'eventDate' => $event_date));
                                                if (!isset($check_event_block) || empty($check_event_block))
                                                    $event_next_date[] = $event_date;
                                            }
                                        }
                                    }
                                }
                                if (empty($event_next_date)) {
                                    continue;
                                }
                            }

                            /* if ($row->recurring_event == 1) {
                              $row->week_day_id = ($row->week_day_id == 7) ? 0 : $row->week_day_id;
                              if ($row->week_day_id == $day)
                              $EventArr[] = $row;
                              }else {
                              $EventArr[] = $row;
                              } */
                            $EventArr[] = $row;
                        }
                        if (empty($EventArr)) {
                            $this->response->success = 205;
                            $this->response->message = $this->lang->line('no_record');
                            die(json_encode($this->response));
                        }
                        $this->response->success = 200;
                        $this->response->message = $this->lang->line('events_list');
                        $this->response->data = $EventArr;
                        die(json_encode($this->response));
                        break;
                endswitch;
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function GetEventDetails() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';
        $event_id = ($this->input->post('event_id')) ? $this->input->post('event_id') : '';
        $user_role = ($this->input->post('user_role')) ? $this->input->post('user_role') : '';

        if (!empty($user_id) && !empty($event_id) && !empty($user_role)) {
            $user_record = $this->Custom->get_where('users', array('id' => $user_id, 'role' => $user_role));
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }

                /* if ($user_record[0]->status != 1) {
                  $this->response->success = 204;
                  $this->response->message = $this->lang->line('account_not_activate');
                  die(json_encode($this->response));
                  } */
                $where = "event.id = $event_id";
                $Events = $this->Apimodel->GetEvents($where);
                if (empty($Events)) {
                    $this->response->success = 203;
                    $this->response->message = $this->lang->line('invalid_event');
                    die(json_encode($this->response));
                }
                $user_profile_pic = $this->Custom->query("SELECT profile_pic FROM user_profile WHERE user_id = " . $Events[0]->user_id);
                if (isset($user_profile_pic[0]->profile_pic) && !empty($user_profile_pic[0]->profile_pic)) {
                    if (preg_match("/http/i", $user_profile_pic[0]->profile_pic)) {
                        $Events[0]->profile_pic = $user_profile_pic[0]->profile_pic;
                    } else {
                        $Events[0]->profile_pic = ($user_profile_pic[0]->profile_pic) ? base_url() . USER_PROFILE_URL . $user_profile_pic[0]->profile_pic : "";
                    }
                } else {
                    $Events[0]->profile_pic = "";
                }
                $Events[0]->cover_pic = $this->Apimodel->GetEventCoverImage($event_id);
                $review_data = $this->Custom->query("SELECT AVG(rating) as avg_rating, count(*) as total_review from ratings_and_reviews where event_id = $event_id");
                $Events[0]->rating = (isset($review_data[0]->avg_rating) && !empty($review_data[0]->avg_rating)) ? $review_data[0]->avg_rating : 0.0;
                $Events[0]->total_review = (isset($review_data) && !empty($review_data)) ? $review_data[0]->total_review : 0;

                $all_reviews = $this->Custom->query('SELECT ratings_and_reviews.user_id,ratings_and_reviews.rating,ratings_and_reviews.review,ratings_and_reviews.created_at,user_profile.first_name,user_profile.last_name,user_profile.profile_pic from ratings_and_reviews JOIN user_profile ON user_profile.user_id = ratings_and_reviews.user_id where ratings_and_reviews.event_id = ' . $event_id);
                if (isset($all_reviews) && !empty($all_reviews)) {
                    foreach ($all_reviews as $reviewkey => $reviewvalue) {
                        if (preg_match("/http/i", $reviewvalue->profile_pic)) {
                            $all_reviews[$reviewkey]->profile_pic = $reviewvalue->profile_pic;
                        } else {
                            $all_reviews[$reviewkey]->profile_pic = ($reviewvalue->profile_pic) ? base_url() . USER_PROFILE_URL . $reviewvalue->profile_pic : "";
                        }
                    }
                } else {
                    $all_reviews = array();
                }
                $Events[0]->reviews = $all_reviews;

                if (!empty($Events[0]->common_allergies)) {
                    $alleries = explode(',', $Events[0]->common_allergies);
                    foreach ($alleries as $allergy) {
                        $allergy_where = "id = $allergy";
                        $Allergies = $this->Apimodel->GetAllergies($allergy_where, $this->language);
                        $AllergiesData[] = $Allergies[0]['allergy_name'];
                    }
                }
                $Events[0]->AllergiesData = ($AllergiesData) ? implode(',', $AllergiesData) : "";
                if (!empty($Events[0]->vegan_cuisine)) {
                    $vegan_cuisine = explode(',', $Events[0]->vegan_cuisine);
                    foreach ($vegan_cuisine as $VC) {
                        $cuisine_where = "id = $VC";
                        $VeganCuisine = $this->Apimodel->GetVeganCuisine($cuisine_where, $this->language);
                        $CuisineData[] = $VeganCuisine[0]['cuisine_name'];
                    }
                }
                $Events[0]->VeganCuisineData = ($CuisineData) ? implode(',', $CuisineData) : "";
                if (!empty($Events[0]->drinks)) {
                    $drinks = explode(',', $Events[0]->drinks);
                    foreach ($drinks as $drink) {
                        $drink_where = "id = $drink";
                        $Drinks = $this->Apimodel->GetDrinks($drink_where, $this->language);
                        $DrinkData[] = $Drinks[0]['drink_name'];
                    }
                }
                $Events[0]->DrinkData = ($DrinkData) ? implode(',', $DrinkData) : "";
                if (!empty($Events[0]->dessert)) {
                    $desserts = explode(',', $Events[0]->dessert);
                    foreach ($desserts as $dessert) {
                        $dessert_where = "id = $dessert";
                        $Dessert = $this->Apimodel->GetDesserts($dessert_where, $this->language);
                        $DessertData[] = $Dessert[0]['dessert_name'];
                    }
                }
                $Events[0]->DessertData = ($DessertData) ? implode(',', $DessertData) : "";
                //get next dates
                $event_next_date = array();
                $recurring_event = $Events[0]->recurring_event;
                $week_day_id = $Events[0]->week_day_id;
                $event_date = $Events[0]->event_next_date;
                //print_r($Events[0]); die('>>>>>>>>>>>>>');
                if (!empty($event_date)) {
                    $eventThreeMonthDate = date('Y-m-d', strtotime("+2 months", strtotime($event_date)));
                    if ($recurring_event == 1) {
                        $check_event_block = $this->Custom->get_where('cancelled_event', array('event_id' => $event_id, 'eventDate' => $event_date));
                        if (!isset($check_event_block) || empty($check_event_block))
                            $event_next_date[] = $event_date;
                        switch ($week_day_id):
                            case 1:
                                while (strtotime($eventThreeMonthDate) >= strtotime($event_date)) {
                                    $event_date = date('Y-m-d', strtotime('next monday', strtotime($event_date)));
                                    $check_event_block = $this->Custom->get_where('cancelled_event', array('event_id' => $event_id, 'eventDate' => $event_date));
                                    if (!isset($check_event_block) || empty($check_event_block))
                                        $event_next_date[] = $event_date;
                                }
                                break;
                            case 2:
                                while (strtotime($eventThreeMonthDate) >= strtotime($event_date)) {
                                    $event_date = date('Y-m-d', strtotime('next tuesday', strtotime($event_date)));
                                    $check_event_block = $this->Custom->get_where('cancelled_event', array('event_id' => $event_id, 'eventDate' => $event_date));
                                    if (!isset($check_event_block) || empty($check_event_block))
                                        $event_next_date[] = $event_date;
                                }
                                break;
                            case 3:
                                while (strtotime($eventThreeMonthDate) >= strtotime($event_date)) {
                                    $event_date = date('Y-m-d', strtotime('next wednesday', strtotime($event_date)));
                                    $check_event_block = $this->Custom->get_where('cancelled_event', array('event_id' => $event_id, 'eventDate' => $event_date));
                                    if (!isset($check_event_block) || empty($check_event_block))
                                        $event_next_date[] = $event_date;
                                }
                                break;
                            case 4:
                                while (strtotime($eventThreeMonthDate) >= strtotime($event_date)) {
                                    $event_date = date('Y-m-d', strtotime('next thursday', strtotime($event_date)));
                                    $check_event_block = $this->Custom->get_where('cancelled_event', array('event_id' => $event_id, 'eventDate' => $event_date));
                                    if (!isset($check_event_block) || empty($check_event_block))
                                        $event_next_date[] = $event_date;
                                }
                                break;
                            case 5:
                                while (strtotime($eventThreeMonthDate) >= strtotime($event_date)) {
                                    $event_date = date('Y-m-d', strtotime('next friday', strtotime($event_date)));
                                    $check_event_block = $this->Custom->get_where('cancelled_event', array('event_id' => $event_id, 'eventDate' => $event_date));
                                    if (!isset($check_event_block) || empty($check_event_block))
                                        $event_next_date[] = $event_date;
                                }
                                break;
                            case 6:
                                while (strtotime($eventThreeMonthDate) >= strtotime($event_date)) {
                                    $event_date = date('Y-m-d', strtotime('next saturday', strtotime($event_date)));
                                    $check_event_block = $this->Custom->get_where('cancelled_event', array('event_id' => $event_id, 'eventDate' => $event_date));
                                    if (!isset($check_event_block) || empty($check_event_block))
                                        $event_next_date[] = $event_date;
                                }
                                break;
                            case 7:
                                while (strtotime($eventThreeMonthDate) >= strtotime($event_date)) {
                                    $event_date = date('Y-m-d', strtotime('next sunday', strtotime($event_date)));
                                    $check_event_block = $this->Custom->get_where('cancelled_event', array('event_id' => $event_id, 'eventDate' => $event_date));
                                    if (!isset($check_event_block) || empty($check_event_block))
                                        $event_next_date[] = $event_date;
                                }
                                break;
                        endswitch;
                    }
                    if ($recurring_event == 2) {
                        $check_event_block = $this->Custom->get_where('cancelled_event', array('event_id' => $event_id, 'eventDate' => $event_date));
                        if (!isset($check_event_block) || empty($check_event_block))
                            $event_next_date[] = $event_date;
                        while (strtotime($eventThreeMonthDate) >= strtotime($event_date)) {
                            $event_date = date('Y-m-d', strtotime('+1 day', strtotime($event_date)));
                            $check_event_block = $this->Custom->get_where('cancelled_event', array('event_id' => $event_id, 'eventDate' => $event_date));
                            if (!isset($check_event_block) || empty($check_event_block))
                                $event_next_date[] = $event_date;
                        }
                    }
                }
                $Events[0]->EventNextDateArr = $event_next_date;

                if ($recurring_event == 1) {
                    $current_event_date = $Events[0]->event_date;
                    $next_event_date = $Events[0]->event_next_date;
                    if (strtotime($today) < strtotime($next_event_date)) {
                        $event_date = $current_event_date;
                    } else {
                        $event_date = $next_event_date;
                    }
                    $guest_count = $this->Custom->query("SELECT SUM(no_of_guests) as total_joined FROM event_bookings WHERE event_id = " . $Events[0]->id . " AND booking_date = '$event_date'");
                    $Events[0]->current_event_date = $event_date;
                    $Events[0]->JoinGuestCount = (isset($guest_count[0]->total_joined) && !empty($guest_count[0]->total_joined)) ? $guest_count[0]->total_joined : 0;
                } else if ($recurring_event == 2) {
                    $event_date = $Events[0]->event_next_date;
                    $guest_count = $this->Custom->query("SELECT SUM(no_of_guests) as total_joined FROM event_bookings WHERE event_id = " . $Events[0]->id . " AND booking_date = '$event_date'");
                    $Events[0]->current_event_date = $event_date;
                    $Events[0]->JoinGuestCount = (isset($guest_count[0]->total_joined) && !empty($guest_count[0]->total_joined)) ? $guest_count[0]->total_joined : 0;
                } else {
                    $event_date = $Events[0]->event_date;
                    $guest_count = $this->Custom->query("SELECT SUM(no_of_guests) as total_joined FROM event_bookings WHERE event_id = " . $Events[0]->id);
                    $Events[0]->current_event_date = $event_date;
                    $Events[0]->JoinGuestCount = (isset($guest_count[0]->total_joined) && !empty($guest_count[0]->total_joined)) ? $guest_count[0]->total_joined : 0;
                }

                //$guest_count = $this->Custom->query("SELECT SUM(no_of_guests) as total_joined FROM event_bookings WHERE event_id = " . $Events[0]->id);
                //$Events[0]->JoinGuestCount = (isset($guest_count[0]->total_joined) && !empty($guest_count[0]->total_joined)) ? $guest_count[0]->total_joined : 0;
                $Events[0]->credit_balance = $user_record[0]->credit_balance;
                //print_r($Events); die('>>>>>>>>>>');

                $this->response->success = 200;
                $this->response->message = $this->lang->line('events_details');
                $this->response->data = $Events[0];
                die(json_encode($this->response));
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function GetEventCities() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';
        $user_role = ($this->input->post('user_role')) ? $this->input->post('user_role') : '';

        if (!empty($user_id) && !empty($user_role)) {
            $user_record = $this->Custom->get_where('users', array('id' => $user_id, 'role' => $user_role));
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }

                /* if ($user_record[0]->status != 1) {
                  $this->response->success = 204;
                  $this->response->message = $this->lang->line('account_not_activate');
                  die(json_encode($this->response));
                  } */
                $EventCities = $this->Custom->query("select DISTINCT event_other_details.city from event INNER JOIN event_other_details ON event.id = event_other_details.event_id where (event_other_details.recurring_event != 0 OR event.event_date >= CURRENT_DATE())");
                if (empty($EventCities)) {
                    $this->response->success = 205;
                    $this->response->message = $this->lang->line('no_record');
                    die(json_encode($this->response));
                }
                $CityArr = array();
                foreach ($EventCities as $row) {
                    $CityArr[] = $row->city;
                }
                $this->response->success = 200;
                $this->response->message = $this->lang->line('city_list');
                $this->response->data = $CityArr;
                die(json_encode($this->response));
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function RequestBooking() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';
        $event_id = ($this->input->post('event_id')) ? $this->input->post('event_id') : '';
        $booking_date = ($this->input->post('booking_date')) ? $this->input->post('booking_date') : '';
        $no_of_guests = ($this->input->post('no_of_guests')) ? $this->input->post('no_of_guests') : '';
        $promocode = ($this->input->post('promocode')) ? $this->input->post('promocode') : '';
        $discount = ($this->input->post('discount')) ? $this->input->post('discount') : 0;
        $credit_applied = ($this->input->post('credit_applied')) ? $this->input->post('credit_applied') : 0.00;
        $donation = ($this->input->post('donation')) ? $this->input->post('donation') : 0;
        $donate_to = ($this->input->post('donate_to')) ? $this->input->post('donate_to') : '';
        $booking_fee = ($this->input->post('booking_fee')) ? $this->input->post('booking_fee') : '';
        $sub_total = ($this->input->post('sub_total')) ? $this->input->post('sub_total') : '';
        $total_amount = ($this->input->post('total_amount')) ? $this->input->post('total_amount') : '';
        $introduction = ($this->input->post('introduction')) ? $this->input->post('introduction') : '';
        $phone_number = ($this->input->post('phone_number')) ? $this->input->post('phone_number') : '';
        $card_id = ($this->input->post('card_id')) ? $this->input->post('card_id') : '';
        $transaction_token = ($this->input->post('transaction_token')) ? $this->input->post('transaction_token') : '';
        $payment_status = ($this->input->post('payment_status')) ? $this->input->post('payment_status') : '';
        $payment_mode = ($this->input->post('payment_mode')) ? $this->input->post('payment_mode') : '';

        if (isset($user_id) && !empty($user_id) && isset($sub_total) && !empty($sub_total) && isset($event_id) && !empty($event_id) && isset($booking_date) && !empty($booking_date) && isset($no_of_guests) && !empty($no_of_guests) && isset($total_amount) && !empty($total_amount) && isset($introduction) && !empty($introduction) && isset($phone_number) && !empty($phone_number) && isset($card_id) && !empty($card_id) && isset($payment_status) && !empty($payment_status)) {
            $user_record = $this->Custom->get_where('users', array('id' => $user_id));
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }

                $check_limit = $this->Custom->query("SELECT sum(no_of_guests) as total_booked FROM event_bookings WHERE booking_date = '$booking_date' AND event_id = $event_id AND status != 2");
                $check_event = $this->Custom->get_where('event', array('id' => $event_id));
                $check_event_details = $this->Custom->get_where('event_other_details', array('event_id' => $event_id));
                if ($check_event[0]->event_type == 2) {
                    if (isset($check_limit) && !empty($check_limit)) {
                        $total_booked = $check_limit[0]->total_booked;
                        $max_limit = $check_event_details[0]->max_guest;
                        $available_limit = $max_limit - $total_booked;
                        if ($no_of_guests > $available_limit) {
                            $this->response->success = 210;
                            $this->response->message = "Guest limit exceeds from available limit.";
                            $this->response->available_limit = $available_limit;
                            die(json_encode($this->response));
                        }
                    }
                }
                $check_event_log = $this->Custom->get_where('event_log', array('eventDate' => $booking_date, 'event_id' => $event_id));
                if (!isset($check_event_log) || empty($check_event_log)) {
                    $this->Custom->insert_data('event_log', array('eventDate' => $booking_date, 'event_id' => $event_id, 'event_status' => 0));
                }

                $_POST['status'] = 0;
                $_POST['refund_id'] = '';
                $_POST['event_host_id'] = $check_event[0]->user_id;
                $_POST['created'] = date("Y-m-d H:i:s");
                $_POST['updated_at'] = date("Y-m-d H:i:s");

                $insert_id = $this->Custom->insert_data('event_bookings', $_POST);
                if ($insert_id) {
                    if (!empty($credit_applied) && $credit_applied > 0.00) {
                        $available_balance = $user_record[0]->credit_balance;
                        if ($credit_applied <= $available_balance) {
                            $new_credit_balance = $available_balance - $credit_applied;
                            $this->Custom->update('users', array('credit_balance' => $new_credit_balance), 'id', $user_id);
                        }
                    }
                    $this->response->success = 200;
                    $this->response->message = $this->lang->line('event_booking_request_sent');
                    die(json_encode($this->response));
                } else {
                    $this->response->success = 205;
                    $this->response->message = $this->lang->line('event_booking_request_not_sent');
                    die(json_encode($this->response));
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function EventGuestAvailableLimit() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';
        $event_id = ($this->input->post('event_id')) ? $this->input->post('event_id') : '';
        $booking_date = ($this->input->post('booking_date')) ? $this->input->post('booking_date') : '';

        if (isset($user_id) && !empty($user_id) && isset($event_id) && !empty($event_id) && isset($booking_date) && !empty($booking_date)) {
            $user_record = $this->Custom->get_where('users', array('id' => $user_id));
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }

                $check_limit = $this->Custom->query("SELECT sum(no_of_guests) as total_booked FROM event_bookings WHERE booking_date = '$booking_date' AND event_id = $event_id AND status != 2");
                $check_event = $this->Custom->get_where('event', array('id' => $event_id));
                $check_event_details = $this->Custom->get_where('event_other_details', array('event_id' => $event_id));
                $available_limit = 0;
                if (isset($check_limit) && !empty($check_limit)) {
                    $total_booked = $check_limit[0]->total_booked;
                    $max_limit = $check_event_details[0]->max_guest;
                    $available_limit = $max_limit - $total_booked;
                }
                $this->response->success = 200;
                $this->response->message = "Guest limit";
                $this->response->available_limit = $available_limit;
                die(json_encode($this->response));
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function GetEventBookings() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';
        $user_role = ($this->input->post('user_role')) ? $this->input->post('user_role') : '';
        $user_type = ($this->input->post('user_type')) ? $this->input->post('user_type') : '';

        if (!empty($user_id) && !empty($user_role) && isset($user_type) && !empty($user_type)) {
            $user_record = $this->Custom->get_where('users', array('id' => $user_id, 'role' => $user_role));
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }

                $EventBookings = '';
                if ($user_type == 'Host') {
                    $EventBookings = $this->Custom->query("select event.id, event.title, event.start_time, event.meal_type, event_bookings.booking_date, event_bookings.id as booking_id, event_bookings.no_of_guests, event_bookings.status as booking_status, event_bookings.event_host_id, user_profile.first_name, user_profile.last_name, user_profile.profile_pic from event INNER JOIN event_bookings ON event.id = event_bookings.event_id INNER JOIN user_profile ON event_bookings.user_id = user_profile.user_id where event_bookings.event_host_id = $user_id ORDER BY event_bookings.created DESC");
                } else {
                    $EventBookings = $this->Custom->query("select event.id, event.title, event.start_time, event.meal_type, event_bookings.booking_date, event_bookings.id as booking_id, event_bookings.no_of_guests, event_bookings.status as booking_status, event_bookings.event_host_id, user_profile.first_name, user_profile.last_name, user_profile.profile_pic from event INNER JOIN event_bookings ON event.id = event_bookings.event_id INNER JOIN user_profile ON event_bookings.event_host_id = user_profile.user_id where event_bookings.user_id = $user_id ORDER BY event_bookings.created DESC");
                }
                if (!isset($EventBookings) || empty($EventBookings)) {
                    $this->response->success = 205;
                    $this->response->message = $this->lang->line('no_record');
                    die(json_encode($this->response));
                }
                $BookingRequestArr = array();
                $BookingConfirmedArr = array();
                $BookingPreviousArr = array();
                $current_date = date("Y-m-d");
                foreach ($EventBookings as $row) {
                    $row->credit_balance = $user_record[0]->credit_balance;
                    $payment_url = $this->Custom->get_where('event_booking_payments', array('token' => $row->transaction_token, 'event_id' => $row->event_id));
                    if (isset($payment_url) && !empty($payment_url)) {
                        $row->payment_url = $payment_url[0]->payment_url;
                    } else {
                        $row->payment_url = '';
                    }
                    $row->profile_pic = ($row->profile_pic) ? base_url() . USER_PROFILE_URL . $row->profile_pic : "";
                    if ($user_type != 'Host' && $row->booking_status == 3) {
                        $BookingPreviousArr[] = $row;
                    }
                    $guest_count = $this->Custom->query("SELECT SUM(no_of_guests) as total_joined FROM event_bookings WHERE event_id = " . $row->id);
                    $row->JoinGuestCount = (isset($guest_count[0]->total_joined) && !empty($guest_count[0]->total_joined)) ? $guest_count[0]->total_joined : 0;
                    if ($row->booking_status == 0) {
                        $BookingRequestArr[] = $row;
                    } else if ($row->booking_status == 1 || $row->booking_status == 2) {
                        if (strtotime($current_date) >= strtotime($row->booking_date) && $row->booking_status == 1) {
                            $BookingConfirmedArr[] = $row;
                        } else {
                            $BookingPreviousArr[] = $row;
                        }
                    }
                    //$BookingArr[] = $row;
                }
                $this->response->success = 200;
                $this->response->message = $this->lang->line('event_booking_list');
                $this->response->BookingRequest = $BookingRequestArr;
                $this->response->BookingConfirmed = $BookingConfirmedArr;
                $this->response->BookingPreviou = $BookingPreviousArr;
                die(json_encode($this->response));
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function ApproveBooking() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';
        $booking_id = ($this->input->post('booking_id')) ? $this->input->post('booking_id') : '';

        if (isset($user_id) && !empty($user_id) && isset($booking_id) && !empty($booking_id)) {
            $user_record = $this->Custom->get_where('users', array('id' => $user_id));
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }

                $bookingData = $this->Custom->get_where('event_bookings', array('id' => $booking_id));
                if (isset($bookingData) && !empty($bookingData)) {
                    $bookingUser_id = $bookingData[0]->user_id;
                    $requestedUser = $this->Custom->get_where('users', array('id' => $bookingData[0]->user_id));
                    $sub_total = (isset($bookingData[0]->sub_total) && !empty($bookingData[0]->sub_total)) ? $bookingData[0]->sub_total : '';

                    if (isset($bookingData[0]->payment_mode) && !empty($bookingData[0]->payment_mode) && $bookingData[0]->payment_mode == 'crypto') {
                        $host_id = (isset($bookingData[0]->event_host_id) && !empty($bookingData[0]->event_host_id)) ? $bookingData[0]->event_host_id : '';
                        $this->Custom->update('event_bookings', array('updated_at' => date("Y-m-d H:i:s"), 'status' => 1), 'id', $booking_id);
                        if (!empty($host_id)) {
                            $hostUser = $this->Custom->get_where('user_profile', array('user_id' => $host_id));
                            $first_name = (isset($hostUser[0]->first_name) && !empty($hostUser[0]->first_name)) ? $hostUser[0]->first_name : '';
                            $last_name = (isset($hostUser[0]->last_name) && !empty($hostUser[0]->last_name)) ? $hostUser[0]->last_name : '';
                            $device_type = (isset($requestedUser[0]->device_type) && !empty($requestedUser[0]->device_type)) ? $requestedUser[0]->device_type : '';
                            $device_token = (isset($requestedUser[0]->device_token) && !empty($requestedUser[0]->device_token)) ? $requestedUser[0]->device_token : '';
                            $message = "$first_name $last_name has approved your reservation, you have 2 hours to complete your Litecoin/Bitcoin payment otherwise your reservation will be cancelled.";
                            if (!empty($device_type) && !empty($device_token)) {
                                $this->send_notification($device_type, $device_token, $message, $booking_id, 'PaymentRequest');
                            }
                            if ($sub_total >= 50) {
                                $invite_refer_code = (isset($requestedUser[0]->invite_refer_code) && !empty($requestedUser[0]->invite_refer_code)) ? $requestedUser[0]->invite_refer_code : '';
                                if (!empty($invite_refer_code)) {
                                    $get_refer_user = $this->Custom->get_where('users', array('refer_code' => $invite_refer_code));
                                    if (isset($get_refer_user) && !empty($get_refer_user)) {
                                        $refer_user_id = $get_refer_user[0]->id;
                                        $refer_credit_balance = $get_refer_user[0]->credit_balance;
                                        $refer_new_credit_balance = $refer_credit_balance + 10;
                                        $credit_balance = $requestedUser[0]->credit_balance;
                                        $new_credit_balance = $credit_balance + 20;
                                        $check_claim = $this->Custom->get_where('user_wallet', array('from_user' => $refer_user_id, 'user_id' => $bookingUser_id));
                                        if (!isset($check_claim) || empty($check_claim)) {
                                            $referer_insert = array(
                                                'from_user' => $bookingUser_id,
                                                'user_id' => $refer_user_id,
                                                'credits' => 10,
                                                'created' => date("Y-m-d H:i:s")
                                            );
                                            $this->Custom->insert_data('user_wallet', $referer_insert);
                                            $this->Custom->update('users', array('credit_balance' => $refer_new_credit_balance), 'id', $refer_user_id);
                                            $this->Custom->update('users', array('credit_balance' => $new_credit_balance), 'id', $bookingUser_id);

                                            $refered_insert = array(
                                                'from_user' => $refer_user_id,
                                                'user_id' => $bookingUser_id,
                                                'credits' => 20,
                                                'created' => date("Y-m-d H:i:s")
                                            );
                                            $this->Custom->insert_data('user_wallet', $refered_insert);
                                        }
                                    }
                                }
                            }
                            $this->response->success = 200;
                            $this->response->message = "Request approved.";
                            die(json_encode($this->response));
                        }
                    } else {
                        if (isset($requestedUser[0]->stripe_cus_id) && !empty($requestedUser[0]->stripe_cus_id)) {
                            try {
                                $charge = \Stripe\Charge::create(array(
                                            "amount" => ($bookingData[0]->total_amount * 100),
                                            "currency" => "usd",
                                            "customer" => $requestedUser[0]->stripe_cus_id,
                                            "source" => $bookingData[0]->card_id,
                                            "description" => "Charge for " . $user_record[0]->email
                                ));
                                if ($this->Custom->update('event_bookings', array('transaction_token' => $charge->id, 'payment_status' => 'paid', 'status' => 1), 'id', $booking_id)) {
                                    if (isset($bookingData[0]->promocode) && !empty($bookingData[0]->promocode)) {
                                        $this->Custom->update_where('promocode_usage', array('status' => 1), array('promocode' => $bookingData[0]->promocode, 'user_id' => $user_id));
                                    }

                                    if ($sub_total >= 50) {
                                        $invite_refer_code = (isset($requestedUser[0]->invite_refer_code) && !empty($requestedUser[0]->invite_refer_code)) ? $requestedUser[0]->invite_refer_code : '';
                                        if (!empty($invite_refer_code)) {
                                            $get_refer_user = $this->Custom->get_where('users', array('refer_code' => $invite_refer_code));
                                            if (isset($get_refer_user) && !empty($get_refer_user)) {
                                                $refer_user_id = $get_refer_user[0]->id;
                                                $refer_credit_balance = $get_refer_user[0]->credit_balance;
                                                $refer_new_credit_balance = $refer_credit_balance + 10;
                                                $credit_balance = $requestedUser[0]->credit_balance;
                                                $new_credit_balance = $credit_balance + 20;

                                                $check_claim = $this->Custom->get_where('user_wallet', array('from_user' => $refer_user_id, 'user_id' => $bookingUser_id));
                                                if (!isset($check_claim) || empty($check_claim)) {
                                                    $referer_insert = array(
                                                        'from_user' => $bookingUser_id,
                                                        'user_id' => $refer_user_id,
                                                        'credits' => 10,
                                                        'created' => date("Y-m-d H:i:s")
                                                    );
                                                    $this->Custom->insert_data('user_wallet', $referer_insert);
                                                    $this->Custom->update('users', array('credit_balance' => $refer_new_credit_balance), 'id', $refer_user_id);
                                                    $this->Custom->update('users', array('credit_balance' => $new_credit_balance), 'id', $bookingUser_id);

                                                    $refered_insert = array(
                                                        'from_user' => $refer_user_id,
                                                        'user_id' => $bookingUser_id,
                                                        'credits' => 20,
                                                        'created' => date("Y-m-d H:i:s")
                                                    );
                                                    $this->Custom->insert_data('user_wallet', $refered_insert);
                                                }
                                            }
                                        }
                                    }

                                    $this->response->success = 200;
                                    $this->response->message = "Payment successful.";
                                    $this->response->data = $charge;
                                    die(json_encode($this->response));
                                } else {
                                    $this->response->success = 203;
                                    $this->response->message = "Payment failed.";
                                    die(json_encode($this->response));
                                }
                            } catch (\Stripe\Error\Base $e) {
                                $error2 = $e->getMessage();
                                $this->response->success = 203;
                                $this->response->message = $error2;
                                die(json_encode($this->response));
                            } catch (Exception $e) {
                                $error2 = $e->getMessage();
                                $this->response->success = 203;
                                $this->response->message = $error2;
                                die(json_encode($this->response));
                            }
                        } else {
                            $this->response->success = 203;
                            $this->response->message = "stripe_client_id not found. Please add customer first";
                            die(json_encode($this->response));
                        }
                    }
                } else {
                    $this->response->success = 203;
                    $this->response->message = "Booking not found.";
                    die(json_encode($this->response));
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function DeclineBooking() {
        $booking_id = ($this->input->post('booking_id')) ? $this->input->post('booking_id') : '';
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';

        if (isset($booking_id) && !empty($booking_id) && isset($user_id) && !empty($user_id)) {
            $user_record = $this->Custom->get_where('users', array('id' => $user_id));
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }
                $booking_record = $this->Custom->get_data('event_bookings', 'id', $booking_id);
                if (!isset($booking_record) || empty($booking_record)) {
                    $this->response->success = 205;
                    $this->response->message = $this->lang->line('no_record');
                    die(json_encode($this->response));
                }

                if ($this->Custom->update("event_bookings", array('status' => 2), 'id', $booking_id)) {
                    $credit_applied = (isset($booking_record[0]->credit_applied) && !empty($booking_record[0]->credit_applied) && $booking_record[0]->credit_applied > 0.00) ? $booking_record[0]->credit_applied : '';
                    if (!empty($credit_applied)) {
                        $available_balance = $user_record[0]->credit_balance;
                        $new_balance = $available_balance + $credit_applied;
                        $this->Custom->update('users', array('credit_balance' => $new_balance), 'id', $user_id);
                    }
                    $this->response->success = 200;
                    $this->response->message = "Booking Declined";
                    die(json_encode($this->response));
                } else {
                    $this->response->success = 202;
                    $this->response->message = $this->lang->line('something_went_wrong');
                    die(json_encode($this->response));
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function CancelBooking() {
        $booking_id = ($this->input->post('booking_id')) ? $this->input->post('booking_id') : '';
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';

        if (isset($booking_id) && !empty($booking_id) && isset($user_id) && !empty($user_id)) {
            $user_record = $this->Custom->get_where('users', array('id' => $user_id));
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }
                $booking_record = $this->Custom->get_data('event_bookings', 'id', $booking_id);
                if (!isset($booking_record) || empty($booking_record)) {
                    $this->response->success = 205;
                    $this->response->message = $this->lang->line('no_record');
                    die(json_encode($this->response));
                }

                if ($this->Custom->update("event_bookings", array('status' => 3), 'id', $booking_id)) {
                    $credit_applied = (isset($booking_record[0]->credit_applied) && !empty($booking_record[0]->credit_applied) && $booking_record[0]->credit_applied > 0.00) ? $booking_record[0]->credit_applied : '';
                    if (!empty($credit_applied)) {
                        $available_balance = $user_record[0]->credit_balance;
                        $new_balance = $available_balance + $credit_applied;
                        $this->Custom->update('users', array('credit_balance' => $new_balance), 'id', $user_id);
                    }
                    $this->response->success = 200;
                    $this->response->message = "Booking Canceled";
                    die(json_encode($this->response));
                } else {
                    $this->response->success = 202;
                    $this->response->message = $this->lang->line('something_went_wrong');
                    die(json_encode($this->response));
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function EventBookingDetails() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';
        $booking_id = ($this->input->post('booking_id')) ? $this->input->post('booking_id') : '';
        $user_role = ($this->input->post('user_role')) ? $this->input->post('user_role') : '';
        $user_type = ($this->input->post('user_type')) ? $this->input->post('user_type') : '';

        if (!empty($user_id) && !empty($user_role) && isset($user_type) && !empty($user_type)) {
            $user_record = $this->Custom->get_where('users', array('id' => $user_id, 'role' => $user_role));
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }

                $EventBookings = '';
                $BookingRequestArr = array();
                if ($user_type == 'Host') {
                    $EventBookings = $this->Custom->query("select event.*, event_bookings.*, event_bookings.id as booking_id, event_other_details.*, user_profile.first_name, user_profile.last_name, user_profile.profile_pic from event INNER JOIN event_bookings ON event.id = event_bookings.event_id INNER JOIN user_profile ON event_bookings.user_id = user_profile.user_id INNER JOIN event_other_details ON event.id = event_other_details.event_id where event_bookings.id = $booking_id");
                } else {
                    $EventBookings = $this->Custom->query("select event.*, event_bookings.*, event_bookings.id as booking_id, event_other_details.*, user_profile.first_name, user_profile.last_name, user_profile.profile_pic from event INNER JOIN event_bookings ON event.id = event_bookings.event_id INNER JOIN user_profile ON event_bookings.event_host_id = user_profile.user_id INNER JOIN event_other_details ON event.id = event_other_details.event_id where event_bookings.id = $booking_id");
                }
                if (!isset($EventBookings) || empty($EventBookings)) {
                    $this->response->success = 205;
                    $this->response->message = $this->lang->line('no_record');
                    die(json_encode($this->response));
                }
                $current_date = date("Y-m-d");
                foreach ($EventBookings as $row) {
                    $row->credit_balance = $user_record[0]->credit_balance;
                    $payment_url = $this->Custom->get_where('event_booking_payments', array('token' => $row->transaction_token, 'event_id' => $row->event_id));
                    if (isset($payment_url) && !empty($payment_url)) {
                        $row->payment_url = $payment_url[0]->payment_url;
                    } else {
                        $row->payment_url = '';
                    }
                    $row->profile_pic = ($row->profile_pic) ? base_url() . USER_PROFILE_URL . $row->profile_pic : "";
                    $row->cover_pic = $this->Apimodel->GetEventCoverImage($row->event_id);
                    $review_data = $this->Custom->query("SELECT AVG(rating) as avg_rating, count(*) as total_review from ratings_and_reviews where event_id = $row->event_id");
                    $row->rating = (isset($review_data[0]->rating) && !empty($review_data[0]->rating)) ? $review_data[0]->rating : 0.0;
                    $row->total_review = (isset($review_data) && !empty($review_data)) ? $review_data[0]->total_review : 0;

                    $review_status = $this->Custom->query("SELECT * from ratings_and_reviews where event_id = $row->event_id AND user_id = $user_id AND event_date = '$row->booking_date'");
                    if (isset($review_status) && !empty($review_status)) {
                        $row->event_reviewed = 1;
                    } else {
                        $row->event_reviewed = 0;
                    }

                    if (!empty($row->common_allergies)) {
                        $alleries = explode(',', $row->common_allergies);
                        foreach ($alleries as $allergy) {
                            $allergy_where = "id = $allergy";
                            $Allergies = $this->Apimodel->GetAllergies($allergy_where, $this->language);
                            $AllergiesData[] = $Allergies[0]['allergy_name'];
                        }
                    }
                    $row->AllergiesData = ($AllergiesData) ? implode(',', $AllergiesData) : "";
                    if (!empty($row->vegan_cuisine)) {
                        $vegan_cuisine = explode(',', $row->vegan_cuisine);
                        foreach ($vegan_cuisine as $VC) {
                            $cuisine_where = "id = $VC";
                            $VeganCuisine = $this->Apimodel->GetVeganCuisine($cuisine_where, $this->language);
                            $CuisineData[] = $VeganCuisine[0]['cuisine_name'];
                        }
                    }
                    $row->VeganCuisineData = ($CuisineData) ? implode(',', $CuisineData) : "";
                    if (!empty($row->drinks)) {
                        $drinks = explode(',', $row->drinks);
                        foreach ($drinks as $drink) {
                            $drink_where = "id = $drink";
                            $Drinks = $this->Apimodel->GetDrinks($drink_where, $this->language);
                            $DrinkData[] = $Drinks[0]['drink_name'];
                        }
                    }
                    $row->DrinkData = ($DrinkData) ? implode(',', $DrinkData) : "";
                    if (!empty($row->dessert)) {
                        $desserts = explode(',', $row->dessert);
                        foreach ($desserts as $dessert) {
                            $dessert_where = "id = $dessert";
                            $Dessert = $this->Apimodel->GetDesserts($dessert_where, $this->language);
                            $DessertData[] = $Dessert[0]['dessert_name'];
                        }
                    }
                    $row->DessertData = ($DessertData) ? implode(',', $DessertData) : "";
                    $host_data = $this->Custom->query("SELECT address FROM user_profile WHERE user_id = " . $row->event_host_id);
                    $row->host_address = (isset($host_data[0]->address) && !empty($host_data[0]->address)) ? $host_data[0]->address : '';
                    $guest_count = $this->Custom->query("SELECT SUM(no_of_guests) as total_joined FROM event_bookings WHERE event_id = " . $row->event_id . "  AND booking_date = '$row->booking_date'");
                    $row->JoinGuestCount = (isset($guest_count[0]->total_joined) && !empty($guest_count[0]->total_joined)) ? $guest_count[0]->total_joined : 0;
                    $BookingRequestArr[] = $row;
                }
                $this->response->success = 200;
                $this->response->message = $this->lang->line('event_booking_list');
                $this->response->data = $BookingRequestArr[0];
                die(json_encode($this->response));
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function apply_promocode() {
        $user_id = $this->input->post("user_id");
        $promocode_id = $this->input->post("promocode");
        $used_date = date("Y-m-d");

        if (isset($user_id) && !empty($user_id) && isset($promocode_id) && !empty($promocode_id) && !empty($used_date)) {
            $promo_codes = $this->Custom->get_where('promocodes', array('promocode' => $promocode_id));
            if (isset($promo_codes) && !empty($promo_codes)) {
                $expiry_date = $promo_codes[0]->expiry_date;
                if (strtotime($used_date) > strtotime($expiry_date)) {
                    $this->response->success = 203;
                    $this->response->message = "Promocode has been expired.";
                    die(json_encode($this->response));
                }

                $check_promo_code_used = $this->Custom->get_where('promocode_usage', array('promocode' => $promocode_id, 'user_id' => $user_id));

                if (isset($check_promo_code_used) && !empty($check_promo_code_used)) {
                    if ($check_promo_code_used[0]->status == 0) {
                        $this->Custom->update_where('promocode_usage', array('used_date' => $used_date), array('promocode' => $promocode_id, 'user_id' => $user_id));
                        $this->response->success = 200;
                        $this->response->message = "Promocode applied Successfully";
                        $this->response->data = $promo_codes[0];
                        die(json_encode($this->response));
                    } else {
                        $this->response->success = 203;
                        $this->response->message = "Promocode already used.";
                        die(json_encode($this->response));
                    }
                }

                $insert_data = array(
                    'user_id' => $user_id,
                    'promocode' => $promocode_id,
                    'used_date' => $used_date,
                    'created' => date("Y-m-d H:i:s")
                );

                $insert_id = $this->Custom->insert_data('promocode_usage', $insert_data);
                if (!empty($insert_id)) {
                    $this->response->success = 200;
                    $this->response->message = "Promocode applied Successfully";
                    $this->response->data = $promo_codes[0];
                    die(json_encode($this->response));
                } else {
                    $this->response->success = 203;
                    $this->response->message = "Promocode not applied.";
                    die(json_encode($this->response));
                }
            } else {
                $this->response->success = 202;
                $this->response->message = "Invalid promocode";
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function RatingsAndReviews() {
        $user_id = ($this->input->post('user_id', TRUE)) ? $this->input->post('user_id', TRUE) : '';
        $user_role = ($this->input->post('user_role')) ? $this->input->post('user_role') : '';
        $event_id = ($this->input->post('event_id', TRUE)) ? $this->input->post('event_id', TRUE) : '';
        $event_date = ($this->input->post('event_date', TRUE)) ? $this->input->post('event_date', TRUE) : '';
        $rating = ($this->input->post('rating', TRUE)) ? $this->input->post('rating', TRUE) : '';
        $review = ($this->input->post('review', TRUE)) ? $this->input->post('review', TRUE) : '';

        if (isset($user_id) && !empty($user_id) && isset($event_id) && !empty($event_id) && isset($rating) && !empty($rating) && isset($event_date) && !empty($event_date)) {
            $user_record = $this->Custom->get_where('users', array('id' => $user_id, 'role' => $user_role));
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }
                $review_data = $this->Custom->get_where('ratings_and_reviews', array('user_id' => $user_id, 'event_id' => $event_id, 'event_date' => $event_date));
                if (!empty($review_data)) {
                    $this->response->success = 205;
                    $this->response->message = "You have already rated this event.";
                    die(json_encode($this->response));
                } else {
                    $insertArr = array(
                        'user_id' => $user_id,
                        'event_id' => $event_id,
                        'event_date' => $event_date,
                        'rating' => $rating,
                        'review' => $review,
                        'created_at' => date('Y-m-d H:i:s')
                    );
                    $res = $this->Custom->insert_data('ratings_and_reviews', $insertArr);
                    if ($res) {
                        $this->response->success = 200;
                        $this->response->message = "Rated successfully.";
                        die(json_encode($this->response));
                    } else {
                        $this->response->success = 202;
                        $this->response->message = $this->lang->line('something_went_wrong');
                        die(json_encode($this->response));
                    }
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function checkChat() {
        $user_id = ($this->input->post('user_id', TRUE)) ? $this->input->post('user_id', TRUE) : '';
        $receiver_id = ($this->input->post('receiver_id', TRUE)) ? $this->input->post('receiver_id', TRUE) : '';
        $user_role = ($this->input->post('user_role')) ? $this->input->post('user_role') : '';

        if (isset($user_id) && !empty($user_id) && isset($receiver_id) && !empty($receiver_id)) {
            $user_record = $this->Custom->get_where('users', array('id' => $user_id, 'role' => $user_role));
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }
                $check_chat = $this->Custom->query("SELECT * FROM chats WHERE (user1 = $user_id AND user2 = $receiver_id) OR (user1 = $receiver_id AND user2 = $user_id)");
                if (!isset($check_chat) || empty($check_chat)) {
                    $this->response->success = 205;
                    $this->response->message = "No chat found";
                    die(json_encode($this->response));
                } else {
                    $this->response->success = 200;
                    $this->response->message = "Chat";
                    $this->response->chat_id = ($check_chat[0]->id) ? $check_chat[0]->id : '';
                    die(json_encode($this->response));
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function upcomingEventDates() {
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';
        $event_id = ($this->input->post('event_id')) ? $this->input->post('event_id') : '';
        $user_role = ($this->input->post('user_role')) ? $this->input->post('user_role') : '';

        if (!empty($user_id) && !empty($event_id) && !empty($user_role)) {
            $user_record = $this->Custom->get_where('users', array('id' => $user_id, 'role' => $user_role));
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }

                /* if ($user_record[0]->status != 1) {
                  $this->response->success = 204;
                  $this->response->message = $this->lang->line('account_not_activate');
                  die(json_encode($this->response));
                  } */
                $where = "event.id = $event_id";
                $Events = $this->Apimodel->GetEvents($where);
                if (empty($Events)) {
                    $this->response->success = 203;
                    $this->response->message = $this->lang->line('invalid_event');
                    die(json_encode($this->response));
                }
                //get next dates
                $event_next_date = array();
                $recurring_event = $Events[0]->recurring_event;
                $week_day_id = $Events[0]->week_day_id;
                $event_date = $Events[0]->event_next_date;
                //print_r($Events[0]); die('>>>>>>>>>>>>>');
                if (!empty($event_date)) {
                    $eventThreeMonthDate = date('Y-m-d', strtotime("+2 months", strtotime($event_date)));
                    if ($recurring_event == 1) {
                        $guest_count = $this->Custom->query("SELECT SUM(no_of_guests) as total_joined FROM event_bookings WHERE event_id = " . $Events[0]->id . " AND booking_date = '$event_date'");
                        $check_cancel = $this->Custom->get_where('cancelled_event', array('event_id' => $event_id, 'eventDate' => $event_date));
                        $booking_data = array(
                            'event_date' => $event_date,
                            'JoinGuestCount' => (isset($guest_count[0]->total_joined) && !empty($guest_count[0]->total_joined)) ? $guest_count[0]->total_joined : 0,
                            'isCancelled' => (isset($check_cancel) && !empty($check_cancel)) ? 1 : 0
                        );
                        /* $check_event_block = $this->Custom->get_where('cancelled_event', array('event_id' => $event_id, 'eventDate' => $event_date));
                          if(!isset($check_event_block) || empty($check_event_block)) */
                        $event_next_date[] = $booking_data;
                        switch ($week_day_id):
                            case 1:
                                while (strtotime($eventThreeMonthDate) >= strtotime($event_date)) {
                                    $event_date = date('Y-m-d', strtotime('next monday', strtotime($event_date)));
                                    $guest_count = $this->Custom->query("SELECT SUM(no_of_guests) as total_joined FROM event_bookings WHERE event_id = " . $Events[0]->id . " AND booking_date = '$event_date'");
                                    $check_cancel = $this->Custom->get_where('cancelled_event', array('event_id' => $event_id, 'eventDate' => $event_date));
                                    $booking_data = array(
                                        'event_date' => $event_date,
                                        'JoinGuestCount' => (isset($guest_count[0]->total_joined) && !empty($guest_count[0]->total_joined)) ? $guest_count[0]->total_joined : 0,
                                        'isCancelled' => (isset($check_cancel) && !empty($check_cancel)) ? 1 : 0
                                    );
                                    $event_next_date[] = $booking_data;
                                }
                                break;
                            case 2:
                                while (strtotime($eventThreeMonthDate) >= strtotime($event_date)) {
                                    $event_date = date('Y-m-d', strtotime('next tuesday', strtotime($event_date)));
                                    $guest_count = $this->Custom->query("SELECT SUM(no_of_guests) as total_joined FROM event_bookings WHERE event_id = " . $Events[0]->id . " AND booking_date = '$event_date'");
                                    $check_cancel = $this->Custom->get_where('cancelled_event', array('event_id' => $event_id, 'eventDate' => $event_date));
                                    $booking_data = array(
                                        'event_date' => $event_date,
                                        'JoinGuestCount' => (isset($guest_count[0]->total_joined) && !empty($guest_count[0]->total_joined)) ? $guest_count[0]->total_joined : 0,
                                        'isCancelled' => (isset($check_cancel) && !empty($check_cancel)) ? 1 : 0
                                    );
                                    $event_next_date[] = $booking_data;
                                }
                                break;
                            case 3:
                                while (strtotime($eventThreeMonthDate) >= strtotime($event_date)) {
                                    $event_date = date('Y-m-d', strtotime('next wednesday', strtotime($event_date)));
                                    $guest_count = $this->Custom->query("SELECT SUM(no_of_guests) as total_joined FROM event_bookings WHERE event_id = " . $Events[0]->id . " AND booking_date = '$event_date'");
                                    $check_cancel = $this->Custom->get_where('cancelled_event', array('event_id' => $event_id, 'eventDate' => $event_date));
                                    $booking_data = array(
                                        'event_date' => $event_date,
                                        'JoinGuestCount' => (isset($guest_count[0]->total_joined) && !empty($guest_count[0]->total_joined)) ? $guest_count[0]->total_joined : 0,
                                        'isCancelled' => (isset($check_cancel) && !empty($check_cancel)) ? 1 : 0
                                    );
                                    $event_next_date[] = $booking_data;
                                }
                                break;
                            case 4:
                                while (strtotime($eventThreeMonthDate) >= strtotime($event_date)) {
                                    $event_date = date('Y-m-d', strtotime('next thursday', strtotime($event_date)));
                                    $guest_count = $this->Custom->query("SELECT SUM(no_of_guests) as total_joined FROM event_bookings WHERE event_id = " . $Events[0]->id . " AND booking_date = '$event_date'");
                                    $check_cancel = $this->Custom->get_where('cancelled_event', array('event_id' => $event_id, 'eventDate' => $event_date));
                                    $booking_data = array(
                                        'event_date' => $event_date,
                                        'JoinGuestCount' => (isset($guest_count[0]->total_joined) && !empty($guest_count[0]->total_joined)) ? $guest_count[0]->total_joined : 0,
                                        'isCancelled' => (isset($check_cancel) && !empty($check_cancel)) ? 1 : 0
                                    );
                                    $event_next_date[] = $booking_data;
                                }
                                break;
                            case 5:
                                while (strtotime($eventThreeMonthDate) >= strtotime($event_date)) {
                                    $event_date = date('Y-m-d', strtotime('next friday', strtotime($event_date)));
                                    $guest_count = $this->Custom->query("SELECT SUM(no_of_guests) as total_joined FROM event_bookings WHERE event_id = " . $Events[0]->id . " AND booking_date = '$event_date'");
                                    $check_cancel = $this->Custom->get_where('cancelled_event', array('event_id' => $event_id, 'eventDate' => $event_date));
                                    $booking_data = array(
                                        'event_date' => $event_date,
                                        'JoinGuestCount' => (isset($guest_count[0]->total_joined) && !empty($guest_count[0]->total_joined)) ? $guest_count[0]->total_joined : 0,
                                        'isCancelled' => (isset($check_cancel) && !empty($check_cancel)) ? 1 : 0
                                    );
                                    $event_next_date[] = $booking_data;
                                }
                                break;
                            case 6:
                                while (strtotime($eventThreeMonthDate) >= strtotime($event_date)) {
                                    $event_date = date('Y-m-d', strtotime('next saturday', strtotime($event_date)));
                                    $guest_count = $this->Custom->query("SELECT SUM(no_of_guests) as total_joined FROM event_bookings WHERE event_id = " . $Events[0]->id . " AND booking_date = '$event_date'");
                                    $check_cancel = $this->Custom->get_where('cancelled_event', array('event_id' => $event_id, 'eventDate' => $event_date));
                                    $booking_data = array(
                                        'event_date' => $event_date,
                                        'JoinGuestCount' => (isset($guest_count[0]->total_joined) && !empty($guest_count[0]->total_joined)) ? $guest_count[0]->total_joined : 0,
                                        'isCancelled' => (isset($check_cancel) && !empty($check_cancel)) ? 1 : 0
                                    );
                                    $event_next_date[] = $booking_data;
                                }
                                break;
                            case 7:
                                while (strtotime($eventThreeMonthDate) >= strtotime($event_date)) {
                                    $event_date = date('Y-m-d', strtotime('next sunday', strtotime($event_date)));
                                    $guest_count = $this->Custom->query("SELECT SUM(no_of_guests) as total_joined FROM event_bookings WHERE event_id = " . $Events[0]->id . " AND booking_date = '$event_date'");
                                    $check_cancel = $this->Custom->get_where('cancelled_event', array('event_id' => $event_id, 'eventDate' => $event_date));
                                    $booking_data = array(
                                        'event_date' => $event_date,
                                        'JoinGuestCount' => (isset($guest_count[0]->total_joined) && !empty($guest_count[0]->total_joined)) ? $guest_count[0]->total_joined : 0,
                                        'isCancelled' => (isset($check_cancel) && !empty($check_cancel)) ? 1 : 0
                                    );
                                    $event_next_date[] = $booking_data;
                                }
                                break;
                        endswitch;
                    }
                    if ($recurring_event == 2) {
                        $guest_count = $this->Custom->query("SELECT SUM(no_of_guests) as total_joined FROM event_bookings WHERE event_id = " . $Events[0]->id . " AND booking_date = '$event_date'");
                        $check_cancel = $this->Custom->get_where('cancelled_event', array('event_id' => $event_id, 'eventDate' => $event_date));
                        $booking_data = array(
                            'event_date' => $event_date,
                            'JoinGuestCount' => (isset($guest_count[0]->total_joined) && !empty($guest_count[0]->total_joined)) ? $guest_count[0]->total_joined : 0,
                            'isCancelled' => (isset($check_cancel) && !empty($check_cancel)) ? 1 : 0
                        );
                        $event_next_date[] = $booking_data;
                        while (strtotime($eventThreeMonthDate) >= strtotime($event_date)) {
                            $event_date = date('Y-m-d', strtotime('+1 day', strtotime($event_date)));
                            $guest_count = $this->Custom->query("SELECT SUM(no_of_guests) as total_joined FROM event_bookings WHERE event_id = " . $Events[0]->id . " AND booking_date = '$event_date'");
                            $check_cancel = $this->Custom->get_where('cancelled_event', array('event_id' => $event_id, 'eventDate' => $event_date));
                            $booking_data = array(
                                'event_date' => $event_date,
                                'JoinGuestCount' => (isset($guest_count[0]->total_joined) && !empty($guest_count[0]->total_joined)) ? $guest_count[0]->total_joined : 0,
                                'isCancelled' => (isset($check_cancel) && !empty($check_cancel)) ? 1 : 0
                            );
                            $event_next_date[] = $booking_data;
                        }
                    }
                }
                if (isset($event_next_date) && !empty($event_next_date)) {
                    $this->response->success = 200;
                    $this->response->message = $this->lang->line('events_details');
                    $this->response->data = $event_next_date;
                    die(json_encode($this->response));
                } else {
                    $this->response->success = 205;
                    $this->response->message = "No data found";
                    die(json_encode($this->response));
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function CancelEvent() {
        $event_id = ($this->input->post('event_id')) ? $this->input->post('event_id') : '';
        $event_date = ($this->input->post('event_date')) ? $this->input->post('event_date') : '';
        $user_id = ($this->input->post('user_id')) ? $this->input->post('user_id') : '';

        if (isset($event_id) && !empty($event_id) && isset($user_id) && !empty($user_id) && isset($event_date) && !empty($event_date)) {
            $user_record = $this->Custom->get_where('users', array('id' => $user_id));
            if (isset($user_record) && !empty($user_record)) {
                //check authentication
                $headers = apache_request_headers();
                $auth_record = CheckAuthentication($headers['Authenticationtoken'], $user_id);
                if (empty($auth_record)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('authentication_failed');
                    die(json_encode($this->response));
                }

                $event_record = $this->Custom->get_where('event_bookings', array('event_id' => $event_id, 'booking_date' => $event_date, 'payment_status' => 'paid', 'status' => 1));
                if (isset($event_record) && !empty($event_record)) {
                    $cancel_record = $this->Custom->get_where('cancelled_event', array('event_id' => $event_id, 'eventDate' => $event_date));
                    if (!isset($cancel_record) || empty($cancel_record)) {
                        $this->Custom->insert_data('cancelled_event', array('event_id' => $event_id, 'eventDate' => $event_date));
                    }
                    $event = $this->Custom->get_where('event', array('id' => $event_id));
                    if (!isset($event) || empty($event)) {
                        $this->response->success = 202;
                        $this->response->message = $this->lang->line('something_went_wrong');
                        die(json_encode($this->response));
                    }
                    $current_date = date("Y-m-d H:i:s");
                    $start_time = $event[0]->start_time;
                    $event_date_time = $event_date . ' ' . $start_time . ':00';
                    $compare_time = date("Y-m-d H:i:s", strtotime($event_date_time, strtotime("-24 hours")));
                    if (strtotime($compare_time) < strtotime($current_date)) {
                        $this->response->success = 202;
                        $this->response->message = "You can cancel an event when 24 hours or more left to start.";
                        die(json_encode($this->response));
                    }
                    if ($this->Custom->update_where("event_bookings", array('status' => 4), array('event_id' => $event_id, 'booking_date' => $event_date))) {
                        foreach ($event_record as $key => $value) {
                            /* if($value->payment_mode == 'card') {
                              $charge_id = (isset($value->transaction_token) && !empty($value->transaction_token)) ? $value->transaction_token : '';
                              if(!empty($charge_id)) {
                              $re = \Stripe\Refund::create(array(
                              "charge" => $charge_id
                              ));
                              if(isset($re) && !empty($re) && $re->status == 'succeeded') {
                              $this->Custom->update_where("event_bookings", array('refund_id' => $re->id), array('id' => $value->id));
                              }
                              }
                              } else {
                              // crypto refund functionality here
                              } */
                            $user_data = $this->Custom->get_where('users', array('id' => $value->user_id));
                            $user_profile = $this->Custom->get_where('user_profile', array('user_id' => $value->user_id));
                            if (isset($user_data) && !empty($user_data) && isset($user_profile) && !empty($user_profile)) {
                                $value->first_name = (isset($user_profile[0]->first_name) && !empty($user_profile[0]->first_name)) ? $user_profile[0]->first_name : '';
                                $value->last_name = (isset($user_profile[0]->last_name) && !empty($user_profile[0]->last_name)) ? $user_profile[0]->last_name : '';
                                $value->email = (isset($user_data[0]->email) && !empty($user_data[0]->email)) ? $user_data[0]->email : '';

                                $hostUser = $this->Custom->get_where('user_profile', array('user_id' => $user_id));
                                $first_name = (isset($hostUser[0]->first_name) && !empty($hostUser[0]->first_name)) ? $hostUser[0]->first_name : '';
                                $last_name = (isset($hostUser[0]->last_name) && !empty($hostUser[0]->last_name)) ? $hostUser[0]->last_name : '';
                                $device_type = (isset($user_data[0]->device_type) && !empty($user_data[0]->device_type)) ? $user_data[0]->device_type : '';
                                $device_token = (isset($user_data[0]->device_token) && !empty($user_data[0]->device_token)) ? $user_data[0]->device_token : '';
                                $message = "We're sorry for the inconvenience, but your host '$first_name $last_name' has cancelled event '" . $event_record[0]->title . "' on $event_date. We will be in touch within 48 hours to discuss refunds.";
                                if (!empty($device_type) && !empty($device_token)) {
                                    $this->send_notification($device_type, $device_token, $message, $event_record->id, 'EventCancelled');
                                }
                            }
                        }
                        $hostUser = $this->Custom->get_where('user_profile', array('user_id' => $user_id));
                        $data = array();
                        $email = 'eat@veats.app';
                        //$email = 'sfs.manisharora@gmail.com';
                        $data['host'] = $hostUser[0];
                        $data['users'] = $event_record;
                        $data['event_date'] = $event_date;
                        $data['event'] = $event[0];
                        $content = $this->load->view('mail/cancel_event', $data, TRUE);
                        $subject = 'Event cancelled by host - Veats App';
                        $this->SendMail($email, $subject, $content);

                        $this->response->success = 200;
                        $this->response->message = "Event Cancelled";
                        die(json_encode($this->response));
                    } else {
                        $this->response->success = 202;
                        $this->response->message = $this->lang->line('something_went_wrong');
                        die(json_encode($this->response));
                    }
                } else {
                    $cancel_record = $this->Custom->get_where('cancelled_event', array('event_id' => $event_id, 'eventDate' => $event_date));
                    if (!isset($cancel_record) || empty($cancel_record)) {
                        $this->Custom->insert_data('cancelled_event', array('event_id' => $event_id, 'eventDate' => $event_date));
                    }
                    $hostUser = $this->Custom->get_where('user_profile', array('user_id' => $user_id));
                    $data = array();
                    $email = 'eat@veats.app';
                    $event = $this->Custom->get_where('event', array('id' => $event_id));
                    //$email = 'sfs.manisharora@gmail.com';
                    $data['host'] = $hostUser[0];
                    $data['users'] = array();
                    $data['event_date'] = $event_date;
                    $data['event'] = $event[0];
                    $content = $this->load->view('mail/cancel_event', $data, TRUE);
                    $subject = 'Event cancelled by host - Veats App';
                    $this->SendMail($email, $subject, $content);

                    $this->response->success = 200;
                    $this->response->message = "Event Cancelled";
                    die(json_encode($this->response));
                }
            } else {
                $this->response->success = 203;
                $this->response->message = $this->lang->line('invalid_user_id');
                die(json_encode($this->response));
            }
        } else {
            $this->response->success = 201;
            $this->response->message = $this->lang->line('required_field_error');
            die(json_encode($this->response));
        }
    }

    public function send_notification($device_type, $device_token, $message, $booking_id, $type = 'Notification') {
        define("GOOGLE_API_KEY", "AIzaSyAKZoiv4tHDWFDUjBqxDnZwpLgp2e5nvvo");
        if (isset($message) && !empty($message) && isset($booking_id) && isset($booking_id) && isset($device_type) && isset($device_type) && isset($device_token) && isset($device_token)) {
            if ($device_type == 'android') {
                $registatoin_ids = array($device_token);
                $message2 = array("message" => $message, "type" => $type, "booking_id" => $booking_id);

                $url = 'https://fcm.googleapis.com/fcm/send';
                $fields = array(
                    'registration_ids' => $registatoin_ids,
                    'data' => $message2,
                );
                $headers = array(
                    'Authorization: key=' . GOOGLE_API_KEY,
                    'Content-Type: application/json'
                );
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                $result = curl_exec($ch);
            } else if ($device_type == 'ios') {
                //$app_state = 'prod';
                /* $app_state = '';
                  $body['aps'] = array(
                  'alert' => $message,
                  'type' => "Notification",
                  'booking_id' => $booking_id
                  );

                  $passphrase = '123456789';

                  $ctx = stream_context_create();

                  if (!empty($app_state) && $app_state == 'prod' && isset($app_state)) {
                  stream_context_set_option($ctx, 'ssl', 'local_cert', '/var/www/html/application/controllers/pushcert.pem');
                  $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
                  } else {
                  stream_context_set_option($ctx, 'ssl', 'local_cert', '/var/www/html/application/controllers/ck.pem');
                  $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
                  }
                  stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

                  $payload = json_encode($body);
                  // $msg = chr(0) . pack('n', 32) . pack('H*', str_replace(' ', '', sprintf('%u', CRC32($device_token)))) . pack('n', strlen($payload)) . $payload;
                  $msg = chr(0) . pack('n', 32) . pack('H*', $device_token) . pack('n', strlen($payload)) . $payload;
                  $result = fwrite($fp, $msg, strlen($msg));

                  fclose($fp); */
            }
        }
    }

    public function TestCallback() {
       $data = json_decode(file_get_contents('php://input'), true);
       $response = ($data) ? json_encode($data) : "";
       $this->Custom->insert_data("testcallback", array('response' => $response));
//       $retValue = (object) array('success' => 1, 'Message' => "Callback Successful.");
//       http_response_code(200);
//       echo json_encode($retValue);
        require_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';

//        $token = "c43b6185bd3ad0ac7478d3c562833baa";
//        $sid = "AC4d2b4610ae0d821b36a3870bbba5ef7c";
//        $twilio = new Client($sid, $token);
//            $messages = $twilio->messages
//                               ->read(array(), 20);
////            print_r($messages);
//            foreach ($messages as $record) {
//                print($record);
//                //print($record->sid);
//                echo "<br>";
//            }

        $t = new Twilio\Twiml();
        header("content-type: text/xml");

        $response = $t->MessagingResponse();
        $response->message(
                "I'm using the Twilio PHP library to respond to this SMS!"
        );

        echo $response;
    }

    /*
    * Upload Properties video
    */

    public function properties_video_upload(){
        
        $propertyID = $this->input->post('property_id');
        $user_id = $this->input->post('user_id');
        $video_link ='';
        $Thumburl ='';
        if (empty($propertyID)) {
                    $this->response->success = 404;
                    $this->response->message = $this->lang->line('required_field_error');
                    die(json_encode($this->response));
        }
        // thumb Check
          if (empty($_FILES['video_thumb']['name'])) {
                    $this->response->success = 201;
                    $this->response->message = $this->lang->line('required_fieldthumb_error');
                    die(json_encode($this->response));
                }
        // video check 
            if (empty($_FILES['property_video']['name'])) {
                    $this->response->success = 201;
                    $this->response->message = $this->lang->line('required_fieldvideo_error');
                    die(json_encode($this->response));
                }

                // video upload bucket
                  $Video_name = rand(1, 1000).'_'.time() . '_' . $_FILES["property_video"]['name'];
                  $Thumb_name = rand(1, 1000).'_'.time() . '_' . $_FILES["video_thumb"]['name'];
                   $path1 = $_FILES["property_video"]['tmp_name'];
                   $ContentType1 = $_FILES["property_video"]['type'];
                   $path2 = $_FILES["video_thumb"]['tmp_name'];
                   $ContentType2 = $_FILES["video_thumb"]['type'];

                  $bucket = 'realestatepropertyvideo';

                    $result = $this->s3->putObject(
                                array(
                                'Bucket' => $bucket,
                                'Key' => $Video_name,
                                'SourceFile' => $path1,
                                'ACL' => 'public-read'
                               // 'ContentType' => $ContentType1
                                )
                    );
                   
                    if (isset($result['ObjectURL']) && !empty($result['ObjectURL'])) {

                        $video_link = $result['ObjectURL'];

                         $Videoresult = $this->s3->putObject(
                             array(
                                'Bucket' => $bucket,
                                'Key' => $Thumb_name,
                                'SourceFile' => $path2,
                                'ACL' => 'public-read'
                                //'ContentType' => $ContentType2  
                                )
                        );

                        if (isset($Videoresult['ObjectURL']) && !empty($Videoresult['ObjectURL'])) {

                            $Thumburl =  $Videoresult['ObjectURL'];

                          }else {

                             $this->response->success = 203;
                            $this->response->message = $this->lang->line('image_not_upload');
                            die(json_encode($this->response));
                          }
                    } else {
                        $this->response->success = 203;
                        $this->response->message = $this->lang->line('video_not_upload');
                        die(json_encode($this->response));
                    }

            $insert_arr = array(
                'propertyID' => $propertyID,
                'video_thumb' => $Thumburl,
                'video_link' => $video_link,
                'user_id'    => $user_id,     
            );
           /* echo"<pre>"; print_r($insert_arr);die();*/
            $insert_id = $this->Custom->insert_data('property_video', $insert_arr);

            if($insert_id){
                $this->response->success = 200;
                $this->response->message = $this->lang->line('property_video_upload');
                /*$this->response->data = $user_data[0];*/
                die(json_encode($this->response));            
            }

    }

    /*
    * Video List 
    */

    public function properties_video_list(){

        $propertyID = $this->input->post('property_id');
        $page = $this->input->post('page');
        $per_page = 10;
        $offset = ($page == 1) ? 0 : ($page - 1) * $per_page;
        $new_per_page = $per_page + 1;

         if (empty($propertyID)) {
                $this->response->success = 404;
                $this->response->message = $this->lang->line('required_field_error');
                die(json_encode($this->response));
            }

          if (empty($per_page)) {
                $this->response->success = 404;
                $this->response->message = $this->lang->line('required_field_error');
                die(json_encode($this->response));
            }  

           $data = $this->Custom->query("SELECT * from property_video 
           	where propertyID = '" . $propertyID . "' 
           ORDER BY created_at DESC LIMIT $offset,$new_per_page");

            $this->response->success = 200;
            $this->response->message = "Properties Video list";
            $this->response->properties_video = $data;
            $this->response->countvideo= (!empty($data)) ? count(  $data ) : 0;
            die(json_encode($this->response));
    }

}
