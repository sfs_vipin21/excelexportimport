<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AdminController extends CI_Controller {

    public $language;

    public function __construct() {
        parent::__construct();
        ini_set("display_errors", 0);
        error_reporting(0);
        date_default_timezone_set("UTC");
    }

    public function index() {
        $data['title'] = "Login";
        if (isset($_POST['login_sub'])) {
            $adminData = $this->Custom->get_where('admin_details', array('email' => $_POST['email'], 'password' => md5($_POST['pwd'])));
            if ($adminData) {
                $this->session->set_userdata('admin_data', array('id' => $adminData[0]->id));
                redirect(base_url() . 'dashboard');
            } else {
                $this->session->set_flashdata('error_msg', 'Email/Password is not correct, please try again!');
                redirect(base_url() . 'admin');
            }
        } else {
            if ($this->session->userdata('admin_data')) {
                redirect(base_url() . 'dashboard');
            } else {
                $this->load->view('admin/login', $data);
            }
        }
    }

    public function Dashboard() {
        CheckAdminLogin();
        $AdminData = $this->session->userdata('admin_data');
        $data['AdminDetails'] = $this->Custom->get_where('admin_details', array('id' => $AdminData['id']));
        $data['title'] = "Dashboard";
        $this->load->view('admin/header', $data);
        $this->load->view('admin/leftpanel', $data);
        $this->load->view('admin/dashboard', $data);
        $this->load->view('admin/footer', $data);
    }

    public function Logout() {
        $this->session->sess_destroy();
        redirect(base_url() . 'admin');
    }

    /*     * ********** User Management *********** */

    public function UserList() {
        CheckAdminLogin();
        $AdminData = $this->session->userdata('admin_data');
        $data['AdminDetails'] = $this->Custom->get_where('admin_details', array('id' => $AdminData['id']));
        $data['usersData'] = $this->Custom->query('SELECT * FROM users');
        $data['title'] = "Users";
        $this->load->view('admin/header', $data);
        $this->load->view('admin/leftpanel', $data);
        $this->load->view('admin/users/list', $data);
        $this->load->view('admin/footer', $data);
    }

    /*     * ********** Brand Management *********** */

    public function Brands() {
        CheckAdminLogin();
        $AdminData = $this->session->userdata('admin_data');
        $data['AdminDetails'] = $this->Custom->get_where('admin_details', array('id' => $AdminData['id']));
        $data['brandsData'] = $this->Custom->get_where('brands', array('delete_status' => 0));
        $data['title'] = "Brands";
        $this->load->view('admin/header', $data);
        $this->load->view('admin/leftpanel', $data);
        $this->load->view('admin/brand/list', $data);
        $this->load->view('admin/footer', $data);
    }

    public function AddBrand() {
        CheckAdminLogin();
        $AdminData = $this->session->userdata('admin_data');
        $data['AdminDetails'] = $this->Custom->get_where('admin_details', array('id' => $AdminData['id']));
        $data['title'] = "Add Brand";
        if (isset($_POST['submit_btn']) && !empty($_POST['submit_btn'])) {
            $this->form_validation->set_rules('brand', 'Brand', 'trim|required|is_unique[brands.brand]', array('is_unique' => 'Brand Already Exists.'));
            if ($this->form_validation->run() == false) {
                $this->load->view('admin/header', $data);
                $this->load->view('admin/leftpanel', $data);
                $this->load->view('admin/brand/add', $data);
                $this->load->view('admin/footer', $data);
            } else {
                $insertArr = array(
                    'brand' => $_POST['brand'],
                    'status' => 1,
                    'created' => date('Y-m-d H:i:s')
                );
                $insert_id = $this->Custom->insert_data('brands', $insertArr);
                if ($insert_id) {
                    $this->session->set_flashdata('success_msg', "Brand added successfully");
                    redirect(base_url() . 'brands');
                } else {
                    $this->session->set_flashdata('error_msg', "Something went wrong");
                    redirect(base_url() . 'addbrand');
                }
            }
        } else {
            $this->load->view('admin/header', $data);
            $this->load->view('admin/leftpanel', $data);
            $this->load->view('admin/brand/add', $data);
            $this->load->view('admin/footer', $data);
        }
    }

    public function DeleteBrand() {
        CheckAdminLogin();
        $brand_id = $this->uri->segment(2);
        $brandsData = $this->Custom->get_where('brands', array('id' => $brand_id));
        if ($brandsData) {
            $update_status = $this->Custom->update_where('brands', array('delete_status' => 1), array('id' => $brand_id));
            if ($update_status) {
                $this->session->set_flashdata('success_msg', "Brand deleted successfully");
                redirect(base_url() . 'brands');
            } else {
                $this->session->set_flashdata('error_msg', "Something went wrong");
                redirect(base_url() . 'brands');
            }
        } else {
            $this->session->set_flashdata('error_msg', "Brand is not valid");
            redirect(base_url() . 'addbrand');
        }
    }

    public function import_properties() {
        $page = 1;
        $data = file_get_contents("http://dataapi.rs.realtymx.com/listings?apiKey=5569317051725531&limit=100&page=$page&sort=date&order=asc&status=2,21&includeContacts=1&addLabels=1");
        $data = json_decode($data);
        $total = $data->TOTAL_COUNT;
        $num_pages = round($total / 40);
        //echo '<pre>'; print_r($data); die('>>>>>>>>>>>>');
        $this->db->query('Update properties SET is_deleted = 1 WHERE DATEDIFF(CURDATE(),DATE_LISTED) > 30');
        for ($i = 1; $i <= $num_pages; $i++) {
            $newData = file_get_contents("http://dataapi.rs.realtymx.com/listings?apiKey=5569317051725531&limit=40&page=$i&sort=date&order=asc&status=2,21&includeContacts=1&addLabels=1");
            $newData = json_decode($newData);
            if (isset($newData->LISTINGS) && !empty($newData->LISTINGS)) {
                foreach ($newData->LISTINGS as $key => $value) {
                    $id = $value->ID;
                    $BUILDING_ID = $value->BUILDING_ID;
                    unset($value->ID);
                    $building_data = file_get_contents("http://dataapi.rs.realtymx.com/buildings?apiKey=5569317051725531&id=$BUILDING_ID");
                    $building_data = json_decode($building_data);
                    $value->DEALS_NOTE = $building_data->BUILDINGS[0]->DEALS_NOTE;
                     $value->BUILDING_ACCESS_NOTE = $building_data->BUILDINGS[0]->ACCESS_NOTE;
                     $value->BUILDING_AGENTS_NOTE = $building_data->BUILDINGS[0]->AGENTS_NOTE;
                    $checkData = $this->Custom->get_where('properties', array('propertyID' => $id,'is_deleted' => 0));
                    if (isset($checkData) && !empty($checkData)) {
                        $photos = $value->PHOTOS;
                        $contacts = $value->CONTACTS;
                        $labels = $value->LABELS;
                        $amenities = ($value->AMENITIES) ? explode(",", $value->AMENITIES) : '';
                        unset($value->PHOTOS);
                        unset($value->CONTACTS);
                        unset($value->AMENITIES);
                        unset($value->LABELS);
                        $this->Custom->update_where('properties', $value, array('propertyID' => $id));
                        if (isset($photos) && !empty($photos)) {
                            $this->Custom->delete_where('property_images', array('propertyID' => $id));
                            foreach ($photos as $k => $val) {
                                $val->propertyID = $id;
                                $val->created = date("Y-m-d H:i:s");
                                $this->Custom->insert_data('property_images', $val);
                            }
                        }
                        if (isset($contacts) && !empty($contacts)) {
                            $this->Custom->delete_where('property_contacts', array('propertyID' => $id));
                            foreach ($contacts as $k => $val) {
                                $val->propertyID = $id;
                                $val->created = date("Y-m-d H:i:s");
                                $this->Custom->insert_data('property_contacts', $val);
                            }
                        }

                        if (isset($amenities) && !empty($amenities)) {
                            $this->Custom->delete_where('property_amenities', array('propertyID' => $id));
                            foreach ($amenities as $k => $val) {
                                $amenituArr = array();
                                $amenituArr = array(
                                    'propertyID' => $id,
                                    'amenity' => $val,
                                    'created' => date("Y-m-d H:i:s"),
                                    'status' => 1
                                );
                                $this->Custom->insert_data('property_amenities', $amenituArr);
                            }
                        }
                        if (isset($labels) && !empty($labels)) {
                            $this->Custom->delete_where('property_label', array('propertyID' => $id));
                            foreach ($labels as $k => $val) {
                                $labelsArr = array();
                                $labelsArr = array(
                                    'propertyID' => $id,
                                    'name' => $val->NAME,
                                    'label_id' => $val->ID,
                                    'created' => date("Y-m-d H:i:s")
                                );
                                $this->Custom->insert_data('property_label', $labelsArr);
                            }
                        }
                    } else {
                        $photos = $value->PHOTOS;
                        $contacts = $value->CONTACTS;
                        $amenities = ($value->AMENITIES) ? explode(",", $value->AMENITIES) : '';
                        $labels = $value->LABELS;
                        unset($value->PHOTOS);
                        unset($value->CONTACTS);
                        unset($value->AMENITIES);
                        unset($value->LABELS);
                        $value->propertyID = $id;
                        $this->Custom->insert_data('properties', $value);
                        if (isset($photos) && !empty($photos)) {
                            foreach ($photos as $k => $val) {
                                $val->propertyID = $id;
                                $val->created = date("Y-m-d H:i:s");
                                $this->Custom->insert_data('property_images', $val);
                            }
                        }
                        if (isset($contacts) && !empty($contacts)) {
                            $this->Custom->delete_where('property_contacts', array('propertyID' => $id));
                            foreach ($contacts as $k => $val) {
                                $val->propertyID = $id;
                                $val->created = date("Y-m-d H:i:s");
                                $this->Custom->insert_data('property_contacts', $val);
                            }
                        }
                        foreach ($amenities as $k => $val) {
                            $amenituArr = array();
                            $amenituArr = array(
                                'propertyID' => $id,
                                'amenity' => $val,
                                'created' => date("Y-m-d H:i:s"),
                                'status' => 1
                            );
                            $this->Custom->insert_data('property_amenities', $amenituArr);
                        }
                        if (isset($labels) && !empty($labels)) {
                            $this->Custom->delete_where('property_label', array('propertyID' => $id));
                            foreach ($labels as $k => $val) {
                                $labelsArr = array(
                                    'propertyID' => $id,
                                    'name' => $val->NAME,
                                    'label_id' => $val->ID,
                                    'created' => date("Y-m-d H:i:s")
                                );
                                $this->Custom->insert_data('property_label', $labelsArr);
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    /*bckup work*/
    
    public function import_properties_new() {
        
        $page = 1;
        $data = file_get_contents("http://dataapi.rs.realtymx.com/listings?apiKey=5569317051725531&limit=100&page=$page&sort=date&order=asc&status=2,21&includeContacts=1&addLabels=1");
        $data = json_decode($data);
        $total = $data->TOTAL_COUNT;
        $num_pages = round($total / 40);
        //echo '<pre>'; print_r($data); die('>>>>>>>>>>>>');
          $runquery = $this->db->query('Update properties SET is_deleted = 1 WHERE DATEDIFF(CURDATE(),DATE_LISTED) > 30');

        for ($i = 1; $i <= $num_pages; $i++) {
            $newData = file_get_contents("http://dataapi.rs.realtymx.com/listings?apiKey=5569317051725531&limit=40&page=$i&sort=date&order=asc&status=2,21&includeContacts=1&addLabels=1");
            $newData = json_decode($newData);
            if (isset($newData->LISTINGS) && !empty($newData->LISTINGS)) {
                foreach ($newData->LISTINGS as $key => $value) {
                    $id = $value->ID;
                    $BUILDING_ID = $value->BUILDING_ID;
                    unset($value->ID);
                    $building_data = file_get_contents("http://dataapi.rs.realtymx.com/buildings?apiKey=5569317051725531&id=$BUILDING_ID");
                    $building_data = json_decode($building_data);
                    $value->DEALS_NOTE = $building_data->BUILDINGS[0]->DEALS_NOTE;
                     $value->BUILDING_ACCESS_NOTE = $building_data->BUILDINGS[0]->ACCESS_NOTE;
                     $value->BUILDING_AGENTS_NOTE = $building_data->BUILDINGS[0]->AGENTS_NOTE;
                    $checkData = $this->Custom->get_where('properties', array('propertyID' => $id,'is_deleted' => 0));
                    if (isset($checkData) && !empty($checkData)) {
                        $photos = $value->PHOTOS;
                        $contacts = $value->CONTACTS;
                        $labels = $value->LABELS;
                        $amenities = ($value->AMENITIES) ? explode(",", $value->AMENITIES) : '';
                        unset($value->PHOTOS);
                        unset($value->CONTACTS);
                        unset($value->AMENITIES);
                        unset($value->LABELS);
                        $this->Custom->update_where('properties', $value, array('propertyID' => $id));
                        if (isset($photos) && !empty($photos)) {
                            $this->Custom->delete_where('property_images', array('propertyID' => $id));
                            foreach ($photos as $k => $val) {
                                $val->propertyID = $id;
                                $val->created = date("Y-m-d H:i:s");
                                $this->Custom->insert_data('property_images', $val);
                            }
                        }
                        if (isset($contacts) && !empty($contacts)) {
                            $this->Custom->delete_where('property_contacts', array('propertyID' => $id));
                            foreach ($contacts as $k => $val) {
                                $val->propertyID = $id;
                                $val->created = date("Y-m-d H:i:s");
                                $this->Custom->insert_data('property_contacts', $val);
                            }
                        }

                        if (isset($amenities) && !empty($amenities)) {
                            $this->Custom->delete_where('property_amenities', array('propertyID' => $id));
                            foreach ($amenities as $k => $val) {
                                $amenituArr = array();
                                $amenituArr = array(
                                    'propertyID' => $id,
                                    'amenity' => $val,
                                    'created' => date("Y-m-d H:i:s"),
                                    'status' => 1
                                );
                                $this->Custom->insert_data('property_amenities', $amenituArr);
                            }
                        }
                        if (isset($labels) && !empty($labels)) {
                            $this->Custom->delete_where('property_label', array('propertyID' => $id));
                            foreach ($labels as $k => $val) {
                                $labelsArr = array();
                                $labelsArr = array(
                                    'propertyID' => $id,
                                    'name' => $val->NAME,
                                    'label_id' => $val->ID,
                                    'created' => date("Y-m-d H:i:s")
                                );
                                $this->Custom->insert_data('property_label', $labelsArr);
                            }
                        }
                    } else {
                        $photos = $value->PHOTOS;
                        $contacts = $value->CONTACTS;
                        $amenities = ($value->AMENITIES) ? explode(",", $value->AMENITIES) : '';
                        $labels = $value->LABELS;
                        unset($value->PHOTOS);
                        unset($value->CONTACTS);
                        unset($value->AMENITIES);
                        unset($value->LABELS);
                        $value->propertyID = $id;
                        $this->Custom->insert_data('properties', $value);
                        if (isset($photos) && !empty($photos)) {
                            foreach ($photos as $k => $val) {
                                $val->propertyID = $id;
                                $val->created = date("Y-m-d H:i:s");
                                $this->Custom->insert_data('property_images', $val);
                            }
                        }
                        if (isset($contacts) && !empty($contacts)) {
                            $this->Custom->delete_where('property_contacts', array('propertyID' => $id));
                            foreach ($contacts as $k => $val) {
                                $val->propertyID = $id;
                                $val->created = date("Y-m-d H:i:s");
                                $this->Custom->insert_data('property_contacts', $val);
                            }
                        }
                        foreach ($amenities as $k => $val) {
                            $amenituArr = array();
                            $amenituArr = array(
                                'propertyID' => $id,
                                'amenity' => $val,
                                'created' => date("Y-m-d H:i:s"),
                                'status' => 1
                            );
                            $this->Custom->insert_data('property_amenities', $amenituArr);
                        }
                        if (isset($labels) && !empty($labels)) {
                            $this->Custom->delete_where('property_label', array('propertyID' => $id));
                            foreach ($labels as $k => $val) {
                                $labelsArr = array(
                                    'propertyID' => $id,
                                    'name' => $val->NAME,
                                    'label_id' => $val->ID,
                                    'created' => date("Y-m-d H:i:s")
                                );
                                $this->Custom->insert_data('property_label', $labelsArr);
                            }
                        }
                    }
                }
            }
        }
    }
    

    public function amenities() {
        $Data = $this->Custom->get_data('properties');
        if (isset($Data) && !empty($Data)) {
            foreach ($Data as $key => $value) {
                $amenities = ($value->AMENITIES) ? explode(",", $value->AMENITIES) : '';
                foreach ($amenities as $k => $val) {
                    $amenituArr = array();
                    $amenituArr = array(
                        'propertyID' => $value->propertyID,
                        'amenity' => $val,
                        'created' => date("Y-m-d H:i:s"),
                        'status' => 1
                    );
                    $this->Custom->insert_data('property_amenities', $amenituArr);
                }
            }
        }
    }
    
    

}
