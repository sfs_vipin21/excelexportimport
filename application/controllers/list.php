<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Pharmacy</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . 'Dashboard'; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Pharmacy List</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row" ng-if="initialView">
            <div class="col-xs-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title" style="float:left">Pharmacy List</h3>
                        <button type="button" class="btn btn-success pull-right" ng-click="addNewPharmacy()">Add Pharmacy</button>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                       
                        <table class="table table-bordered table-striped datatable" datatable="ng">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Phone no</th>
                                    <th>Type</th>
                                  
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="phar in pharmacy">
                                    <td>{{$index+1}}</td>
                                    <td>{{ phar.pharmacy_name }}</td>
                                    <td>{{ phar.address }}</td>
                                    <td>{{ phar.phone_number }}</td>
                                    <td>{{ phar.type }}</td>
                                   
                                    <td>
                                        <a href="javascript:void(0)" title="Edit" class="btn btn-primary btn-xs" ng-click="editPharmacy(phar.id)">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a href="javascript:void(0)" title="delete" class="btn btn-danger btn-xs" ng-click="deletePharmacy(phar.id)">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                        <a href="javascript:void(0)" title="Timing" class="btn btn-success btn-xs" ng-click="pharmacyOpeningHours(phar.id)">
                                            <i class="fa fa-calendar"></i>
                                        </a>
                                        <a href="javascript:void(0)" title="Lunch Time" class="btn btn-default btn-xs" ng-click="pharmacyLunchHours(phar.id)">
                                            <i class="fa fa-calendar"></i>
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<div id="addNewPharmacy" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">{{!editmode?"Add Pharmacy":"Edit pharmacy"}}</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="#" enctype="multipart/form-data" id="add_pharmacy_form">
                    <div class="form-group">
                        <input type="text" class="form-control" required name="name" id="usr" placeholder="Pharmacy name" ng-model="name">
                    </div>
                    <div class="form-group">
                        <select class="form-control" id="type" name="type" ng-model="type"> 
                            <option value="" selected="selected">Select pharmacy type</option>
                            <option value="LioydsApotek">LioydsApotek</option>
                            <option value="KronansApotek">KronansApotek</option>
                            <option value="Apotek Hjärtat">Apotek Hjärtat</option>
                            <option value="Apoteksgruppen">Apoteksgruppen</option>
                            <option value="Apoteket">Apoteket</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <textarea name="address" id="address" class="form-control" placeholder="Enter address of pharmacy" ng-model="address"></textarea>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" required name="contact_number" id="contact_number" placeholder="Pharmacy contact no" ng-model="contact_number">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" required name="latitude" id="latitude" placeholder="Pharmacy latitude" ng-model="latitude">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" required name="longitude" id="longitude" placeholder="Pharmacy longitude" ng-model="longitude">
                    </div>
                    <div class="form-group">
                        <textarea name="description" id="description" class="form-control" placeholder="Enter description of pharmacy" ng-model="description"></textarea>
                    </div>
                    
                    <button type="submit" value="submit" class="btn btn-success">Submit</button>
                </form>
            </div>

        </div>

    </div>
</div>

<div id="openingHours" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <?php 
            $counter_startime = strtotime('00:00');
            $counter_endtime = strtotime('23:30');
        ?>
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Opening hours</h4>
            </div>
            <div class="modal-body">
                <form method="post" enctype="multipart/form-data" id="add_pharmacy_opening_form">
                    <div class="form-group">
                        <div class="form_intervals col-sm-6 col-xs-6">
                            <label style="text-align: center;">Monday</label>
                            <input type="hidden" name="day" value="Monday">
                            <select class="form-control schedule_starttime" name="monday_starttime" id = "monday_starttime" required="required">
                                <option value="closed" selected>Closed</option>
                                <?php
                                for ($i = $counter_startime; $i <= $counter_endtime; $i = $i + 15 * 60) {
                                    ?>
                                    <option value="<?php echo date('H:i', $i); ?>"><?php echo date('H:i', $i); ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form_intervals col-sm-6 col-xs-6">
                            <label style="text-align: center;">&nbsp;</label>
                            <select class="form-control schedule_endtime" name="monday_endtime" required="required" id="monday_endtime">
                                <option value="closed" selected ng-selected="$('#monday_starttime').val() == 'closed'">Closed</option>
                                <?php
                                for ($i = $counter_startime; $i <= $counter_endtime; $i = $i + 15 * 60) {
                                    ?>
                                    <option value="<?php echo date('H:i', $i); ?>"><?php echo date('H:i', $i); ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form_intervals col-sm-6 col-xs-6">
                            <label style="text-align: center;">Tuesday</label>
                            <input type="hidden" name="day" value="Tuesday">
                            <select class="form-control schedule_starttime" name="tuesday_starttime" required="required" id="tuesday_starttime">
                                <option value="closed" selected>Closed</option>
                                <?php
                                for ($i = $counter_startime; $i <= $counter_endtime; $i = $i + 15 * 60) {
                                    ?>
                                    <option value="<?php echo date('H:i', $i); ?>"><?php echo date('H:i', $i); ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form_intervals col-sm-6 col-xs-6">
                            <label style="text-align: center;">&nbsp;</label>
                            <select class="form-control schedule_endtime" name="tuesday_endtime" required="required" id="tuesday_endtime">
                                <option value="closed" selected>Closed</option>
                                <?php
                                for ($i = $counter_startime; $i <= $counter_endtime; $i = $i + 15 * 60) {
                                    ?>
                                    <option value="<?php echo date('H:i', $i); ?>"><?php echo date('H:i', $i); ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form_intervals col-sm-6 col-xs-6">
                            <label style="text-align: center;">Wednesday</label>
                            <input type="hidden" name="day" value="Wednesday">
                            <select class="form-control schedule_starttime" name="wednesday_starttime" required="required" id="wednesday_starttime">
                                <option value="closed" selected>Closed</option>
                                <?php
                                for ($i = $counter_startime; $i <= $counter_endtime; $i = $i + 15 * 60) {
                                    ?>
                                    <option value="<?php echo date('H:i', $i); ?>"><?php echo date('H:i', $i); ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form_intervals col-sm-6 col-xs-6">
                            <label style="text-align: center;">&nbsp;</label>
                            <select class="form-control schedule_endtime" name="wednesday_endtime" required="required" id="wednesday_endtime">
                                <option value="closed" selected>Closed</option>
                                <?php
                                for ($i = $counter_startime; $i <= $counter_endtime; $i = $i + 15 * 60) {
                                    ?>
                                    <option value="<?php echo date('H:i', $i); ?>"><?php echo date('H:i', $i); ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form_intervals col-sm-6 col-xs-6">
                            <label style="text-align: center;">Thursday</label>
                            <input type="hidden" name="day" value="Thursday">
                            <select class="form-control schedule_starttime" name="thursday_starttime" required="required" id="thursday_starttime">
                                <option value="closed" selected>Closed</option>
                                <?php
                                for ($i = $counter_startime; $i <= $counter_endtime; $i = $i + 15 * 60) {
                                    ?>
                                    <option value="<?php echo date('H:i', $i); ?>"><?php echo date('H:i', $i); ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form_intervals col-sm-6 col-xs-6">
                            <label style="text-align: center;">&nbsp;</label>
                            <select class="form-control schedule_endtime" name="thursday_endtime" required="required" id="thursday_endtime">
                                <option value="closed" selected>Closed</option>
                                <?php
                                for ($i = $counter_startime; $i <= $counter_endtime; $i = $i + 15 * 60) {
                                    ?>
                                    <option value="<?php echo date('H:i', $i); ?>"><?php echo date('H:i', $i); ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form_intervals col-sm-6 col-xs-6">
                            <label style="text-align: center;">Friday</label>
                            <input type="hidden" name="day" value="Friday">
                            <select class="form-control schedule_starttime" name="friday_starttime" required="required" id="friday_starttime">
                                <option value="closed" selected>Closed</option>
                                <?php
                                for ($i = $counter_startime; $i <= $counter_endtime; $i = $i + 15 * 60) {
                                    ?>
                                    <option value="<?php echo date('H:i', $i); ?>"><?php echo date('H:i', $i); ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form_intervals col-sm-6 col-xs-6">
                            <label style="text-align: center;">&nbsp;</label>
                            <select class="form-control schedule_endtime" name="friday_endtime" required="required" id="friday_endtime">
                                <option value="closed" selected>Closed</option>
                                <?php
                                for ($i = $counter_startime; $i <= $counter_endtime; $i = $i + 15 * 60) {
                                    ?>
                                    <option value="<?php echo date('H:i', $i); ?>"><?php echo date('H:i', $i); ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form_intervals col-sm-6 col-xs-6">
                            <label style="text-align: center;">Saturday</label>
                            <input type="hidden" name="day" value="Saturday">
                            <select class="form-control schedule_starttime" name="saturday_starttime" required="required" id="saturday_starttime">
                                <option value="closed" selected>Closed</option>
                                <?php
                                for ($i = $counter_startime; $i <= $counter_endtime; $i = $i + 15 * 60) {
                                    ?>
                                    <option value="<?php echo date('H:i', $i); ?>"><?php echo date('H:i', $i); ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form_intervals col-sm-6 col-xs-6">
                            <label style="text-align: center;">&nbsp;</label>
                            <select class="form-control schedule_endtime" name="saturday_endtime" required="required" id="saturday_endtime">
                                <option value="closed" selected>Closed</option>
                                <?php
                                for ($i = $counter_startime; $i <= $counter_endtime; $i = $i + 15 * 60) {
                                    ?>
                                    <option value="<?php echo date('H:i', $i); ?>"><?php echo date('H:i', $i); ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form_intervals col-sm-6 col-xs-6">
                            <label style="text-align: center;">Sunday</label>
                            <input type="hidden" name="day" value="Sunday">
                            <select class="form-control schedule_starttime" name="sunday_starttime" required="required" id="sunday_starttime">
                                <option value="closed" selected>Closed</option>
                                <?php
                                for ($i = $counter_startime; $i <= $counter_endtime; $i = $i + 15 * 60) {
                                    ?>
                                    <option value="<?php echo date('H:i', $i); ?>"><?php echo date('H:i', $i); ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form_intervals col-sm-6 col-xs-6">
                            <label style="text-align: center;">&nbsp;</label>
                            <select class="form-control schedule_endtime" name="sunday_endtime" required="required" id="sunday_endtime">
                                <option value="closed" selected>Closed</option>
                                <?php
                                for ($i = $counter_startime; $i <= $counter_endtime; $i = $i + 15 * 60) {
                                    ?>
                                    <option value="<?php echo date('H:i', $i); ?>"><?php echo date('H:i', $i); ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="clearfix"></div>
                    <br>
                    <button type="submit" value="submit" class="btn btn-success" ng-click="saveOpeningHours()">Submit</button>
                </form>
            </div>

        </div>

    </div>
</div>

<div id="lunchHours" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <?php 
            $counter_startime = strtotime('00:00');
            $counter_endtime = strtotime('23:30');
        ?>
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Lunch hours</h4>
            </div>
            <div class="modal-body">
                <form method="post" enctype="multipart/form-data" id="add_pharmacy_lunch_form">
                    <div class="form-group">
                        <div class="form_intervals col-sm-6 col-xs-6">
                            <label style="text-align: center;">Monday</label>
                            <input type="hidden" name="day" value="Monday">
                            <select class="form-control schedule_starttime" name="monday_lunch_starttime" id="monday_lunch_starttime" required="required">
                                <option value="closed" selected>Closed</option>
                                <?php
                                for ($i = $counter_startime; $i <= $counter_endtime; $i = $i + 15 * 60) {
                                    ?>
                                    <option value="<?php echo date('H:i', $i); ?>"><?php echo date('H:i', $i); ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form_intervals col-sm-6 col-xs-6">
                            <label style="text-align: center;">&nbsp;</label>
                            <select class="form-control schedule_endtime" name="monday_lunch_endtime" required="required" id="monday_lunch_endtime">
                                <option value="closed" selected ng-selected="$('#monday_lunch_starttime').val() == 'closed'">Closed</option>
                                <?php
                                for ($i = $counter_startime; $i <= $counter_endtime; $i = $i + 15 * 60) {
                                    ?>
                                    <option value="<?php echo date('H:i', $i); ?>"><?php echo date('H:i', $i); ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form_intervals col-sm-6 col-xs-6">
                            <label style="text-align: center;">Tuesday</label>
                            <input type="hidden" name="day" value="Tuesday">
                            <select class="form-control schedule_starttime" name="tuesday_lunch_starttime" required="required" id="tuesday_lunch_starttime">
                                <option value="closed" selected>Closed</option>
                                <?php
                                for ($i = $counter_startime; $i <= $counter_endtime; $i = $i + 15 * 60) {
                                    ?>
                                    <option value="<?php echo date('H:i', $i); ?>"><?php echo date('H:i', $i); ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form_intervals col-sm-6 col-xs-6">
                            <label style="text-align: center;">&nbsp;</label>
                            <select class="form-control schedule_endtime" name="tuesday_lunch_endtime" required="required" id="tuesday_lunch_endtime">
                                <option value="closed" selected>Closed</option>
                                <?php
                                for ($i = $counter_startime; $i <= $counter_endtime; $i = $i + 15 * 60) {
                                    ?>
                                    <option value="<?php echo date('H:i', $i); ?>"><?php echo date('H:i', $i); ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form_intervals col-sm-6 col-xs-6">
                            <label style="text-align: center;">Wednesday</label>
                            <input type="hidden" name="day" value="Wednesday">
                            <select class="form-control schedule_starttime" name="wednesday_lunch_starttime" required="required" id="wednesday_lunch_starttime">
                                <option value="closed" selected>Closed</option>
                                <?php
                                for ($i = $counter_startime; $i <= $counter_endtime; $i = $i + 15 * 60) {
                                    ?>
                                    <option value="<?php echo date('H:i', $i); ?>"><?php echo date('H:i', $i); ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form_intervals col-sm-6 col-xs-6">
                            <label style="text-align: center;">&nbsp;</label>
                            <select class="form-control schedule_endtime" name="wednesday_lunch_endtime" required="required" id="wednesday_lunch_endtime">
                                <option value="closed" selected>Closed</option>
                                <?php
                                for ($i = $counter_startime; $i <= $counter_endtime; $i = $i + 15 * 60) {
                                    ?>
                                    <option value="<?php echo date('H:i', $i); ?>"><?php echo date('H:i', $i); ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form_intervals col-sm-6 col-xs-6">
                            <label style="text-align: center;">Thursday</label>
                            <input type="hidden" name="day" value="Thursday">
                            <select class="form-control schedule_starttime" name="thursday_lunch_starttime" required="required" id="thursday_lunch_starttime">
                                <option value="closed" selected>Closed</option>
                                <?php
                                for ($i = $counter_startime; $i <= $counter_endtime; $i = $i + 15 * 60) {
                                    ?>
                                    <option value="<?php echo date('H:i', $i); ?>"><?php echo date('H:i', $i); ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form_intervals col-sm-6 col-xs-6">
                            <label style="text-align: center;">&nbsp;</label>
                            <select class="form-control schedule_endtime" name="thursday_lunch_endtime" required="required" id="thursday_lunch_endtime">
                                <option value="closed" selected>Closed</option>
                                <?php
                                for ($i = $counter_startime; $i <= $counter_endtime; $i = $i + 15 * 60) {
                                    ?>
                                    <option value="<?php echo date('H:i', $i); ?>"><?php echo date('H:i', $i); ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form_intervals col-sm-6 col-xs-6">
                            <label style="text-align: center;">Friday</label>
                            <input type="hidden" name="day" value="Friday">
                            <select class="form-control schedule_starttime" name="friday_lunch_starttime" required="required" id="friday_lunch_starttime">
                                <option value="closed" selected>Closed</option>
                                <?php
                                for ($i = $counter_startime; $i <= $counter_endtime; $i = $i + 15 * 60) {
                                    ?>
                                    <option value="<?php echo date('H:i', $i); ?>"><?php echo date('H:i', $i); ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form_intervals col-sm-6 col-xs-6">
                            <label style="text-align: center;">&nbsp;</label>
                            <select class="form-control schedule_endtime" name="friday_lunch_endtime" required="required" id="friday_lunch_endtime">
                                <option value="closed" selected>Closed</option>
                                <?php
                                for ($i = $counter_startime; $i <= $counter_endtime; $i = $i + 15 * 60) {
                                    ?>
                                    <option value="<?php echo date('H:i', $i); ?>"><?php echo date('H:i', $i); ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form_intervals col-sm-6 col-xs-6">
                            <label style="text-align: center;">Saturday</label>
                            <input type="hidden" name="day" value="Saturday">
                            <select class="form-control schedule_starttime" name="saturday_lunch_starttime" required="required" id="saturday_lunch_starttime">
                                <option value="closed" selected>Closed</option>
                                <?php
                                for ($i = $counter_startime; $i <= $counter_endtime; $i = $i + 15 * 60) {
                                    ?>
                                    <option value="<?php echo date('H:i', $i); ?>"><?php echo date('H:i', $i); ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form_intervals col-sm-6 col-xs-6">
                            <label style="text-align: center;">&nbsp;</label>
                            <select class="form-control schedule_endtime" name="saturday_lunch_endtime" required="required" id="saturday_lunch_endtime">
                                <option value="closed" selected>Closed</option>
                                <?php
                                for ($i = $counter_startime; $i <= $counter_endtime; $i = $i + 15 * 60) {
                                    ?>
                                    <option value="<?php echo date('H:i', $i); ?>"><?php echo date('H:i', $i); ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form_intervals col-sm-6 col-xs-6">
                            <label style="text-align: center;">Sunday</label>
                            <input type="hidden" name="day" value="Sunday">
                            <select class="form-control schedule_starttime" name="sunday_lunch_starttime" required="required" id="sunday_lunch_starttime">
                                <option value="closed" selected>Closed</option>
                                <?php
                                for ($i = $counter_startime; $i <= $counter_endtime; $i = $i + 15 * 60) {
                                    ?>
                                    <option value="<?php echo date('H:i', $i); ?>"><?php echo date('H:i', $i); ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form_intervals col-sm-6 col-xs-6">
                            <label style="text-align: center;">&nbsp;</label>
                            <select class="form-control schedule_endtime" name="sunday_lunch_endtime" required="required" id="sunday_lunch_endtime">
                                <option value="closed" selected>Closed</option>
                                <?php
                                for ($i = $counter_startime; $i <= $counter_endtime; $i = $i + 15 * 60) {
                                    ?>
                                    <option value="<?php echo date('H:i', $i); ?>"><?php echo date('H:i', $i); ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="clearfix"></div>
                    <br>
                    <button type="submit" value="submit" class="btn btn-success" ng-click="saveLunchHours()">Submit</button>
                </form>
            </div>

        </div>

    </div>
</div>