<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class RentalAgenciesController extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('rentalagencies_model');
    }
    
	public function index()
	{
		//phpinfo();
		$this->load->view('excel_upload_form');
	}

    public function spreadsheet_download()
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Name');
        $sheet->setCellValue('B1', 'Email');
        $sheet->setCellValue('C1', 'Password');
        $sheet->setCellValue('D1', 'Phone');
        $writer = new Xlsx($spreadsheet);
        $filename = 'users';
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"');
        header('Cache-Control: max-age=0');
        $writer->save('php://output'); // download file
    }

    public function import_excel_data_to_db()
    {
        $upload_file = $_FILES['upload_file']['name'];

        $extension = pathinfo($upload_file, PATHINFO_EXTENSION);

        if($extension == 'csv')
        {
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
        }
        else if($extension == 'xls')
        {
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        }
        else
        {
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        }

        $spreadsheet = $reader->load($_FILES['upload_file']['tmp_name']);

        $sheetData = $spreadsheet->getActiveSheet()->toArray();
      
        // echo "<pre>";
        // print_r($sheetData);
        // exit();

        $sheetCount = count($sheetData);
        // echo $sheetCount;
        // echo '<br/>';

        if($sheetCount>1)
        {
            $data = array();

            for($i=1; $i<$sheetCount;$i++)
            {
                $bd = $sheetData[$i][0]; 
                $apt = $sheetData[$i][1]; 
                $unit = $sheetData[$i][2];

                $rent = str_replace("$", "",$sheetData[$i][3]);
                $rent = str_replace(",", "",$rent);
                
                $phone = $sheetData[$i][4]; 
                $avail = $sheetData[$i][5]; 
                $central_air = $sheetData[$i][6]; 
                $rehab = $sheetData[$i][7]; 
                $heat = $sheetData[$i][8]; 
                $dw = $sheetData[$i][9]; 
                $wd = $sheetData[$i][10]; 
                $tour = $sheetData[$i][11]; 
                $source_type = 'self'; 

                $data[] = array(
                    'bd'=> $bd,
                    'apt'=> $apt,
                    'unit'=> $unit,
                    'rent'=> $rent,
                    'phone'=> $phone,
                    'avail'=> $avail,
                    'central_air'=> $central_air,
                    'rehab'=> $rehab,
                    'heat'=> $heat,
                    'dw'=> $dw,
                    'wd'=> $wd,
                    'tour'=> $tour,
                    'source_type'=> $source_type,
                );
                
                // $unit = $sheetData[$i][3]; 
                // $rent = $sheetData[$i][4]; 
            }

            // echo "<pre>";
            // print_r($data);
            // exit();

            $data_inserted = $this->rentalagencies_model->insert_batch($data);

            if($data_inserted)
            {
                $this->session->set_flashdata('message', '<div class="alert alert-success">Inserted Successfully!</div>');

                redirect('http://localhost/realestate/index.php/RentalAgenciesController/index');
            }
            else
            {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">Something Went Wrong!</div>');
                redirect('http://localhost/realestate/index.php/RentalAgenciesController/index');
            }
        }

    }
}
